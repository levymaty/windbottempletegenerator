using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Marik", "StructureDeckMarik")]
    public class StructureDeckMarikExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int GilGarth = 38445524;
            // Initialize all effect monsters
            public const int MysticTomato = 83011278;
            public const int ViserDes = 56043446;
            public const int LegendaryFiend = 99747800;
            public const int DarkJeroid = 90980792;
            public const int Newdoria = 4335645;
            public const int GravekeepersSpy = 24317029;
            public const int GravekeepersCurse = 50712728;
            public const int GravekeepersGuard = 37101832;
            public const int GravekeepersSpearSoldier = 63695531;
            public const int GravekeepersChief = 62473983;
            public const int GravekeepersCannonholder = 99877698;
            public const int GravekeepersAssailant = 25262697;
            public const int LavaGolem = 102380;
            public const int Drillago = 99050989;
            public const int Bowganian = 52090844;
            public const int GravekeepersCommandant = 17393207;
            public const int GravekeepersVisionary = 3825890;
            public const int GravekeepersDescendant = 30213599;
            public const int MysticalBeastofSerket = 89194033;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int MysticalSpaceTyphoon = 5318639;
            public const int NightmaresSteelcage = 58775978;
            public const int CreatureSwap = 31036355;
            public const int BookofMoon = 14087893;
            public const int DarkRoomofNightmare = 85562745;
            public const int Necrovalley = 47355498;
            public const int FoolishBurial = 81439174;
            public const int MagicalStoneExcavation = 98494543;
            public const int AllureofDarkness = 1475311;
            public const int AcidTrapHole = 41356845;
            public const int MirrorForce = 44095762;
            public const int SkullInvitation = 98139712;
            public const int CoffinSeller = 65830223;
            public const int NightmareWheel = 54704216;
            public const int MetalReflectSlime = 26905245;
            public const int MalevolentCatastrophe = 1224927;
            public const int DarkIllusion = 5562461;
            public const int TempleoftheKings = 29762407;
            // Initialize all useless cards

         }
        public Structure Deck - MarikExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.GilGarth, GilGarthNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GilGarth, GilGarthMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GilGarth, GilGarthRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.MysticTomato, MysticTomatoNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MysticTomato, MysticTomatoMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MysticTomato, MysticTomatoRepos);
            AddExecutor(ExecutorType.Activate, CardId.MysticTomato, MysticTomatoActivate);
            AddExecutor(ExecutorType.Summon, CardId.ViserDes, ViserDesNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ViserDes, ViserDesMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ViserDes, ViserDesRepos);
            AddExecutor(ExecutorType.Activate, CardId.ViserDes, ViserDesActivate);
            AddExecutor(ExecutorType.Summon, CardId.LegendaryFiend, LegendaryFiendNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LegendaryFiend, LegendaryFiendMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LegendaryFiend, LegendaryFiendRepos);
            AddExecutor(ExecutorType.Activate, CardId.LegendaryFiend, LegendaryFiendActivate);
            AddExecutor(ExecutorType.Summon, CardId.DarkJeroid, DarkJeroidNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DarkJeroid, DarkJeroidMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DarkJeroid, DarkJeroidRepos);
            AddExecutor(ExecutorType.Activate, CardId.DarkJeroid, DarkJeroidActivate);
            AddExecutor(ExecutorType.Summon, CardId.Newdoria, NewdoriaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Newdoria, NewdoriaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Newdoria, NewdoriaRepos);
            AddExecutor(ExecutorType.Activate, CardId.Newdoria, NewdoriaActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersSpy, GravekeepersSpyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersSpy, GravekeepersSpyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersSpy, GravekeepersSpyRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersSpy, GravekeepersSpyActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersCurse, GravekeepersCurseNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersCurse, GravekeepersCurseMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersCurse, GravekeepersCurseRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersCurse, GravekeepersCurseActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersGuard, GravekeepersGuardNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersGuard, GravekeepersGuardMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersGuard, GravekeepersGuardRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersGuard, GravekeepersGuardActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersSpearSoldier, GravekeepersSpearSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersSpearSoldier, GravekeepersSpearSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersSpearSoldier, GravekeepersSpearSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersSpearSoldier, GravekeepersSpearSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersChief, GravekeepersChiefNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersChief, GravekeepersChiefMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersChief, GravekeepersChiefRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersChief, GravekeepersChiefActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersCannonholder, GravekeepersCannonholderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersCannonholder, GravekeepersCannonholderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersCannonholder, GravekeepersCannonholderRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersCannonholder, GravekeepersCannonholderActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersAssailant, GravekeepersAssailantNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersAssailant, GravekeepersAssailantMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersAssailant, GravekeepersAssailantRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersAssailant, GravekeepersAssailantActivate);
            AddExecutor(ExecutorType.Summon, CardId.LavaGolem, LavaGolemNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LavaGolem, LavaGolemMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LavaGolem, LavaGolemRepos);
            AddExecutor(ExecutorType.Activate, CardId.LavaGolem, LavaGolemActivate);
            AddExecutor(ExecutorType.Summon, CardId.Drillago, DrillagoNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Drillago, DrillagoMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Drillago, DrillagoRepos);
            AddExecutor(ExecutorType.Activate, CardId.Drillago, DrillagoActivate);
            AddExecutor(ExecutorType.Summon, CardId.Bowganian, BowganianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Bowganian, BowganianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Bowganian, BowganianRepos);
            AddExecutor(ExecutorType.Activate, CardId.Bowganian, BowganianActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersCommandant, GravekeepersCommandantNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersCommandant, GravekeepersCommandantMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersCommandant, GravekeepersCommandantRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersCommandant, GravekeepersCommandantActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersVisionary, GravekeepersVisionaryNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersVisionary, GravekeepersVisionaryMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersVisionary, GravekeepersVisionaryRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersVisionary, GravekeepersVisionaryActivate);
            AddExecutor(ExecutorType.Summon, CardId.GravekeepersDescendant, GravekeepersDescendantNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GravekeepersDescendant, GravekeepersDescendantMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GravekeepersDescendant, GravekeepersDescendantRepos);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersDescendant, GravekeepersDescendantActivate);
            AddExecutor(ExecutorType.Summon, CardId.MysticalBeastofSerket, MysticalBeastofSerketNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MysticalBeastofSerket, MysticalBeastofSerketMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MysticalBeastofSerket, MysticalBeastofSerketRepos);
            AddExecutor(ExecutorType.Activate, CardId.MysticalBeastofSerket, MysticalBeastofSerketActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.NightmaresSteelcage, NightmaresSteelcageSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.NightmaresSteelcage, NightmaresSteelcageActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BookofMoon, BookofMoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BookofMoon, BookofMoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkRoomofNightmare, DarkRoomofNightmareSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkRoomofNightmare, DarkRoomofNightmareActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Necrovalley, NecrovalleySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Necrovalley, NecrovalleyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FoolishBurial, FoolishBurialSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FoolishBurial, FoolishBurialActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MagicalStoneExcavation, MagicalStoneExcavationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MagicalStoneExcavation, MagicalStoneExcavationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AllureofDarkness, AllureofDarknessSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AllureofDarkness, AllureofDarknessActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AcidTrapHole, AcidTrapHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AcidTrapHole, AcidTrapHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, MirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, MirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SkullInvitation, SkullInvitationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SkullInvitation, SkullInvitationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CoffinSeller, CoffinSellerSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CoffinSeller, CoffinSellerActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.NightmareWheel, NightmareWheelSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.NightmareWheel, NightmareWheelActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MetalReflectSlime, MetalReflectSlimeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MetalReflectSlime, MetalReflectSlimeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MalevolentCatastrophe, MalevolentCatastropheSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MalevolentCatastrophe, MalevolentCatastropheActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkIllusion, DarkIllusionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkIllusion, DarkIllusionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TempleoftheKings, TempleoftheKingsSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TempleoftheKings, TempleoftheKingsActivate);

         }

            // All Normal Monster Methods

        private bool GilGarthNormalSummon()
        {

            return true;
        }

        private bool GilGarthMonsterSet()
        {

            return true;
        }

        private bool GilGarthRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool MysticTomatoNormalSummon()
        {

            return true;
        }

        private bool MysticTomatoMonsterSet()
        {

            return true;
        }

        private bool MysticTomatoRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MysticTomatoActivate()
        {

            return true;
        }

        private bool ViserDesNormalSummon()
        {

            return true;
        }

        private bool ViserDesMonsterSet()
        {

            return true;
        }

        private bool ViserDesRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ViserDesActivate()
        {

            return true;
        }

        private bool LegendaryFiendNormalSummon()
        {

            return true;
        }

        private bool LegendaryFiendMonsterSet()
        {

            return true;
        }

        private bool LegendaryFiendRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LegendaryFiendActivate()
        {

            return true;
        }

        private bool DarkJeroidNormalSummon()
        {

            return true;
        }

        private bool DarkJeroidMonsterSet()
        {

            return true;
        }

        private bool DarkJeroidRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DarkJeroidActivate()
        {

            return true;
        }

        private bool NewdoriaNormalSummon()
        {

            return true;
        }

        private bool NewdoriaMonsterSet()
        {

            return true;
        }

        private bool NewdoriaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NewdoriaActivate()
        {

            return true;
        }

        private bool GravekeepersSpyNormalSummon()
        {

            return true;
        }

        private bool GravekeepersSpyMonsterSet()
        {

            return true;
        }

        private bool GravekeepersSpyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersSpyActivate()
        {

            return true;
        }

        private bool GravekeepersCurseNormalSummon()
        {

            return true;
        }

        private bool GravekeepersCurseMonsterSet()
        {

            return true;
        }

        private bool GravekeepersCurseRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersCurseActivate()
        {

            return true;
        }

        private bool GravekeepersGuardNormalSummon()
        {

            return true;
        }

        private bool GravekeepersGuardMonsterSet()
        {

            return true;
        }

        private bool GravekeepersGuardRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersGuardActivate()
        {

            return true;
        }

        private bool GravekeepersSpearSoldierNormalSummon()
        {

            return true;
        }

        private bool GravekeepersSpearSoldierMonsterSet()
        {

            return true;
        }

        private bool GravekeepersSpearSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersSpearSoldierActivate()
        {

            return true;
        }

        private bool GravekeepersChiefNormalSummon()
        {

            return true;
        }

        private bool GravekeepersChiefMonsterSet()
        {

            return true;
        }

        private bool GravekeepersChiefRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersChiefActivate()
        {

            return true;
        }

        private bool GravekeepersCannonholderNormalSummon()
        {

            return true;
        }

        private bool GravekeepersCannonholderMonsterSet()
        {

            return true;
        }

        private bool GravekeepersCannonholderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersCannonholderActivate()
        {

            return true;
        }

        private bool GravekeepersAssailantNormalSummon()
        {

            return true;
        }

        private bool GravekeepersAssailantMonsterSet()
        {

            return true;
        }

        private bool GravekeepersAssailantRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersAssailantActivate()
        {

            return true;
        }

        private bool LavaGolemNormalSummon()
        {

            return true;
        }

        private bool LavaGolemMonsterSet()
        {

            return true;
        }

        private bool LavaGolemRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LavaGolemActivate()
        {

            return true;
        }

        private bool DrillagoNormalSummon()
        {

            return true;
        }

        private bool DrillagoMonsterSet()
        {

            return true;
        }

        private bool DrillagoRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DrillagoActivate()
        {

            return true;
        }

        private bool BowganianNormalSummon()
        {

            return true;
        }

        private bool BowganianMonsterSet()
        {

            return true;
        }

        private bool BowganianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BowganianActivate()
        {

            return true;
        }

        private bool GravekeepersCommandantNormalSummon()
        {

            return true;
        }

        private bool GravekeepersCommandantMonsterSet()
        {

            return true;
        }

        private bool GravekeepersCommandantRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersCommandantActivate()
        {

            return true;
        }

        private bool GravekeepersVisionaryNormalSummon()
        {

            return true;
        }

        private bool GravekeepersVisionaryMonsterSet()
        {

            return true;
        }

        private bool GravekeepersVisionaryRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersVisionaryActivate()
        {

            return true;
        }

        private bool GravekeepersDescendantNormalSummon()
        {

            return true;
        }

        private bool GravekeepersDescendantMonsterSet()
        {

            return true;
        }

        private bool GravekeepersDescendantRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GravekeepersDescendantActivate()
        {

            return true;
        }

        private bool MysticalBeastofSerketNormalSummon()
        {

            return true;
        }

        private bool MysticalBeastofSerketMonsterSet()
        {

            return true;
        }

        private bool MysticalBeastofSerketRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MysticalBeastofSerketActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool NightmaresSteelcageSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NightmaresSteelcageActivate()
        {

            return true;
        }

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool BookofMoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BookofMoonActivate()
        {

            return true;
        }

        private bool DarkRoomofNightmareSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkRoomofNightmareActivate()
        {

            return true;
        }

        private bool NecrovalleySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NecrovalleyActivate()
        {

            return true;
        }

        private bool FoolishBurialSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FoolishBurialActivate()
        {

            return true;
        }

        private bool MagicalStoneExcavationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MagicalStoneExcavationActivate()
        {

            return true;
        }

        private bool AllureofDarknessSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AllureofDarknessActivate()
        {

            return true;
        }

        private bool AcidTrapHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AcidTrapHoleActivate()
        {

            return true;
        }

        private bool MirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MirrorForceActivate()
        {

            return true;
        }

        private bool SkullInvitationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SkullInvitationActivate()
        {

            return true;
        }

        private bool CoffinSellerSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CoffinSellerActivate()
        {

            return true;
        }

        private bool NightmareWheelSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NightmareWheelActivate()
        {

            return true;
        }

        private bool MetalReflectSlimeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MetalReflectSlimeActivate()
        {

            return true;
        }

        private bool MalevolentCatastropheSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MalevolentCatastropheActivate()
        {

            return true;
        }

        private bool DarkIllusionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkIllusionActivate()
        {

            return true;
        }

        private bool TempleoftheKingsSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TempleoftheKingsActivate()
        {

            return true;
        }

    }
}