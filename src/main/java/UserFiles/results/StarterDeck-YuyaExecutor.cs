using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Starter Deck - Yuya", "StarterDeckYuya")]
    public class StarterDeckYuyaExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int FoucaultsCannon = 43785278;
            public const int GeneWarpedWarwolf = 69247929;
            // Initialize all effect monsters
            public const int PerformapalSleightHandMagician = 20403123;
            public const int PerformapalKingBear = 67808837;
            public const int PerformapalSwincobra = 93892436;
            public const int PerformapalMomoncarpet = 20281581;
            public const int PerformapalParrotrio = 56675280;
            public const int PerformapalLongphoneBull = 92170894;
            public const int PerformapalTeeterTotterHopper = 29169993;
            public const int OddEyesPendulumDragon = 16178681;
            public const int StargazerMagician = 94415058;
            public const int TimegazerMagician = 20409757;
            public const int PerformapalDrummerilla = 70479321;
            public const int PerformapalSecondonkey = 15978426;
            public const int PerformapalHipHippo = 41440148;
            public const int ArchfiendEccentrick = 57624336;
            public const int BeastKingBarbaros = 78651105;
            public const int PitchBlackWarwolf = 88975532;
            public const int DragonDowser = 76066541;
            public const int GiantRat = 97017120;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int PerformapalDramaticTheater = 55553602;
            public const int SmileWorld = 2099841;
            public const int HippoCarnival = 18027138;
            public const int DrawMuscle = 41367003;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int LightningVortex = 69162969;
            public const int BookofMoon = 14087893;
            public const int LuckyIronAxe = 34664411;
            public const int Ceasefire = 36468556;
            public const int BurdenoftheMighty = 44947065;
            public const int BackupSquad = 28486799;
            public const int PerformapalShowDown = 92958307;
            public const int PerformapalPinchHelper = 36415522;
            public const int WallofDisruption = 58169731;
            public const int RaigekiBreak = 4178474;
            public const int DrainingShield = 43250041;
            public const int ThreateningRoar = 36361633;
            public const int DarkBribe = 77538567;
            public const int ChaosBurst = 4923662;
            public const int PendulumReborn = 77826734;
            // Initialize all useless cards

         }
        public Starter Deck - YuyaExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.FoucaultsCannon, FoucaultsCannonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FoucaultsCannon, FoucaultsCannonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FoucaultsCannon, FoucaultsCannonRepos);
            AddExecutor(ExecutorType.Summon, CardId.GeneWarpedWarwolf, GeneWarpedWarwolfNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GeneWarpedWarwolf, GeneWarpedWarwolfMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GeneWarpedWarwolf, GeneWarpedWarwolfRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.PerformapalSleightHandMagician, PerformapalSleightHandMagicianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalSleightHandMagician, PerformapalSleightHandMagicianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalSleightHandMagician, PerformapalSleightHandMagicianRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalSleightHandMagician, PerformapalSleightHandMagicianActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalKingBear, PerformapalKingBearNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalKingBear, PerformapalKingBearMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalKingBear, PerformapalKingBearRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalKingBear, PerformapalKingBearActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalSwincobra, PerformapalSwincobraNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalSwincobra, PerformapalSwincobraMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalSwincobra, PerformapalSwincobraRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalSwincobra, PerformapalSwincobraActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalMomoncarpet, PerformapalMomoncarpetNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalMomoncarpet, PerformapalMomoncarpetMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalMomoncarpet, PerformapalMomoncarpetRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalMomoncarpet, PerformapalMomoncarpetActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalParrotrio, PerformapalParrotrioNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalParrotrio, PerformapalParrotrioMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalParrotrio, PerformapalParrotrioRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalParrotrio, PerformapalParrotrioActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalLongphoneBull, PerformapalLongphoneBullNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalLongphoneBull, PerformapalLongphoneBullMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalLongphoneBull, PerformapalLongphoneBullRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalLongphoneBull, PerformapalLongphoneBullActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalTeeterTotterHopper, PerformapalTeeterTotterHopperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalTeeterTotterHopper, PerformapalTeeterTotterHopperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalTeeterTotterHopper, PerformapalTeeterTotterHopperRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalTeeterTotterHopper, PerformapalTeeterTotterHopperActivate);
            AddExecutor(ExecutorType.Summon, CardId.OddEyesPendulumDragon, OddEyesPendulumDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.OddEyesPendulumDragon, OddEyesPendulumDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.OddEyesPendulumDragon, OddEyesPendulumDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.OddEyesPendulumDragon, OddEyesPendulumDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.StargazerMagician, StargazerMagicianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.StargazerMagician, StargazerMagicianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.StargazerMagician, StargazerMagicianRepos);
            AddExecutor(ExecutorType.Activate, CardId.StargazerMagician, StargazerMagicianActivate);
            AddExecutor(ExecutorType.Summon, CardId.TimegazerMagician, TimegazerMagicianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TimegazerMagician, TimegazerMagicianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TimegazerMagician, TimegazerMagicianRepos);
            AddExecutor(ExecutorType.Activate, CardId.TimegazerMagician, TimegazerMagicianActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalDrummerilla, PerformapalDrummerillaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalDrummerilla, PerformapalDrummerillaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalDrummerilla, PerformapalDrummerillaRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalDrummerilla, PerformapalDrummerillaActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalSecondonkey, PerformapalSecondonkeyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalSecondonkey, PerformapalSecondonkeyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalSecondonkey, PerformapalSecondonkeyRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalSecondonkey, PerformapalSecondonkeyActivate);
            AddExecutor(ExecutorType.Summon, CardId.PerformapalHipHippo, PerformapalHipHippoNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PerformapalHipHippo, PerformapalHipHippoMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PerformapalHipHippo, PerformapalHipHippoRepos);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalHipHippo, PerformapalHipHippoActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArchfiendEccentrick, ArchfiendEccentrickNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArchfiendEccentrick, ArchfiendEccentrickMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArchfiendEccentrick, ArchfiendEccentrickRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArchfiendEccentrick, ArchfiendEccentrickActivate);
            AddExecutor(ExecutorType.Summon, CardId.BeastKingBarbaros, BeastKingBarbarosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BeastKingBarbaros, BeastKingBarbarosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BeastKingBarbaros, BeastKingBarbarosRepos);
            AddExecutor(ExecutorType.Activate, CardId.BeastKingBarbaros, BeastKingBarbarosActivate);
            AddExecutor(ExecutorType.Summon, CardId.PitchBlackWarwolf, PitchBlackWarwolfNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PitchBlackWarwolf, PitchBlackWarwolfMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PitchBlackWarwolf, PitchBlackWarwolfRepos);
            AddExecutor(ExecutorType.Activate, CardId.PitchBlackWarwolf, PitchBlackWarwolfActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragonDowser, DragonDowserNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragonDowser, DragonDowserMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragonDowser, DragonDowserRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragonDowser, DragonDowserActivate);
            AddExecutor(ExecutorType.Summon, CardId.GiantRat, GiantRatNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GiantRat, GiantRatMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GiantRat, GiantRatRepos);
            AddExecutor(ExecutorType.Activate, CardId.GiantRat, GiantRatActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.PerformapalDramaticTheater, PerformapalDramaticTheaterSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalDramaticTheater, PerformapalDramaticTheaterActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SmileWorld, SmileWorldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SmileWorld, SmileWorldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HippoCarnival, HippoCarnivalSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HippoCarnival, HippoCarnivalActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrawMuscle, DrawMuscleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrawMuscle, DrawMuscleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightningVortex, LightningVortexSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightningVortex, LightningVortexActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BookofMoon, BookofMoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BookofMoon, BookofMoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LuckyIronAxe, LuckyIronAxeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LuckyIronAxe, LuckyIronAxeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Ceasefire, CeasefireSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Ceasefire, CeasefireActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BurdenoftheMighty, BurdenoftheMightySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BurdenoftheMighty, BurdenoftheMightyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BackupSquad, BackupSquadSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BackupSquad, BackupSquadActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PerformapalShowDown, PerformapalShowDownSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalShowDown, PerformapalShowDownActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PerformapalPinchHelper, PerformapalPinchHelperSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PerformapalPinchHelper, PerformapalPinchHelperActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.WallofDisruption, WallofDisruptionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.WallofDisruption, WallofDisruptionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RaigekiBreak, RaigekiBreakSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RaigekiBreak, RaigekiBreakActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrainingShield, DrainingShieldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrainingShield, DrainingShieldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ThreateningRoar, ThreateningRoarSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ThreateningRoar, ThreateningRoarActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkBribe, DarkBribeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkBribe, DarkBribeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ChaosBurst, ChaosBurstSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ChaosBurst, ChaosBurstActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PendulumReborn, PendulumRebornSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PendulumReborn, PendulumRebornActivate);

         }

            // All Normal Monster Methods

        private bool FoucaultsCannonNormalSummon()
        {

            return true;
        }

        private bool FoucaultsCannonMonsterSet()
        {

            return true;
        }

        private bool FoucaultsCannonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GeneWarpedWarwolfNormalSummon()
        {

            return true;
        }

        private bool GeneWarpedWarwolfMonsterSet()
        {

            return true;
        }

        private bool GeneWarpedWarwolfRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool PerformapalSleightHandMagicianNormalSummon()
        {

            return true;
        }

        private bool PerformapalSleightHandMagicianMonsterSet()
        {

            return true;
        }

        private bool PerformapalSleightHandMagicianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalSleightHandMagicianActivate()
        {

            return true;
        }

        private bool PerformapalKingBearNormalSummon()
        {

            return true;
        }

        private bool PerformapalKingBearMonsterSet()
        {

            return true;
        }

        private bool PerformapalKingBearRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalKingBearActivate()
        {

            return true;
        }

        private bool PerformapalSwincobraNormalSummon()
        {

            return true;
        }

        private bool PerformapalSwincobraMonsterSet()
        {

            return true;
        }

        private bool PerformapalSwincobraRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalSwincobraActivate()
        {

            return true;
        }

        private bool PerformapalMomoncarpetNormalSummon()
        {

            return true;
        }

        private bool PerformapalMomoncarpetMonsterSet()
        {

            return true;
        }

        private bool PerformapalMomoncarpetRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalMomoncarpetActivate()
        {

            return true;
        }

        private bool PerformapalParrotrioNormalSummon()
        {

            return true;
        }

        private bool PerformapalParrotrioMonsterSet()
        {

            return true;
        }

        private bool PerformapalParrotrioRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalParrotrioActivate()
        {

            return true;
        }

        private bool PerformapalLongphoneBullNormalSummon()
        {

            return true;
        }

        private bool PerformapalLongphoneBullMonsterSet()
        {

            return true;
        }

        private bool PerformapalLongphoneBullRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalLongphoneBullActivate()
        {

            return true;
        }

        private bool PerformapalTeeterTotterHopperNormalSummon()
        {

            return true;
        }

        private bool PerformapalTeeterTotterHopperMonsterSet()
        {

            return true;
        }

        private bool PerformapalTeeterTotterHopperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalTeeterTotterHopperActivate()
        {

            return true;
        }

        private bool OddEyesPendulumDragonNormalSummon()
        {

            return true;
        }

        private bool OddEyesPendulumDragonMonsterSet()
        {

            return true;
        }

        private bool OddEyesPendulumDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool OddEyesPendulumDragonActivate()
        {

            return true;
        }

        private bool StargazerMagicianNormalSummon()
        {

            return true;
        }

        private bool StargazerMagicianMonsterSet()
        {

            return true;
        }

        private bool StargazerMagicianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool StargazerMagicianActivate()
        {

            return true;
        }

        private bool TimegazerMagicianNormalSummon()
        {

            return true;
        }

        private bool TimegazerMagicianMonsterSet()
        {

            return true;
        }

        private bool TimegazerMagicianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TimegazerMagicianActivate()
        {

            return true;
        }

        private bool PerformapalDrummerillaNormalSummon()
        {

            return true;
        }

        private bool PerformapalDrummerillaMonsterSet()
        {

            return true;
        }

        private bool PerformapalDrummerillaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalDrummerillaActivate()
        {

            return true;
        }

        private bool PerformapalSecondonkeyNormalSummon()
        {

            return true;
        }

        private bool PerformapalSecondonkeyMonsterSet()
        {

            return true;
        }

        private bool PerformapalSecondonkeyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalSecondonkeyActivate()
        {

            return true;
        }

        private bool PerformapalHipHippoNormalSummon()
        {

            return true;
        }

        private bool PerformapalHipHippoMonsterSet()
        {

            return true;
        }

        private bool PerformapalHipHippoRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PerformapalHipHippoActivate()
        {

            return true;
        }

        private bool ArchfiendEccentrickNormalSummon()
        {

            return true;
        }

        private bool ArchfiendEccentrickMonsterSet()
        {

            return true;
        }

        private bool ArchfiendEccentrickRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArchfiendEccentrickActivate()
        {

            return true;
        }

        private bool BeastKingBarbarosNormalSummon()
        {

            return true;
        }

        private bool BeastKingBarbarosMonsterSet()
        {

            return true;
        }

        private bool BeastKingBarbarosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BeastKingBarbarosActivate()
        {

            return true;
        }

        private bool PitchBlackWarwolfNormalSummon()
        {

            return true;
        }

        private bool PitchBlackWarwolfMonsterSet()
        {

            return true;
        }

        private bool PitchBlackWarwolfRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PitchBlackWarwolfActivate()
        {

            return true;
        }

        private bool DragonDowserNormalSummon()
        {

            return true;
        }

        private bool DragonDowserMonsterSet()
        {

            return true;
        }

        private bool DragonDowserRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragonDowserActivate()
        {

            return true;
        }

        private bool GiantRatNormalSummon()
        {

            return true;
        }

        private bool GiantRatMonsterSet()
        {

            return true;
        }

        private bool GiantRatRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GiantRatActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool PerformapalDramaticTheaterSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PerformapalDramaticTheaterActivate()
        {

            return true;
        }

        private bool SmileWorldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SmileWorldActivate()
        {

            return true;
        }

        private bool HippoCarnivalSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HippoCarnivalActivate()
        {

            return true;
        }

        private bool DrawMuscleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrawMuscleActivate()
        {

            return true;
        }

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool LightningVortexSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightningVortexActivate()
        {

            return true;
        }

        private bool BookofMoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BookofMoonActivate()
        {

            return true;
        }

        private bool LuckyIronAxeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LuckyIronAxeActivate()
        {

            return true;
        }

        private bool CeasefireSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CeasefireActivate()
        {

            return true;
        }

        private bool BurdenoftheMightySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BurdenoftheMightyActivate()
        {

            return true;
        }

        private bool BackupSquadSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BackupSquadActivate()
        {

            return true;
        }

        private bool PerformapalShowDownSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PerformapalShowDownActivate()
        {

            return true;
        }

        private bool PerformapalPinchHelperSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PerformapalPinchHelperActivate()
        {

            return true;
        }

        private bool WallofDisruptionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WallofDisruptionActivate()
        {

            return true;
        }

        private bool RaigekiBreakSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RaigekiBreakActivate()
        {

            return true;
        }

        private bool DrainingShieldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrainingShieldActivate()
        {

            return true;
        }

        private bool ThreateningRoarSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ThreateningRoarActivate()
        {

            return true;
        }

        private bool DarkBribeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkBribeActivate()
        {

            return true;
        }

        private bool ChaosBurstSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ChaosBurstActivate()
        {

            return true;
        }

        private bool PendulumRebornSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PendulumRebornActivate()
        {

            return true;
        }

    }
}