using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Gates of the Underworld", "StructureDeckGatesoftheUnderworld")]
    public class StructureDeckGatesoftheUnderworldExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int ZureKnightofDarkWorld = 7459013;
            public const int RengeGatekeeperofDarkWorld = 60606759;
            // Initialize all effect monsters
            public const int GraphaDragonLordofDarkWorld = 34230233;
            public const int SnowwUnlightofDarkWorld = 60228941;
            public const int CeruliGuruofDarkWorld = 7623640;
            public const int ScarrScoutofDarkWorld = 5498296;
            public const int KahkkiGuerillaofDarkWorld = 25847467;
            public const int GrenTacticianofDarkWorld = 51232472;
            public const int BrowwHuntsmanofDarkWorld = 79126789;
            public const int BeiigeVanguardofDarkWorld = 33731070;
            public const int BrronMadKingofDarkWorld = 6214884;
            public const int SillvaWarlordofDarkWorld = 32619583;
            public const int GolddWuLordofDarkWorld = 78004197;
            public const int ReignBeauxOverlordofDarkWorld = 99458769;
            public const int BelialMarquisofDarkness = 33655493;
            public const int Tragoedia = 98777036;
            public const int Sangan = 26202165;
            public const int Newdoria = 4335645;
            public const int GoblinKing = 18590133;
            public const int GraveSquirmer = 48343627;
            public const int CardGuard = 4694209;
            public const int BattleFader = 19665973;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int TheGatesofDarkWorld = 33017655;
            public const int DarkWorldLightning = 93554166;
            public const int GatewaytoDarkWorld = 93431518;
            public const int DarkWorldDealings = 74117290;
            public const int AllureofDarkness = 1475311;
            public const int CardDestruction = 72892473;
            public const int Terraforming = 73628505;
            public const int DarkEruption = 674561;
            public const int DarkScheme = 69402394;
            public const int TheForcesofDarkness = 29826127;
            public const int DeckDevastationVirus = 35027493;
            public const int EradicatorEpidemicVirus = 54974237;
            public const int MindCrush = 15800838;
            public const int DarkDeal = 65824822;
            public const int TheTransmigrationProphecy = 46652477;
            public const int EscapefromtheDarkDimension = 31550470;
            public const int DarkBribe = 77538567;
            // Initialize all useless cards

         }
        public Structure Deck - Gates of the UnderworldExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.ZureKnightofDarkWorld, ZureKnightofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ZureKnightofDarkWorld, ZureKnightofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ZureKnightofDarkWorld, ZureKnightofDarkWorldRepos);
            AddExecutor(ExecutorType.Summon, CardId.RengeGatekeeperofDarkWorld, RengeGatekeeperofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RengeGatekeeperofDarkWorld, RengeGatekeeperofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RengeGatekeeperofDarkWorld, RengeGatekeeperofDarkWorldRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.GraphaDragonLordofDarkWorld, GraphaDragonLordofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GraphaDragonLordofDarkWorld, GraphaDragonLordofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GraphaDragonLordofDarkWorld, GraphaDragonLordofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.GraphaDragonLordofDarkWorld, GraphaDragonLordofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.SnowwUnlightofDarkWorld, SnowwUnlightofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SnowwUnlightofDarkWorld, SnowwUnlightofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SnowwUnlightofDarkWorld, SnowwUnlightofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.SnowwUnlightofDarkWorld, SnowwUnlightofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.CeruliGuruofDarkWorld, CeruliGuruofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CeruliGuruofDarkWorld, CeruliGuruofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CeruliGuruofDarkWorld, CeruliGuruofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.CeruliGuruofDarkWorld, CeruliGuruofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.ScarrScoutofDarkWorld, ScarrScoutofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ScarrScoutofDarkWorld, ScarrScoutofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ScarrScoutofDarkWorld, ScarrScoutofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.ScarrScoutofDarkWorld, ScarrScoutofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.KahkkiGuerillaofDarkWorld, KahkkiGuerillaofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.KahkkiGuerillaofDarkWorld, KahkkiGuerillaofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.KahkkiGuerillaofDarkWorld, KahkkiGuerillaofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.KahkkiGuerillaofDarkWorld, KahkkiGuerillaofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.GrenTacticianofDarkWorld, GrenTacticianofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GrenTacticianofDarkWorld, GrenTacticianofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GrenTacticianofDarkWorld, GrenTacticianofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.GrenTacticianofDarkWorld, GrenTacticianofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.BrowwHuntsmanofDarkWorld, BrowwHuntsmanofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BrowwHuntsmanofDarkWorld, BrowwHuntsmanofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BrowwHuntsmanofDarkWorld, BrowwHuntsmanofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.BrowwHuntsmanofDarkWorld, BrowwHuntsmanofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.BeiigeVanguardofDarkWorld, BeiigeVanguardofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BeiigeVanguardofDarkWorld, BeiigeVanguardofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BeiigeVanguardofDarkWorld, BeiigeVanguardofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.BeiigeVanguardofDarkWorld, BeiigeVanguardofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.BrronMadKingofDarkWorld, BrronMadKingofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BrronMadKingofDarkWorld, BrronMadKingofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BrronMadKingofDarkWorld, BrronMadKingofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.BrronMadKingofDarkWorld, BrronMadKingofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.SillvaWarlordofDarkWorld, SillvaWarlordofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SillvaWarlordofDarkWorld, SillvaWarlordofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SillvaWarlordofDarkWorld, SillvaWarlordofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.SillvaWarlordofDarkWorld, SillvaWarlordofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.GolddWuLordofDarkWorld, GolddWuLordofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GolddWuLordofDarkWorld, GolddWuLordofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GolddWuLordofDarkWorld, GolddWuLordofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.GolddWuLordofDarkWorld, GolddWuLordofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.ReignBeauxOverlordofDarkWorld, ReignBeauxOverlordofDarkWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ReignBeauxOverlordofDarkWorld, ReignBeauxOverlordofDarkWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ReignBeauxOverlordofDarkWorld, ReignBeauxOverlordofDarkWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.ReignBeauxOverlordofDarkWorld, ReignBeauxOverlordofDarkWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.BelialMarquisofDarkness, BelialMarquisofDarknessNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BelialMarquisofDarkness, BelialMarquisofDarknessMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BelialMarquisofDarkness, BelialMarquisofDarknessRepos);
            AddExecutor(ExecutorType.Activate, CardId.BelialMarquisofDarkness, BelialMarquisofDarknessActivate);
            AddExecutor(ExecutorType.Summon, CardId.Tragoedia, TragoediaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Tragoedia, TragoediaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Tragoedia, TragoediaRepos);
            AddExecutor(ExecutorType.Activate, CardId.Tragoedia, TragoediaActivate);
            AddExecutor(ExecutorType.Summon, CardId.Sangan, SanganNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Sangan, SanganMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Sangan, SanganRepos);
            AddExecutor(ExecutorType.Activate, CardId.Sangan, SanganActivate);
            AddExecutor(ExecutorType.Summon, CardId.Newdoria, NewdoriaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Newdoria, NewdoriaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Newdoria, NewdoriaRepos);
            AddExecutor(ExecutorType.Activate, CardId.Newdoria, NewdoriaActivate);
            AddExecutor(ExecutorType.Summon, CardId.GoblinKing, GoblinKingNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GoblinKing, GoblinKingMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GoblinKing, GoblinKingRepos);
            AddExecutor(ExecutorType.Activate, CardId.GoblinKing, GoblinKingActivate);
            AddExecutor(ExecutorType.Summon, CardId.GraveSquirmer, GraveSquirmerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GraveSquirmer, GraveSquirmerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GraveSquirmer, GraveSquirmerRepos);
            AddExecutor(ExecutorType.Activate, CardId.GraveSquirmer, GraveSquirmerActivate);
            AddExecutor(ExecutorType.Summon, CardId.CardGuard, CardGuardNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CardGuard, CardGuardMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CardGuard, CardGuardRepos);
            AddExecutor(ExecutorType.Activate, CardId.CardGuard, CardGuardActivate);
            AddExecutor(ExecutorType.Summon, CardId.BattleFader, BattleFaderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BattleFader, BattleFaderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BattleFader, BattleFaderRepos);
            AddExecutor(ExecutorType.Activate, CardId.BattleFader, BattleFaderActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.TheGatesofDarkWorld, TheGatesofDarkWorldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheGatesofDarkWorld, TheGatesofDarkWorldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkWorldLightning, DarkWorldLightningSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkWorldLightning, DarkWorldLightningActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GatewaytoDarkWorld, GatewaytoDarkWorldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GatewaytoDarkWorld, GatewaytoDarkWorldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkWorldDealings, DarkWorldDealingsSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkWorldDealings, DarkWorldDealingsActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AllureofDarkness, AllureofDarknessSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AllureofDarkness, AllureofDarknessActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardDestruction, CardDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardDestruction, CardDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Terraforming, TerraformingSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Terraforming, TerraformingActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkEruption, DarkEruptionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkEruption, DarkEruptionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkScheme, DarkSchemeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkScheme, DarkSchemeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TheForcesofDarkness, TheForcesofDarknessSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheForcesofDarkness, TheForcesofDarknessActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DeckDevastationVirus, DeckDevastationVirusSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DeckDevastationVirus, DeckDevastationVirusActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.EradicatorEpidemicVirus, EradicatorEpidemicVirusSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.EradicatorEpidemicVirus, EradicatorEpidemicVirusActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MindCrush, MindCrushSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MindCrush, MindCrushActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkDeal, DarkDealSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkDeal, DarkDealActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TheTransmigrationProphecy, TheTransmigrationProphecySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheTransmigrationProphecy, TheTransmigrationProphecyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.EscapefromtheDarkDimension, EscapefromtheDarkDimensionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.EscapefromtheDarkDimension, EscapefromtheDarkDimensionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkBribe, DarkBribeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkBribe, DarkBribeActivate);

         }

            // All Normal Monster Methods

        private bool ZureKnightofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool ZureKnightofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool ZureKnightofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RengeGatekeeperofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool RengeGatekeeperofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool RengeGatekeeperofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool GraphaDragonLordofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool GraphaDragonLordofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool GraphaDragonLordofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GraphaDragonLordofDarkWorldActivate()
        {

            return true;
        }

        private bool SnowwUnlightofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool SnowwUnlightofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool SnowwUnlightofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SnowwUnlightofDarkWorldActivate()
        {

            return true;
        }

        private bool CeruliGuruofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool CeruliGuruofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool CeruliGuruofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CeruliGuruofDarkWorldActivate()
        {

            return true;
        }

        private bool ScarrScoutofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool ScarrScoutofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool ScarrScoutofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ScarrScoutofDarkWorldActivate()
        {

            return true;
        }

        private bool KahkkiGuerillaofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool KahkkiGuerillaofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool KahkkiGuerillaofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool KahkkiGuerillaofDarkWorldActivate()
        {

            return true;
        }

        private bool GrenTacticianofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool GrenTacticianofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool GrenTacticianofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GrenTacticianofDarkWorldActivate()
        {

            return true;
        }

        private bool BrowwHuntsmanofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool BrowwHuntsmanofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool BrowwHuntsmanofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BrowwHuntsmanofDarkWorldActivate()
        {

            return true;
        }

        private bool BeiigeVanguardofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool BeiigeVanguardofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool BeiigeVanguardofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BeiigeVanguardofDarkWorldActivate()
        {

            return true;
        }

        private bool BrronMadKingofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool BrronMadKingofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool BrronMadKingofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BrronMadKingofDarkWorldActivate()
        {

            return true;
        }

        private bool SillvaWarlordofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool SillvaWarlordofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool SillvaWarlordofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SillvaWarlordofDarkWorldActivate()
        {

            return true;
        }

        private bool GolddWuLordofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool GolddWuLordofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool GolddWuLordofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GolddWuLordofDarkWorldActivate()
        {

            return true;
        }

        private bool ReignBeauxOverlordofDarkWorldNormalSummon()
        {

            return true;
        }

        private bool ReignBeauxOverlordofDarkWorldMonsterSet()
        {

            return true;
        }

        private bool ReignBeauxOverlordofDarkWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ReignBeauxOverlordofDarkWorldActivate()
        {

            return true;
        }

        private bool BelialMarquisofDarknessNormalSummon()
        {

            return true;
        }

        private bool BelialMarquisofDarknessMonsterSet()
        {

            return true;
        }

        private bool BelialMarquisofDarknessRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BelialMarquisofDarknessActivate()
        {

            return true;
        }

        private bool TragoediaNormalSummon()
        {

            return true;
        }

        private bool TragoediaMonsterSet()
        {

            return true;
        }

        private bool TragoediaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TragoediaActivate()
        {

            return true;
        }

        private bool SanganNormalSummon()
        {

            return true;
        }

        private bool SanganMonsterSet()
        {

            return true;
        }

        private bool SanganRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SanganActivate()
        {

            return true;
        }

        private bool NewdoriaNormalSummon()
        {

            return true;
        }

        private bool NewdoriaMonsterSet()
        {

            return true;
        }

        private bool NewdoriaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NewdoriaActivate()
        {

            return true;
        }

        private bool GoblinKingNormalSummon()
        {

            return true;
        }

        private bool GoblinKingMonsterSet()
        {

            return true;
        }

        private bool GoblinKingRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GoblinKingActivate()
        {

            return true;
        }

        private bool GraveSquirmerNormalSummon()
        {

            return true;
        }

        private bool GraveSquirmerMonsterSet()
        {

            return true;
        }

        private bool GraveSquirmerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GraveSquirmerActivate()
        {

            return true;
        }

        private bool CardGuardNormalSummon()
        {

            return true;
        }

        private bool CardGuardMonsterSet()
        {

            return true;
        }

        private bool CardGuardRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CardGuardActivate()
        {

            return true;
        }

        private bool BattleFaderNormalSummon()
        {

            return true;
        }

        private bool BattleFaderMonsterSet()
        {

            return true;
        }

        private bool BattleFaderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BattleFaderActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool TheGatesofDarkWorldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheGatesofDarkWorldActivate()
        {

            return true;
        }

        private bool DarkWorldLightningSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkWorldLightningActivate()
        {

            return true;
        }

        private bool GatewaytoDarkWorldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GatewaytoDarkWorldActivate()
        {

            return true;
        }

        private bool DarkWorldDealingsSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkWorldDealingsActivate()
        {

            return true;
        }

        private bool AllureofDarknessSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AllureofDarknessActivate()
        {

            return true;
        }

        private bool CardDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardDestructionActivate()
        {

            return true;
        }

        private bool TerraformingSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TerraformingActivate()
        {

            return true;
        }

        private bool DarkEruptionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkEruptionActivate()
        {

            return true;
        }

        private bool DarkSchemeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkSchemeActivate()
        {

            return true;
        }

        private bool TheForcesofDarknessSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheForcesofDarknessActivate()
        {

            return true;
        }

        private bool DeckDevastationVirusSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DeckDevastationVirusActivate()
        {

            return true;
        }

        private bool EradicatorEpidemicVirusSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool EradicatorEpidemicVirusActivate()
        {

            return true;
        }

        private bool MindCrushSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MindCrushActivate()
        {

            return true;
        }

        private bool DarkDealSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkDealActivate()
        {

            return true;
        }

        private bool TheTransmigrationProphecySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheTransmigrationProphecyActivate()
        {

            return true;
        }

        private bool EscapefromtheDarkDimensionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool EscapefromtheDarkDimensionActivate()
        {

            return true;
        }

        private bool DarkBribeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkBribeActivate()
        {

            return true;
        }

    }
}