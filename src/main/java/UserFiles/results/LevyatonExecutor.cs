using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Levyaton", "Levyaton")]
    public class LevyatonExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int ChaosDragonLevianeer = 55878038;
            public const int ChaosDragonLevianeer = 55878039;
            public const int LeviaDragonDaedalus = 37721209;
            public const int PikariIgnister = 16020923;
            public const int DoyonIgnister = 88413677;
            public const int FabledRaven = 47217354;
            public const int WeepingIdol = 5763020;
            public const int TuningGum = 82744076;
            public const int HiyariIgnister = 41306080;
            public const int DonyoriboIgnister = 14146794;
            public const int SnowmanEater = 91133740;
            public const int PenguinSoldier = 93920745;
            public const int WarriorofAtlantis = 43797906;
            public const int BlizzardDragon = 61802346;
            public const int HammerShark = 17201174;
            public const int BeelzeusoftheDiabolicDragons = 8763963;
            public const int FabledLeviathan = 39477584;
            public const int BeelzeoftheDiabolicDragons = 34408491;
            public const int Number17LeviathanDragon = 69610924;
            public const int LeviairtheSeaDragon = 95992081;
            public const int TriEdgeLevia = 68836428;
            public const int TheArrivalCyberseIgnister = 11738489;
            public const int DarkTemplarIgnister = 97383507;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            public const int WaterLeviathanIgnister = 37061511;
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int CreatureSwap = 31036355;
            public const int Reasoning = 58577036;
            public const int HeyTrunade = 64697431;
            public const int MonsterReborn = 83764719;
            public const int AIsRitual = 85327820;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int Scapegoat = 73915051;
            public const int WaveMotionCannon = 38992735;
            public const int ALegendaryOcean = 295517;
            public const int MirrorForce = 44095762;
            public const int DrowningMirrorForce = 47475363;
            public const int Metalmorph = 68540058;
            public const int MetalReflectSlime = 26905245;
            public const int GravityBind = 85742772;
            public const int SoulLevy = 87844926;
            // Initialize all useless cards

         }
        public LevyatonExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerRepos);
            AddExecutor(ExecutorType.Activate, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerActivate);
            AddExecutor(ExecutorType.Summon, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerRepos);
            AddExecutor(ExecutorType.Activate, CardId.ChaosDragonLevianeer, ChaosDragonLevianeerActivate);
            AddExecutor(ExecutorType.Summon, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusRepos);
            AddExecutor(ExecutorType.Activate, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusActivate);
            AddExecutor(ExecutorType.Summon, CardId.PikariIgnister, PikariIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PikariIgnister, PikariIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PikariIgnister, PikariIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.PikariIgnister, PikariIgnisterActivate);
            AddExecutor(ExecutorType.Summon, CardId.DoyonIgnister, DoyonIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DoyonIgnister, DoyonIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DoyonIgnister, DoyonIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.DoyonIgnister, DoyonIgnisterActivate);
            AddExecutor(ExecutorType.Summon, CardId.FabledRaven, FabledRavenNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FabledRaven, FabledRavenMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FabledRaven, FabledRavenRepos);
            AddExecutor(ExecutorType.Activate, CardId.FabledRaven, FabledRavenActivate);
            AddExecutor(ExecutorType.Summon, CardId.WeepingIdol, WeepingIdolNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WeepingIdol, WeepingIdolMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WeepingIdol, WeepingIdolRepos);
            AddExecutor(ExecutorType.Activate, CardId.WeepingIdol, WeepingIdolActivate);
            AddExecutor(ExecutorType.Summon, CardId.TuningGum, TuningGumNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TuningGum, TuningGumMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TuningGum, TuningGumRepos);
            AddExecutor(ExecutorType.Activate, CardId.TuningGum, TuningGumActivate);
            AddExecutor(ExecutorType.Summon, CardId.HiyariIgnister, HiyariIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HiyariIgnister, HiyariIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HiyariIgnister, HiyariIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.HiyariIgnister, HiyariIgnisterActivate);
            AddExecutor(ExecutorType.Summon, CardId.DonyoriboIgnister, DonyoriboIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DonyoriboIgnister, DonyoriboIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DonyoriboIgnister, DonyoriboIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.DonyoriboIgnister, DonyoriboIgnisterActivate);
            AddExecutor(ExecutorType.Summon, CardId.SnowmanEater, SnowmanEaterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SnowmanEater, SnowmanEaterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SnowmanEater, SnowmanEaterRepos);
            AddExecutor(ExecutorType.Activate, CardId.SnowmanEater, SnowmanEaterActivate);
            AddExecutor(ExecutorType.Summon, CardId.PenguinSoldier, PenguinSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PenguinSoldier, PenguinSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PenguinSoldier, PenguinSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.PenguinSoldier, PenguinSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.WarriorofAtlantis, WarriorofAtlantisNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WarriorofAtlantis, WarriorofAtlantisMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WarriorofAtlantis, WarriorofAtlantisRepos);
            AddExecutor(ExecutorType.Activate, CardId.WarriorofAtlantis, WarriorofAtlantisActivate);
            AddExecutor(ExecutorType.Summon, CardId.BlizzardDragon, BlizzardDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BlizzardDragon, BlizzardDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BlizzardDragon, BlizzardDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.BlizzardDragon, BlizzardDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.HammerShark, HammerSharkNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HammerShark, HammerSharkMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HammerShark, HammerSharkRepos);
            AddExecutor(ExecutorType.Activate, CardId.HammerShark, HammerSharkActivate);
            AddExecutor(ExecutorType.Summon, CardId.BeelzeusoftheDiabolicDragons, BeelzeusoftheDiabolicDragonsNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BeelzeusoftheDiabolicDragons, BeelzeusoftheDiabolicDragonsMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BeelzeusoftheDiabolicDragons, BeelzeusoftheDiabolicDragonsRepos);
            AddExecutor(ExecutorType.Activate, CardId.BeelzeusoftheDiabolicDragons, BeelzeusoftheDiabolicDragonsActivate);
            AddExecutor(ExecutorType.Summon, CardId.FabledLeviathan, FabledLeviathanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FabledLeviathan, FabledLeviathanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FabledLeviathan, FabledLeviathanRepos);
            AddExecutor(ExecutorType.Activate, CardId.FabledLeviathan, FabledLeviathanActivate);
            AddExecutor(ExecutorType.Summon, CardId.BeelzeoftheDiabolicDragons, BeelzeoftheDiabolicDragonsNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BeelzeoftheDiabolicDragons, BeelzeoftheDiabolicDragonsMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BeelzeoftheDiabolicDragons, BeelzeoftheDiabolicDragonsRepos);
            AddExecutor(ExecutorType.Activate, CardId.BeelzeoftheDiabolicDragons, BeelzeoftheDiabolicDragonsActivate);
            AddExecutor(ExecutorType.Summon, CardId.Number17LeviathanDragon, Number17LeviathanDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Number17LeviathanDragon, Number17LeviathanDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Number17LeviathanDragon, Number17LeviathanDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.Number17LeviathanDragon, Number17LeviathanDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.LeviairtheSeaDragon, LeviairtheSeaDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LeviairtheSeaDragon, LeviairtheSeaDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LeviairtheSeaDragon, LeviairtheSeaDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.LeviairtheSeaDragon, LeviairtheSeaDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.TriEdgeLevia, TriEdgeLeviaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TriEdgeLevia, TriEdgeLeviaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TriEdgeLevia, TriEdgeLeviaRepos);
            AddExecutor(ExecutorType.Activate, CardId.TriEdgeLevia, TriEdgeLeviaActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheArrivalCyberseIgnister, TheArrivalCyberseIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheArrivalCyberseIgnister, TheArrivalCyberseIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheArrivalCyberseIgnister, TheArrivalCyberseIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheArrivalCyberseIgnister, TheArrivalCyberseIgnisterActivate);
            AddExecutor(ExecutorType.Summon, CardId.DarkTemplarIgnister, DarkTemplarIgnisterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DarkTemplarIgnister, DarkTemplarIgnisterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DarkTemplarIgnister, DarkTemplarIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.DarkTemplarIgnister, DarkTemplarIgnisterActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            AddExecutor(ExecutorType.Repos, CardId.WaterLeviathanIgnister, WaterLeviathanIgnisterRepos);
            AddExecutor(ExecutorType.Activate, CardId.WaterLeviathanIgnister, WaterLeviathanIgnisterActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.WaterLeviathanIgnister, WaterLeviathanIgnisterSpSummon);
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Reasoning, ReasoningSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Reasoning, ReasoningActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HeyTrunade, HeyTrunadeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HeyTrunade, HeyTrunadeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MonsterReborn, MonsterRebornSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReborn, MonsterRebornActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AIsRitual, AIsRitualSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AIsRitual, AIsRitualActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Scapegoat, ScapegoatSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Scapegoat, ScapegoatActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.WaveMotionCannon, WaveMotionCannonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.WaveMotionCannon, WaveMotionCannonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ALegendaryOcean, ALegendaryOceanSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ALegendaryOcean, ALegendaryOceanActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, MirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, MirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Metalmorph, MetalmorphSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Metalmorph, MetalmorphActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MetalReflectSlime, MetalReflectSlimeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MetalReflectSlime, MetalReflectSlimeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GravityBind, GravityBindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GravityBind, GravityBindActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SoulLevy, SoulLevySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SoulLevy, SoulLevyActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool ChaosDragonLevianeerNormalSummon()
        {

            return true;
        }

        private bool ChaosDragonLevianeerMonsterSet()
        {

            return true;
        }

        private bool ChaosDragonLevianeerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ChaosDragonLevianeerActivate()
        {

            return true;
        }

        private bool ChaosDragonLevianeerNormalSummon()
        {

            return true;
        }

        private bool ChaosDragonLevianeerMonsterSet()
        {

            return true;
        }

        private bool ChaosDragonLevianeerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ChaosDragonLevianeerActivate()
        {

            return true;
        }

        private bool LeviaDragonDaedalusNormalSummon()
        {

            return true;
        }

        private bool LeviaDragonDaedalusMonsterSet()
        {

            return true;
        }

        private bool LeviaDragonDaedalusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LeviaDragonDaedalusActivate()
        {

            return true;
        }

        private bool PikariIgnisterNormalSummon()
        {

            return true;
        }

        private bool PikariIgnisterMonsterSet()
        {

            return true;
        }

        private bool PikariIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PikariIgnisterActivate()
        {

            return true;
        }

        private bool DoyonIgnisterNormalSummon()
        {

            return true;
        }

        private bool DoyonIgnisterMonsterSet()
        {

            return true;
        }

        private bool DoyonIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DoyonIgnisterActivate()
        {

            return true;
        }

        private bool FabledRavenNormalSummon()
        {

            return true;
        }

        private bool FabledRavenMonsterSet()
        {

            return true;
        }

        private bool FabledRavenRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FabledRavenActivate()
        {

            return true;
        }

        private bool WeepingIdolNormalSummon()
        {

            return true;
        }

        private bool WeepingIdolMonsterSet()
        {

            return true;
        }

        private bool WeepingIdolRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WeepingIdolActivate()
        {

            return true;
        }

        private bool TuningGumNormalSummon()
        {

            return true;
        }

        private bool TuningGumMonsterSet()
        {

            return true;
        }

        private bool TuningGumRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TuningGumActivate()
        {

            return true;
        }

        private bool HiyariIgnisterNormalSummon()
        {

            return true;
        }

        private bool HiyariIgnisterMonsterSet()
        {

            return true;
        }

        private bool HiyariIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HiyariIgnisterActivate()
        {

            return true;
        }

        private bool DonyoriboIgnisterNormalSummon()
        {

            return true;
        }

        private bool DonyoriboIgnisterMonsterSet()
        {

            return true;
        }

        private bool DonyoriboIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DonyoriboIgnisterActivate()
        {

            return true;
        }

        private bool SnowmanEaterNormalSummon()
        {

            return true;
        }

        private bool SnowmanEaterMonsterSet()
        {

            return true;
        }

        private bool SnowmanEaterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SnowmanEaterActivate()
        {

            return true;
        }

        private bool PenguinSoldierNormalSummon()
        {

            return true;
        }

        private bool PenguinSoldierMonsterSet()
        {

            return true;
        }

        private bool PenguinSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PenguinSoldierActivate()
        {

            return true;
        }

        private bool WarriorofAtlantisNormalSummon()
        {

            return true;
        }

        private bool WarriorofAtlantisMonsterSet()
        {

            return true;
        }

        private bool WarriorofAtlantisRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WarriorofAtlantisActivate()
        {

            return true;
        }

        private bool BlizzardDragonNormalSummon()
        {

            return true;
        }

        private bool BlizzardDragonMonsterSet()
        {

            return true;
        }

        private bool BlizzardDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BlizzardDragonActivate()
        {

            return true;
        }

        private bool HammerSharkNormalSummon()
        {

            return true;
        }

        private bool HammerSharkMonsterSet()
        {

            return true;
        }

        private bool HammerSharkRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HammerSharkActivate()
        {

            return true;
        }

        private bool BeelzeusoftheDiabolicDragonsNormalSummon()
        {

            return true;
        }

        private bool BeelzeusoftheDiabolicDragonsMonsterSet()
        {

            return true;
        }

        private bool BeelzeusoftheDiabolicDragonsRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BeelzeusoftheDiabolicDragonsActivate()
        {

            return true;
        }

        private bool FabledLeviathanNormalSummon()
        {

            return true;
        }

        private bool FabledLeviathanMonsterSet()
        {

            return true;
        }

        private bool FabledLeviathanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FabledLeviathanActivate()
        {

            return true;
        }

        private bool BeelzeoftheDiabolicDragonsNormalSummon()
        {

            return true;
        }

        private bool BeelzeoftheDiabolicDragonsMonsterSet()
        {

            return true;
        }

        private bool BeelzeoftheDiabolicDragonsRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BeelzeoftheDiabolicDragonsActivate()
        {

            return true;
        }

        private bool Number17LeviathanDragonNormalSummon()
        {

            return true;
        }

        private bool Number17LeviathanDragonMonsterSet()
        {

            return true;
        }

        private bool Number17LeviathanDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool Number17LeviathanDragonActivate()
        {

            return true;
        }

        private bool LeviairtheSeaDragonNormalSummon()
        {

            return true;
        }

        private bool LeviairtheSeaDragonMonsterSet()
        {

            return true;
        }

        private bool LeviairtheSeaDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LeviairtheSeaDragonActivate()
        {

            return true;
        }

        private bool TriEdgeLeviaNormalSummon()
        {

            return true;
        }

        private bool TriEdgeLeviaMonsterSet()
        {

            return true;
        }

        private bool TriEdgeLeviaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TriEdgeLeviaActivate()
        {

            return true;
        }

        private bool TheArrivalCyberseIgnisterNormalSummon()
        {

            return true;
        }

        private bool TheArrivalCyberseIgnisterMonsterSet()
        {

            return true;
        }

        private bool TheArrivalCyberseIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheArrivalCyberseIgnisterActivate()
        {

            return true;
        }

        private bool DarkTemplarIgnisterNormalSummon()
        {

            return true;
        }

        private bool DarkTemplarIgnisterMonsterSet()
        {

            return true;
        }

        private bool DarkTemplarIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DarkTemplarIgnisterActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

        private bool WaterLeviathanIgnisterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WaterLeviathanIgnisterActivate()
        {

            return true;
        }

        private bool WaterLeviathanIgnisterSpSummon()
        {

            return true;
        }

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool ReasoningSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ReasoningActivate()
        {

            return true;
        }

        private bool HeyTrunadeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HeyTrunadeActivate()
        {

            return true;
        }

        private bool MonsterRebornSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MonsterRebornActivate()
        {

            return true;
        }

        private bool AIsRitualSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AIsRitualActivate()
        {

            return true;
        }

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool ScapegoatSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ScapegoatActivate()
        {

            return true;
        }

        private bool WaveMotionCannonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WaveMotionCannonActivate()
        {

            return true;
        }

        private bool ALegendaryOceanSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ALegendaryOceanActivate()
        {

            return true;
        }

        private bool MirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MirrorForceActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

        private bool MetalmorphSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MetalmorphActivate()
        {

            return true;
        }

        private bool MetalReflectSlimeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MetalReflectSlimeActivate()
        {

            return true;
        }

        private bool GravityBindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GravityBindActivate()
        {

            return true;
        }

        private bool SoulLevySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SoulLevyActivate()
        {

            return true;
        }

    }
}