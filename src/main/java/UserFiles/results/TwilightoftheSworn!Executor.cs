using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Twilight of the Sworn!", "TwilightoftheSworn")]
    public class TwilightoftheSwornExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int PunishmentDragon = 19959563;
            public const int GarothLightswornWarrior = 59019082;
            public const int JainTwilightswornGeneral = 84673417;
            public const int JainLightswornPaladin = 96235275;
            public const int RaidenHandoftheLightsworn = 77558536;
            public const int LylaTwilightswornEnchantress = 10071151;
            public const int LylaLightswornSorceress = 22624373;
            public const int EhrenLightswornMonk = 44178886;
            public const int LuminaTwilightswornShaman = 56166150;
            public const int ShireLightswornSpirit = 2420921;
            public const int RykoLightswornHunter = 21502796;
            public const int RykoTwilightswornFighter = 83550869;
            public const int MichaeltheArchLightsworn = 4779823;
            public const int MinervatheExaltedLightsworn = 30100551;
            public const int CurioustheLightswornDominion = 98095162;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int SolarRecharge = 691925;
            public const int MarchoftheDarkBrigade = 24037702;
            public const int CreatureSwap = 31036355;
            public const int ChargeoftheLightBrigade = 94886282;
            public const int LightswornSabre = 30502181;
            public const int RealmofLight = 36099620;
            public const int DrowningMirrorForce = 47475363;
            public const int LightswornBarrier = 22201234;
            public const int LightSpiral = 35577420;
            public const int GloriousIllusion = 61962135;
            // Initialize all useless cards

         }
        public Twilight of the Sworn!Executor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.PunishmentDragon, PunishmentDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PunishmentDragon, PunishmentDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PunishmentDragon, PunishmentDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.PunishmentDragon, PunishmentDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.GarothLightswornWarrior, GarothLightswornWarriorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GarothLightswornWarrior, GarothLightswornWarriorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GarothLightswornWarrior, GarothLightswornWarriorRepos);
            AddExecutor(ExecutorType.Activate, CardId.GarothLightswornWarrior, GarothLightswornWarriorActivate);
            AddExecutor(ExecutorType.Summon, CardId.JainTwilightswornGeneral, JainTwilightswornGeneralNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.JainTwilightswornGeneral, JainTwilightswornGeneralMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.JainTwilightswornGeneral, JainTwilightswornGeneralRepos);
            AddExecutor(ExecutorType.Activate, CardId.JainTwilightswornGeneral, JainTwilightswornGeneralActivate);
            AddExecutor(ExecutorType.Summon, CardId.JainLightswornPaladin, JainLightswornPaladinNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.JainLightswornPaladin, JainLightswornPaladinMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.JainLightswornPaladin, JainLightswornPaladinRepos);
            AddExecutor(ExecutorType.Activate, CardId.JainLightswornPaladin, JainLightswornPaladinActivate);
            AddExecutor(ExecutorType.Summon, CardId.RaidenHandoftheLightsworn, RaidenHandoftheLightswornNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RaidenHandoftheLightsworn, RaidenHandoftheLightswornMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RaidenHandoftheLightsworn, RaidenHandoftheLightswornRepos);
            AddExecutor(ExecutorType.Activate, CardId.RaidenHandoftheLightsworn, RaidenHandoftheLightswornActivate);
            AddExecutor(ExecutorType.Summon, CardId.LylaTwilightswornEnchantress, LylaTwilightswornEnchantressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LylaTwilightswornEnchantress, LylaTwilightswornEnchantressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LylaTwilightswornEnchantress, LylaTwilightswornEnchantressRepos);
            AddExecutor(ExecutorType.Activate, CardId.LylaTwilightswornEnchantress, LylaTwilightswornEnchantressActivate);
            AddExecutor(ExecutorType.Summon, CardId.LylaLightswornSorceress, LylaLightswornSorceressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LylaLightswornSorceress, LylaLightswornSorceressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LylaLightswornSorceress, LylaLightswornSorceressRepos);
            AddExecutor(ExecutorType.Activate, CardId.LylaLightswornSorceress, LylaLightswornSorceressActivate);
            AddExecutor(ExecutorType.Summon, CardId.EhrenLightswornMonk, EhrenLightswornMonkNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EhrenLightswornMonk, EhrenLightswornMonkMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EhrenLightswornMonk, EhrenLightswornMonkRepos);
            AddExecutor(ExecutorType.Activate, CardId.EhrenLightswornMonk, EhrenLightswornMonkActivate);
            AddExecutor(ExecutorType.Summon, CardId.LuminaTwilightswornShaman, LuminaTwilightswornShamanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LuminaTwilightswornShaman, LuminaTwilightswornShamanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LuminaTwilightswornShaman, LuminaTwilightswornShamanRepos);
            AddExecutor(ExecutorType.Activate, CardId.LuminaTwilightswornShaman, LuminaTwilightswornShamanActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShireLightswornSpirit, ShireLightswornSpiritNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShireLightswornSpirit, ShireLightswornSpiritMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShireLightswornSpirit, ShireLightswornSpiritRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShireLightswornSpirit, ShireLightswornSpiritActivate);
            AddExecutor(ExecutorType.Summon, CardId.RykoLightswornHunter, RykoLightswornHunterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RykoLightswornHunter, RykoLightswornHunterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RykoLightswornHunter, RykoLightswornHunterRepos);
            AddExecutor(ExecutorType.Activate, CardId.RykoLightswornHunter, RykoLightswornHunterActivate);
            AddExecutor(ExecutorType.Summon, CardId.RykoTwilightswornFighter, RykoTwilightswornFighterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RykoTwilightswornFighter, RykoTwilightswornFighterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RykoTwilightswornFighter, RykoTwilightswornFighterRepos);
            AddExecutor(ExecutorType.Activate, CardId.RykoTwilightswornFighter, RykoTwilightswornFighterActivate);
            AddExecutor(ExecutorType.Summon, CardId.MichaeltheArchLightsworn, MichaeltheArchLightswornNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MichaeltheArchLightsworn, MichaeltheArchLightswornMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MichaeltheArchLightsworn, MichaeltheArchLightswornRepos);
            AddExecutor(ExecutorType.Activate, CardId.MichaeltheArchLightsworn, MichaeltheArchLightswornActivate);
            AddExecutor(ExecutorType.Summon, CardId.MinervatheExaltedLightsworn, MinervatheExaltedLightswornNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MinervatheExaltedLightsworn, MinervatheExaltedLightswornMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MinervatheExaltedLightsworn, MinervatheExaltedLightswornRepos);
            AddExecutor(ExecutorType.Activate, CardId.MinervatheExaltedLightsworn, MinervatheExaltedLightswornActivate);
            AddExecutor(ExecutorType.Summon, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionRepos);
            AddExecutor(ExecutorType.Activate, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.SolarRecharge, SolarRechargeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SolarRecharge, SolarRechargeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MarchoftheDarkBrigade, MarchoftheDarkBrigadeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MarchoftheDarkBrigade, MarchoftheDarkBrigadeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ChargeoftheLightBrigade, ChargeoftheLightBrigadeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ChargeoftheLightBrigade, ChargeoftheLightBrigadeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightswornSabre, LightswornSabreSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightswornSabre, LightswornSabreActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RealmofLight, RealmofLightSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RealmofLight, RealmofLightActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightswornBarrier, LightswornBarrierSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightswornBarrier, LightswornBarrierActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightSpiral, LightSpiralSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightSpiral, LightSpiralActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GloriousIllusion, GloriousIllusionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GloriousIllusion, GloriousIllusionActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool PunishmentDragonNormalSummon()
        {

            return true;
        }

        private bool PunishmentDragonMonsterSet()
        {

            return true;
        }

        private bool PunishmentDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PunishmentDragonActivate()
        {

            return true;
        }

        private bool GarothLightswornWarriorNormalSummon()
        {

            return true;
        }

        private bool GarothLightswornWarriorMonsterSet()
        {

            return true;
        }

        private bool GarothLightswornWarriorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GarothLightswornWarriorActivate()
        {

            return true;
        }

        private bool JainTwilightswornGeneralNormalSummon()
        {

            return true;
        }

        private bool JainTwilightswornGeneralMonsterSet()
        {

            return true;
        }

        private bool JainTwilightswornGeneralRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool JainTwilightswornGeneralActivate()
        {

            return true;
        }

        private bool JainLightswornPaladinNormalSummon()
        {

            return true;
        }

        private bool JainLightswornPaladinMonsterSet()
        {

            return true;
        }

        private bool JainLightswornPaladinRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool JainLightswornPaladinActivate()
        {

            return true;
        }

        private bool RaidenHandoftheLightswornNormalSummon()
        {

            return true;
        }

        private bool RaidenHandoftheLightswornMonsterSet()
        {

            return true;
        }

        private bool RaidenHandoftheLightswornRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RaidenHandoftheLightswornActivate()
        {

            return true;
        }

        private bool LylaTwilightswornEnchantressNormalSummon()
        {

            return true;
        }

        private bool LylaTwilightswornEnchantressMonsterSet()
        {

            return true;
        }

        private bool LylaTwilightswornEnchantressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LylaTwilightswornEnchantressActivate()
        {

            return true;
        }

        private bool LylaLightswornSorceressNormalSummon()
        {

            return true;
        }

        private bool LylaLightswornSorceressMonsterSet()
        {

            return true;
        }

        private bool LylaLightswornSorceressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LylaLightswornSorceressActivate()
        {

            return true;
        }

        private bool EhrenLightswornMonkNormalSummon()
        {

            return true;
        }

        private bool EhrenLightswornMonkMonsterSet()
        {

            return true;
        }

        private bool EhrenLightswornMonkRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EhrenLightswornMonkActivate()
        {

            return true;
        }

        private bool LuminaTwilightswornShamanNormalSummon()
        {

            return true;
        }

        private bool LuminaTwilightswornShamanMonsterSet()
        {

            return true;
        }

        private bool LuminaTwilightswornShamanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LuminaTwilightswornShamanActivate()
        {

            return true;
        }

        private bool ShireLightswornSpiritNormalSummon()
        {

            return true;
        }

        private bool ShireLightswornSpiritMonsterSet()
        {

            return true;
        }

        private bool ShireLightswornSpiritRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShireLightswornSpiritActivate()
        {

            return true;
        }

        private bool RykoLightswornHunterNormalSummon()
        {

            return true;
        }

        private bool RykoLightswornHunterMonsterSet()
        {

            return true;
        }

        private bool RykoLightswornHunterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RykoLightswornHunterActivate()
        {

            return true;
        }

        private bool RykoTwilightswornFighterNormalSummon()
        {

            return true;
        }

        private bool RykoTwilightswornFighterMonsterSet()
        {

            return true;
        }

        private bool RykoTwilightswornFighterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RykoTwilightswornFighterActivate()
        {

            return true;
        }

        private bool MichaeltheArchLightswornNormalSummon()
        {

            return true;
        }

        private bool MichaeltheArchLightswornMonsterSet()
        {

            return true;
        }

        private bool MichaeltheArchLightswornRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MichaeltheArchLightswornActivate()
        {

            return true;
        }

        private bool MinervatheExaltedLightswornNormalSummon()
        {

            return true;
        }

        private bool MinervatheExaltedLightswornMonsterSet()
        {

            return true;
        }

        private bool MinervatheExaltedLightswornRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MinervatheExaltedLightswornActivate()
        {

            return true;
        }

        private bool CurioustheLightswornDominionNormalSummon()
        {

            return true;
        }

        private bool CurioustheLightswornDominionMonsterSet()
        {

            return true;
        }

        private bool CurioustheLightswornDominionRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CurioustheLightswornDominionActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool SolarRechargeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SolarRechargeActivate()
        {

            return true;
        }

        private bool MarchoftheDarkBrigadeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MarchoftheDarkBrigadeActivate()
        {

            return true;
        }

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool ChargeoftheLightBrigadeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ChargeoftheLightBrigadeActivate()
        {

            return true;
        }

        private bool LightswornSabreSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightswornSabreActivate()
        {

            return true;
        }

        private bool RealmofLightSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RealmofLightActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

        private bool LightswornBarrierSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightswornBarrierActivate()
        {

            return true;
        }

        private bool LightSpiralSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightSpiralActivate()
        {

            return true;
        }

        private bool GloriousIllusionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GloriousIllusionActivate()
        {

            return true;
        }

    }
}