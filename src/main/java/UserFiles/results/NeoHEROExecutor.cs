using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Neo HERO", "NeoHERO")]
    public class NeoHEROExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int ElementalHERONeos = 89943723;
            public const int ElementalHERONeos = 89943724;
            // Initialize all effect monsters
            public const int ElementalHEROHonestNeos = 14124483;
            public const int ElementalHERONeosAlius = 69884162;
            public const int NeoSpacePathfinder = 19594506;
            public const int ElementalHEROStratos = 40044918;
            public const int ElementalHEROPrisma = 89312388;
            public const int ElementalHEROBlazeman = 63060238;
            public const int NeoSpaceConnector = 85840608;
            public const int NeoSpacianGrandMole = 80344569;
            public const int NeoSpacianAirHummingbird = 54959865;
            public const int NeoSpacianAquaDolphin = 17955766;
            public const int NeoSpacianFlareScarab = 89621922;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            public const int ElementalHEROSunrise = 22908820;
            public const int ElementalHEROMagmaNeos = 78512663;
            public const int ElementalHEROAirNeos = 11502550;
            public const int ElementalHEROStormNeos = 49352945;
            public const int ElementalHEROAquaNeos = 55171412;
            public const int ElementalHEROBraveNeos = 64655485;
            public const int ElementalHEROGrandNeos = 48996569;
            public const int ElementalHEROFlareNeos = 81566151;
            public const int ElementalHERONeosKnight = 72926163;
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int NeosFusion = 14088859;
            public const int Polymerization = 24094653;
            public const int Polymerization = 27847700;
            public const int MiracleContact = 35255456;
            public const int MonsterReborn = 83764719;
            public const int InstantNeoSpace = 11913700;
            public const int NeoSpace = 42015635;
            public const int DrowningMirrorForce = 47475363;
            public const int NEXT = 74414885;
            public const int CalloftheHaunted = 97077563;
            // Initialize all useless cards

         }
        public Neo HEROExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.ElementalHERONeos, ElementalHERONeosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHERONeos, ElementalHERONeosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHERONeos, ElementalHERONeosRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHERONeos, ElementalHERONeosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHERONeos, ElementalHERONeosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHERONeos, ElementalHERONeosRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROHonestNeos, ElementalHEROHonestNeosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROHonestNeos, ElementalHEROHonestNeosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROHonestNeos, ElementalHEROHonestNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROHonestNeos, ElementalHEROHonestNeosActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHERONeosAlius, ElementalHERONeosAliusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHERONeosAlius, ElementalHERONeosAliusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHERONeosAlius, ElementalHERONeosAliusRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHERONeosAlius, ElementalHERONeosAliusActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpacePathfinder, NeoSpacePathfinderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpacePathfinder, NeoSpacePathfinderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpacePathfinder, NeoSpacePathfinderRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpacePathfinder, NeoSpacePathfinderActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROStratos, ElementalHEROStratosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROStratos, ElementalHEROStratosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROStratos, ElementalHEROStratosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROStratos, ElementalHEROStratosActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROPrisma, ElementalHEROPrismaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROPrisma, ElementalHEROPrismaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROPrisma, ElementalHEROPrismaRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROPrisma, ElementalHEROPrismaActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROBlazeman, ElementalHEROBlazemanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROBlazeman, ElementalHEROBlazemanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROBlazeman, ElementalHEROBlazemanRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROBlazeman, ElementalHEROBlazemanActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpaceConnector, NeoSpaceConnectorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpaceConnector, NeoSpaceConnectorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpaceConnector, NeoSpaceConnectorRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpaceConnector, NeoSpaceConnectorActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpacianGrandMole, NeoSpacianGrandMoleNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpacianGrandMole, NeoSpacianGrandMoleMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpacianGrandMole, NeoSpacianGrandMoleRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpacianGrandMole, NeoSpacianGrandMoleActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpacianAirHummingbird, NeoSpacianAirHummingbirdNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpacianAirHummingbird, NeoSpacianAirHummingbirdMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpacianAirHummingbird, NeoSpacianAirHummingbirdRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpacianAirHummingbird, NeoSpacianAirHummingbirdActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpacianAquaDolphin, NeoSpacianAquaDolphinNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpacianAquaDolphin, NeoSpacianAquaDolphinMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpacianAquaDolphin, NeoSpacianAquaDolphinRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpacianAquaDolphin, NeoSpacianAquaDolphinActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeoSpacianFlareScarab, NeoSpacianFlareScarabNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeoSpacianFlareScarab, NeoSpacianFlareScarabMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeoSpacianFlareScarab, NeoSpacianFlareScarabRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpacianFlareScarab, NeoSpacianFlareScarabActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROSunrise, ElementalHEROSunriseRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROSunrise, ElementalHEROSunriseActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROSunrise, ElementalHEROSunriseSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROMagmaNeos, ElementalHEROMagmaNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROMagmaNeos, ElementalHEROMagmaNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROMagmaNeos, ElementalHEROMagmaNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROAirNeos, ElementalHEROAirNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROAirNeos, ElementalHEROAirNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROAirNeos, ElementalHEROAirNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROStormNeos, ElementalHEROStormNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROStormNeos, ElementalHEROStormNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROStormNeos, ElementalHEROStormNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROAquaNeos, ElementalHEROAquaNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROAquaNeos, ElementalHEROAquaNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROAquaNeos, ElementalHEROAquaNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROBraveNeos, ElementalHEROBraveNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROBraveNeos, ElementalHEROBraveNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROBraveNeos, ElementalHEROBraveNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROGrandNeos, ElementalHEROGrandNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROGrandNeos, ElementalHEROGrandNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROGrandNeos, ElementalHEROGrandNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROFlareNeos, ElementalHEROFlareNeosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROFlareNeos, ElementalHEROFlareNeosActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROFlareNeos, ElementalHEROFlareNeosSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHERONeosKnight, ElementalHERONeosKnightRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHERONeosKnight, ElementalHERONeosKnightActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHERONeosKnight, ElementalHERONeosKnightSpSummon);
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.NeosFusion, NeosFusionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.NeosFusion, NeosFusionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Polymerization, PolymerizationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Polymerization, PolymerizationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Polymerization, PolymerizationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Polymerization, PolymerizationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MiracleContact, MiracleContactSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MiracleContact, MiracleContactActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MonsterReborn, MonsterRebornSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReborn, MonsterRebornActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.InstantNeoSpace, InstantNeoSpaceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.InstantNeoSpace, InstantNeoSpaceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.NeoSpace, NeoSpaceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.NeoSpace, NeoSpaceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.NEXT, NEXTSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.NEXT, NEXTActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);

         }

            // All Normal Monster Methods

        private bool ElementalHERONeosNormalSummon()
        {

            return true;
        }

        private bool ElementalHERONeosMonsterSet()
        {

            return true;
        }

        private bool ElementalHERONeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHERONeosNormalSummon()
        {

            return true;
        }

        private bool ElementalHERONeosMonsterSet()
        {

            return true;
        }

        private bool ElementalHERONeosRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool ElementalHEROHonestNeosNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROHonestNeosMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROHonestNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROHonestNeosActivate()
        {

            return true;
        }

        private bool ElementalHERONeosAliusNormalSummon()
        {

            return true;
        }

        private bool ElementalHERONeosAliusMonsterSet()
        {

            return true;
        }

        private bool ElementalHERONeosAliusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHERONeosAliusActivate()
        {

            return true;
        }

        private bool NeoSpacePathfinderNormalSummon()
        {

            return true;
        }

        private bool NeoSpacePathfinderMonsterSet()
        {

            return true;
        }

        private bool NeoSpacePathfinderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpacePathfinderActivate()
        {

            return true;
        }

        private bool ElementalHEROStratosNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROStratosMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROStratosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROStratosActivate()
        {

            return true;
        }

        private bool ElementalHEROPrismaNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROPrismaMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROPrismaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROPrismaActivate()
        {

            return true;
        }

        private bool ElementalHEROBlazemanNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROBlazemanMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROBlazemanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROBlazemanActivate()
        {

            return true;
        }

        private bool NeoSpaceConnectorNormalSummon()
        {

            return true;
        }

        private bool NeoSpaceConnectorMonsterSet()
        {

            return true;
        }

        private bool NeoSpaceConnectorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpaceConnectorActivate()
        {

            return true;
        }

        private bool NeoSpacianGrandMoleNormalSummon()
        {

            return true;
        }

        private bool NeoSpacianGrandMoleMonsterSet()
        {

            return true;
        }

        private bool NeoSpacianGrandMoleRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpacianGrandMoleActivate()
        {

            return true;
        }

        private bool NeoSpacianAirHummingbirdNormalSummon()
        {

            return true;
        }

        private bool NeoSpacianAirHummingbirdMonsterSet()
        {

            return true;
        }

        private bool NeoSpacianAirHummingbirdRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpacianAirHummingbirdActivate()
        {

            return true;
        }

        private bool NeoSpacianAquaDolphinNormalSummon()
        {

            return true;
        }

        private bool NeoSpacianAquaDolphinMonsterSet()
        {

            return true;
        }

        private bool NeoSpacianAquaDolphinRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpacianAquaDolphinActivate()
        {

            return true;
        }

        private bool NeoSpacianFlareScarabNormalSummon()
        {

            return true;
        }

        private bool NeoSpacianFlareScarabMonsterSet()
        {

            return true;
        }

        private bool NeoSpacianFlareScarabRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeoSpacianFlareScarabActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

        private bool ElementalHEROSunriseRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROSunriseActivate()
        {

            return true;
        }

        private bool ElementalHEROSunriseSpSummon()
        {

            return true;
        }

        private bool ElementalHEROMagmaNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROMagmaNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROMagmaNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROAirNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROAirNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROAirNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROStormNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROStormNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROStormNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROAquaNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROAquaNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROAquaNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROBraveNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROBraveNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROBraveNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROGrandNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROGrandNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROGrandNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHEROFlareNeosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROFlareNeosActivate()
        {

            return true;
        }

        private bool ElementalHEROFlareNeosSpSummon()
        {

            return true;
        }

        private bool ElementalHERONeosKnightRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHERONeosKnightActivate()
        {

            return true;
        }

        private bool ElementalHERONeosKnightSpSummon()
        {

            return true;
        }

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool NeosFusionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NeosFusionActivate()
        {

            return true;
        }

        private bool PolymerizationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PolymerizationActivate()
        {

            return true;
        }

        private bool PolymerizationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PolymerizationActivate()
        {

            return true;
        }

        private bool MiracleContactSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MiracleContactActivate()
        {

            return true;
        }

        private bool MonsterRebornSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MonsterRebornActivate()
        {

            return true;
        }

        private bool InstantNeoSpaceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool InstantNeoSpaceActivate()
        {

            return true;
        }

        private bool NeoSpaceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NeoSpaceActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

        private bool NEXTSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool NEXTActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

    }
}