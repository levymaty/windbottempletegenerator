using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("HERO of EVIL", "HEROofEVIL")]
    public class HEROofEVILExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int ElementalHEROAvian = 21844577;
            public const int ElementalHEROBurstinatrix = 58932616;
            public const int ElementalHEROBurstinatrix = 58932615;
            public const int ElementalHEROAvian = 21844576;
            public const int ElementalHEROClayman = 84327329;
            public const int ElementalHEROSparkman = 20721928;
            public const int ElementalHEROSparkman = 20721929;
            // Initialize all effect monsters
            public const int ElementalHEROPrisma = 89312388;
            public const int ElementalHEROStratos = 40044918;
            public const int ElementalHEROSolidSoldier = 45195443;
            public const int EvilHEROInfernalGainer = 95943058;
            public const int EvilHEROInfernalProdigy = 50304345;
            public const int EvilHEROMaliciousEdge = 58554959;
            public const int EvilHEROSinisterNecrom = 45659520;
            public const int TheDarkHexSealedFusion = 52101615;
            public const int EvilHEROAdustedGold = 13650422;
            public const int XtraHEROInfernalDevicer = 19324993;
            public const int XtraHERODreadDecimator = 63813056;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            public const int ElementalHEROGrandmerge = 36841733;
            public const int ElementalHEROElectrum = 29343734;
            public const int EvilHEROMaliciousBane = 86165817;
            public const int EvilHEROMaliciousFiend = 86676862;
            public const int EvilHERODarkGaia = 58332301;
            public const int EvilHEROInfernoWing = 22160245;
            public const int EvilHEROLightningGolem = 21947653;
            public const int EvilHEROInfernalSniper = 50282757;
            public const int VisionHEROAdoration = 45170821;
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int CreatureSwap = 31036355;
            public const int DarkCalling = 12071500;
            public const int DarkFusion = 94820406;
            public const int EvilMind = 18438874;
            public const int SupremeKingsCastle = 72043279;
            public const int FoolishBurial = 81439173;
            public const int FutureFusion = 77565204;
            public const int HeroMask = 75141056;
            public const int LegacyofaHERO = 25614410;
            public const int MonsterReborn = 83764719;
            public const int Polymerization = 24094653;
            public const int ReinforcementoftheArmy = 32807846;
            public const int DrowningMirrorForce = 47475363;
            public const int CalloftheHaunted = 97077563;
            // Initialize all useless cards

         }
        public HERO of EVILExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROAvian, ElementalHEROAvianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROAvian, ElementalHEROAvianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROAvian, ElementalHEROAvianRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROBurstinatrix, ElementalHEROBurstinatrixRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROAvian, ElementalHEROAvianNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROAvian, ElementalHEROAvianMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROAvian, ElementalHEROAvianRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROClayman, ElementalHEROClaymanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROClayman, ElementalHEROClaymanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROClayman, ElementalHEROClaymanRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROSparkman, ElementalHEROSparkmanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROSparkman, ElementalHEROSparkmanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROSparkman, ElementalHEROSparkmanRepos);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROSparkman, ElementalHEROSparkmanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROSparkman, ElementalHEROSparkmanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROSparkman, ElementalHEROSparkmanRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROPrisma, ElementalHEROPrismaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROPrisma, ElementalHEROPrismaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROPrisma, ElementalHEROPrismaRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROPrisma, ElementalHEROPrismaActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROStratos, ElementalHEROStratosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROStratos, ElementalHEROStratosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROStratos, ElementalHEROStratosRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROStratos, ElementalHEROStratosActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElementalHEROSolidSoldier, ElementalHEROSolidSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElementalHEROSolidSoldier, ElementalHEROSolidSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROSolidSoldier, ElementalHEROSolidSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROSolidSoldier, ElementalHEROSolidSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilHEROInfernalGainer, EvilHEROInfernalGainerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilHEROInfernalGainer, EvilHEROInfernalGainerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROInfernalGainer, EvilHEROInfernalGainerRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROInfernalGainer, EvilHEROInfernalGainerActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilHEROInfernalProdigy, EvilHEROInfernalProdigyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilHEROInfernalProdigy, EvilHEROInfernalProdigyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROInfernalProdigy, EvilHEROInfernalProdigyRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROInfernalProdigy, EvilHEROInfernalProdigyActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilHEROMaliciousEdge, EvilHEROMaliciousEdgeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilHEROMaliciousEdge, EvilHEROMaliciousEdgeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROMaliciousEdge, EvilHEROMaliciousEdgeRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROMaliciousEdge, EvilHEROMaliciousEdgeActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilHEROSinisterNecrom, EvilHEROSinisterNecromNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilHEROSinisterNecrom, EvilHEROSinisterNecromMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROSinisterNecrom, EvilHEROSinisterNecromRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROSinisterNecrom, EvilHEROSinisterNecromActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheDarkHexSealedFusion, TheDarkHexSealedFusionNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheDarkHexSealedFusion, TheDarkHexSealedFusionMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheDarkHexSealedFusion, TheDarkHexSealedFusionRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheDarkHexSealedFusion, TheDarkHexSealedFusionActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilHEROAdustedGold, EvilHEROAdustedGoldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilHEROAdustedGold, EvilHEROAdustedGoldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROAdustedGold, EvilHEROAdustedGoldRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROAdustedGold, EvilHEROAdustedGoldActivate);
            AddExecutor(ExecutorType.Summon, CardId.XtraHEROInfernalDevicer, XtraHEROInfernalDevicerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.XtraHEROInfernalDevicer, XtraHEROInfernalDevicerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.XtraHEROInfernalDevicer, XtraHEROInfernalDevicerRepos);
            AddExecutor(ExecutorType.Activate, CardId.XtraHEROInfernalDevicer, XtraHEROInfernalDevicerActivate);
            AddExecutor(ExecutorType.Summon, CardId.XtraHERODreadDecimator, XtraHERODreadDecimatorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.XtraHERODreadDecimator, XtraHERODreadDecimatorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.XtraHERODreadDecimator, XtraHERODreadDecimatorRepos);
            AddExecutor(ExecutorType.Activate, CardId.XtraHERODreadDecimator, XtraHERODreadDecimatorActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROGrandmerge, ElementalHEROGrandmergeRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROGrandmerge, ElementalHEROGrandmergeActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROGrandmerge, ElementalHEROGrandmergeSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.ElementalHEROElectrum, ElementalHEROElectrumRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElementalHEROElectrum, ElementalHEROElectrumActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.ElementalHEROElectrum, ElementalHEROElectrumSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROMaliciousBane, EvilHEROMaliciousBaneRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROMaliciousBane, EvilHEROMaliciousBaneActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHEROMaliciousBane, EvilHEROMaliciousBaneSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROMaliciousFiend, EvilHEROMaliciousFiendRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROMaliciousFiend, EvilHEROMaliciousFiendActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHEROMaliciousFiend, EvilHEROMaliciousFiendSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHERODarkGaia, EvilHERODarkGaiaRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHERODarkGaia, EvilHERODarkGaiaActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHERODarkGaia, EvilHERODarkGaiaSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROInfernoWing, EvilHEROInfernoWingRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROInfernoWing, EvilHEROInfernoWingActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHEROInfernoWing, EvilHEROInfernoWingSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROLightningGolem, EvilHEROLightningGolemRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROLightningGolem, EvilHEROLightningGolemActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHEROLightningGolem, EvilHEROLightningGolemSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.EvilHEROInfernalSniper, EvilHEROInfernalSniperRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilHEROInfernalSniper, EvilHEROInfernalSniperActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.EvilHEROInfernalSniper, EvilHEROInfernalSniperSpSummon);
            AddExecutor(ExecutorType.Repos, CardId.VisionHEROAdoration, VisionHEROAdorationRepos);
            AddExecutor(ExecutorType.Activate, CardId.VisionHEROAdoration, VisionHEROAdorationActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.VisionHEROAdoration, VisionHEROAdorationSpSummon);
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkCalling, DarkCallingSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkCalling, DarkCallingActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkFusion, DarkFusionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkFusion, DarkFusionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.EvilMind, EvilMindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.EvilMind, EvilMindActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SupremeKingsCastle, SupremeKingsCastleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SupremeKingsCastle, SupremeKingsCastleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FoolishBurial, FoolishBurialSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FoolishBurial, FoolishBurialActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FutureFusion, FutureFusionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FutureFusion, FutureFusionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HeroMask, HeroMaskSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HeroMask, HeroMaskActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LegacyofaHERO, LegacyofaHEROSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LegacyofaHERO, LegacyofaHEROActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MonsterReborn, MonsterRebornSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReborn, MonsterRebornActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Polymerization, PolymerizationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Polymerization, PolymerizationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ReinforcementoftheArmy, ReinforcementoftheArmySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ReinforcementoftheArmy, ReinforcementoftheArmyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);

         }

            // All Normal Monster Methods

        private bool ElementalHEROAvianNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROAvianMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROAvianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROBurstinatrixNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROBurstinatrixMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROBurstinatrixRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROBurstinatrixNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROBurstinatrixMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROBurstinatrixRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROAvianNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROAvianMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROAvianRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROClaymanNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROClaymanMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROClaymanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROSparkmanNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROSparkmanMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROSparkmanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROSparkmanNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROSparkmanMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROSparkmanRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool ElementalHEROPrismaNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROPrismaMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROPrismaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROPrismaActivate()
        {

            return true;
        }

        private bool ElementalHEROStratosNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROStratosMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROStratosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROStratosActivate()
        {

            return true;
        }

        private bool ElementalHEROSolidSoldierNormalSummon()
        {

            return true;
        }

        private bool ElementalHEROSolidSoldierMonsterSet()
        {

            return true;
        }

        private bool ElementalHEROSolidSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROSolidSoldierActivate()
        {

            return true;
        }

        private bool EvilHEROInfernalGainerNormalSummon()
        {

            return true;
        }

        private bool EvilHEROInfernalGainerMonsterSet()
        {

            return true;
        }

        private bool EvilHEROInfernalGainerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROInfernalGainerActivate()
        {

            return true;
        }

        private bool EvilHEROInfernalProdigyNormalSummon()
        {

            return true;
        }

        private bool EvilHEROInfernalProdigyMonsterSet()
        {

            return true;
        }

        private bool EvilHEROInfernalProdigyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROInfernalProdigyActivate()
        {

            return true;
        }

        private bool EvilHEROMaliciousEdgeNormalSummon()
        {

            return true;
        }

        private bool EvilHEROMaliciousEdgeMonsterSet()
        {

            return true;
        }

        private bool EvilHEROMaliciousEdgeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROMaliciousEdgeActivate()
        {

            return true;
        }

        private bool EvilHEROSinisterNecromNormalSummon()
        {

            return true;
        }

        private bool EvilHEROSinisterNecromMonsterSet()
        {

            return true;
        }

        private bool EvilHEROSinisterNecromRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROSinisterNecromActivate()
        {

            return true;
        }

        private bool TheDarkHexSealedFusionNormalSummon()
        {

            return true;
        }

        private bool TheDarkHexSealedFusionMonsterSet()
        {

            return true;
        }

        private bool TheDarkHexSealedFusionRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheDarkHexSealedFusionActivate()
        {

            return true;
        }

        private bool EvilHEROAdustedGoldNormalSummon()
        {

            return true;
        }

        private bool EvilHEROAdustedGoldMonsterSet()
        {

            return true;
        }

        private bool EvilHEROAdustedGoldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROAdustedGoldActivate()
        {

            return true;
        }

        private bool XtraHEROInfernalDevicerNormalSummon()
        {

            return true;
        }

        private bool XtraHEROInfernalDevicerMonsterSet()
        {

            return true;
        }

        private bool XtraHEROInfernalDevicerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool XtraHEROInfernalDevicerActivate()
        {

            return true;
        }

        private bool XtraHERODreadDecimatorNormalSummon()
        {

            return true;
        }

        private bool XtraHERODreadDecimatorMonsterSet()
        {

            return true;
        }

        private bool XtraHERODreadDecimatorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool XtraHERODreadDecimatorActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

        private bool ElementalHEROGrandmergeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROGrandmergeActivate()
        {

            return true;
        }

        private bool ElementalHEROGrandmergeSpSummon()
        {

            return true;
        }

        private bool ElementalHEROElectrumRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElementalHEROElectrumActivate()
        {

            return true;
        }

        private bool ElementalHEROElectrumSpSummon()
        {

            return true;
        }

        private bool EvilHEROMaliciousBaneRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROMaliciousBaneActivate()
        {

            return true;
        }

        private bool EvilHEROMaliciousBaneSpSummon()
        {

            return true;
        }

        private bool EvilHEROMaliciousFiendRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROMaliciousFiendActivate()
        {

            return true;
        }

        private bool EvilHEROMaliciousFiendSpSummon()
        {

            return true;
        }

        private bool EvilHERODarkGaiaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHERODarkGaiaActivate()
        {

            return true;
        }

        private bool EvilHERODarkGaiaSpSummon()
        {

            return true;
        }

        private bool EvilHEROInfernoWingRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROInfernoWingActivate()
        {

            return true;
        }

        private bool EvilHEROInfernoWingSpSummon()
        {

            return true;
        }

        private bool EvilHEROLightningGolemRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROLightningGolemActivate()
        {

            return true;
        }

        private bool EvilHEROLightningGolemSpSummon()
        {

            return true;
        }

        private bool EvilHEROInfernalSniperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilHEROInfernalSniperActivate()
        {

            return true;
        }

        private bool EvilHEROInfernalSniperSpSummon()
        {

            return true;
        }

        private bool VisionHEROAdorationRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VisionHEROAdorationActivate()
        {

            return true;
        }

        private bool VisionHEROAdorationSpSummon()
        {

            return true;
        }

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool DarkCallingSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkCallingActivate()
        {

            return true;
        }

        private bool DarkFusionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkFusionActivate()
        {

            return true;
        }

        private bool EvilMindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool EvilMindActivate()
        {

            return true;
        }

        private bool SupremeKingsCastleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SupremeKingsCastleActivate()
        {

            return true;
        }

        private bool FoolishBurialSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FoolishBurialActivate()
        {

            return true;
        }

        private bool FutureFusionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FutureFusionActivate()
        {

            return true;
        }

        private bool HeroMaskSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HeroMaskActivate()
        {

            return true;
        }

        private bool LegacyofaHEROSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LegacyofaHEROActivate()
        {

            return true;
        }

        private bool MonsterRebornSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MonsterRebornActivate()
        {

            return true;
        }

        private bool PolymerizationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PolymerizationActivate()
        {

            return true;
        }

        private bool ReinforcementoftheArmySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ReinforcementoftheArmyActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

    }
}