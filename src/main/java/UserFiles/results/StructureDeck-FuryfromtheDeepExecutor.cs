using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Fury from the Deep", "StructureDeckFuryfromtheDeep")]
    public class StructureDeckFuryfromtheDeepExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int 7ColoredFish = 23771716;
            public const int SeaSerpentWarriorofDarkness = 42071342;
            public const int SpaceMambo = 36119641;
            // Initialize all effect monsters
            public const int OceanDragonLordNeoDaedalus = 10485110;
            public const int MotherGrizzly = 57839750;
            public const int StarBoy = 8201910;
            public const int TribeInfectingVirus = 33184167;
            public const int Fenrir = 218704;
            public const int AmphibiousBugrothMK3 = 64342551;
            public const int LeviaDragonDaedalus = 37721209;
            public const int MermaidKnight = 24435369;
            public const int MobiustheFrostMonarch = 4929256;
            public const int UnshavenAngler = 92084010;
            public const int CreepingDoomManta = 52571838;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int SnatchSteal = 45986603;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int PrematureBurial = 70828912;
            public const int PotofGreed = 55144522;
            public const int HeavyStorm = 19613556;
            public const int ALegendaryOcean = 295517;
            public const int CreatureSwap = 31036355;
            public const int Reload = 22589918;
            public const int Salvage = 96947648;
            public const int HammerShot = 26412047;
            public const int BigWaveSmallWave = 51562916;
            public const int DustTornado = 60082869;
            public const int CalloftheHaunted = 97077563;
            public const int GravityBind = 85742772;
            public const int TornadoWall = 18605135;
            public const int TorrentialTribute = 53582587;
            public const int SpellShieldType8 = 38275183;
            public const int XingZhenHu = 76515293;
            // Initialize all useless cards

         }
        public Structure Deck - Fury from the DeepExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.7ColoredFish, 7ColoredFishNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.7ColoredFish, 7ColoredFishMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.7ColoredFish, 7ColoredFishRepos);
            AddExecutor(ExecutorType.Summon, CardId.SeaSerpentWarriorofDarkness, SeaSerpentWarriorofDarknessNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SeaSerpentWarriorofDarkness, SeaSerpentWarriorofDarknessMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SeaSerpentWarriorofDarkness, SeaSerpentWarriorofDarknessRepos);
            AddExecutor(ExecutorType.Summon, CardId.SpaceMambo, SpaceMamboNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SpaceMambo, SpaceMamboMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SpaceMambo, SpaceMamboRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.OceanDragonLordNeoDaedalus, OceanDragonLordNeoDaedalusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.OceanDragonLordNeoDaedalus, OceanDragonLordNeoDaedalusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.OceanDragonLordNeoDaedalus, OceanDragonLordNeoDaedalusRepos);
            AddExecutor(ExecutorType.Activate, CardId.OceanDragonLordNeoDaedalus, OceanDragonLordNeoDaedalusActivate);
            AddExecutor(ExecutorType.Summon, CardId.MotherGrizzly, MotherGrizzlyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MotherGrizzly, MotherGrizzlyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MotherGrizzly, MotherGrizzlyRepos);
            AddExecutor(ExecutorType.Activate, CardId.MotherGrizzly, MotherGrizzlyActivate);
            AddExecutor(ExecutorType.Summon, CardId.StarBoy, StarBoyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.StarBoy, StarBoyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.StarBoy, StarBoyRepos);
            AddExecutor(ExecutorType.Activate, CardId.StarBoy, StarBoyActivate);
            AddExecutor(ExecutorType.Summon, CardId.TribeInfectingVirus, TribeInfectingVirusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TribeInfectingVirus, TribeInfectingVirusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TribeInfectingVirus, TribeInfectingVirusRepos);
            AddExecutor(ExecutorType.Activate, CardId.TribeInfectingVirus, TribeInfectingVirusActivate);
            AddExecutor(ExecutorType.Summon, CardId.Fenrir, FenrirNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Fenrir, FenrirMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Fenrir, FenrirRepos);
            AddExecutor(ExecutorType.Activate, CardId.Fenrir, FenrirActivate);
            AddExecutor(ExecutorType.Summon, CardId.AmphibiousBugrothMK3, AmphibiousBugrothMK3NormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AmphibiousBugrothMK3, AmphibiousBugrothMK3MonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AmphibiousBugrothMK3, AmphibiousBugrothMK3Repos);
            AddExecutor(ExecutorType.Activate, CardId.AmphibiousBugrothMK3, AmphibiousBugrothMK3Activate);
            AddExecutor(ExecutorType.Summon, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusRepos);
            AddExecutor(ExecutorType.Activate, CardId.LeviaDragonDaedalus, LeviaDragonDaedalusActivate);
            AddExecutor(ExecutorType.Summon, CardId.MermaidKnight, MermaidKnightNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MermaidKnight, MermaidKnightMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MermaidKnight, MermaidKnightRepos);
            AddExecutor(ExecutorType.Activate, CardId.MermaidKnight, MermaidKnightActivate);
            AddExecutor(ExecutorType.Summon, CardId.MobiustheFrostMonarch, MobiustheFrostMonarchNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MobiustheFrostMonarch, MobiustheFrostMonarchMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MobiustheFrostMonarch, MobiustheFrostMonarchRepos);
            AddExecutor(ExecutorType.Activate, CardId.MobiustheFrostMonarch, MobiustheFrostMonarchActivate);
            AddExecutor(ExecutorType.Summon, CardId.UnshavenAngler, UnshavenAnglerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.UnshavenAngler, UnshavenAnglerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.UnshavenAngler, UnshavenAnglerRepos);
            AddExecutor(ExecutorType.Activate, CardId.UnshavenAngler, UnshavenAnglerActivate);
            AddExecutor(ExecutorType.Summon, CardId.CreepingDoomManta, CreepingDoomMantaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CreepingDoomManta, CreepingDoomMantaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CreepingDoomManta, CreepingDoomMantaRepos);
            AddExecutor(ExecutorType.Activate, CardId.CreepingDoomManta, CreepingDoomMantaActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.SnatchSteal, SnatchStealSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SnatchSteal, SnatchStealActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PrematureBurial, PrematureBurialSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PrematureBurial, PrematureBurialActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PotofGreed, PotofGreedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PotofGreed, PotofGreedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HeavyStorm, HeavyStormSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HeavyStorm, HeavyStormActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ALegendaryOcean, ALegendaryOceanSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ALegendaryOcean, ALegendaryOceanActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Reload, ReloadSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Reload, ReloadActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Salvage, SalvageSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Salvage, SalvageActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HammerShot, HammerShotSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HammerShot, HammerShotActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BigWaveSmallWave, BigWaveSmallWaveSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BigWaveSmallWave, BigWaveSmallWaveActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DustTornado, DustTornadoSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DustTornado, DustTornadoActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GravityBind, GravityBindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GravityBind, GravityBindActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TornadoWall, TornadoWallSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TornadoWall, TornadoWallActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, TorrentialTributeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TorrentialTribute, TorrentialTributeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellShieldType8, SpellShieldType8SpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellShieldType8, SpellShieldType8Activate);
            AddExecutor(ExecutorType.SpellSet, CardId.XingZhenHu, XingZhenHuSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.XingZhenHu, XingZhenHuActivate);

         }

            // All Normal Monster Methods

        private bool 7ColoredFishNormalSummon()
        {

            return true;
        }

        private bool 7ColoredFishMonsterSet()
        {

            return true;
        }

        private bool 7ColoredFishRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SeaSerpentWarriorofDarknessNormalSummon()
        {

            return true;
        }

        private bool SeaSerpentWarriorofDarknessMonsterSet()
        {

            return true;
        }

        private bool SeaSerpentWarriorofDarknessRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SpaceMamboNormalSummon()
        {

            return true;
        }

        private bool SpaceMamboMonsterSet()
        {

            return true;
        }

        private bool SpaceMamboRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool OceanDragonLordNeoDaedalusNormalSummon()
        {

            return true;
        }

        private bool OceanDragonLordNeoDaedalusMonsterSet()
        {

            return true;
        }

        private bool OceanDragonLordNeoDaedalusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool OceanDragonLordNeoDaedalusActivate()
        {

            return true;
        }

        private bool MotherGrizzlyNormalSummon()
        {

            return true;
        }

        private bool MotherGrizzlyMonsterSet()
        {

            return true;
        }

        private bool MotherGrizzlyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MotherGrizzlyActivate()
        {

            return true;
        }

        private bool StarBoyNormalSummon()
        {

            return true;
        }

        private bool StarBoyMonsterSet()
        {

            return true;
        }

        private bool StarBoyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool StarBoyActivate()
        {

            return true;
        }

        private bool TribeInfectingVirusNormalSummon()
        {

            return true;
        }

        private bool TribeInfectingVirusMonsterSet()
        {

            return true;
        }

        private bool TribeInfectingVirusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TribeInfectingVirusActivate()
        {

            return true;
        }

        private bool FenrirNormalSummon()
        {

            return true;
        }

        private bool FenrirMonsterSet()
        {

            return true;
        }

        private bool FenrirRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FenrirActivate()
        {

            return true;
        }

        private bool AmphibiousBugrothMK3NormalSummon()
        {

            return true;
        }

        private bool AmphibiousBugrothMK3MonsterSet()
        {

            return true;
        }

        private bool AmphibiousBugrothMK3Repos()
        {

            return DefaultMonsterRepos;
        }

        private bool AmphibiousBugrothMK3Activate()
        {

            return true;
        }

        private bool LeviaDragonDaedalusNormalSummon()
        {

            return true;
        }

        private bool LeviaDragonDaedalusMonsterSet()
        {

            return true;
        }

        private bool LeviaDragonDaedalusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LeviaDragonDaedalusActivate()
        {

            return true;
        }

        private bool MermaidKnightNormalSummon()
        {

            return true;
        }

        private bool MermaidKnightMonsterSet()
        {

            return true;
        }

        private bool MermaidKnightRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MermaidKnightActivate()
        {

            return true;
        }

        private bool MobiustheFrostMonarchNormalSummon()
        {

            return true;
        }

        private bool MobiustheFrostMonarchMonsterSet()
        {

            return true;
        }

        private bool MobiustheFrostMonarchRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MobiustheFrostMonarchActivate()
        {

            return true;
        }

        private bool UnshavenAnglerNormalSummon()
        {

            return true;
        }

        private bool UnshavenAnglerMonsterSet()
        {

            return true;
        }

        private bool UnshavenAnglerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool UnshavenAnglerActivate()
        {

            return true;
        }

        private bool CreepingDoomMantaNormalSummon()
        {

            return true;
        }

        private bool CreepingDoomMantaMonsterSet()
        {

            return true;
        }

        private bool CreepingDoomMantaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CreepingDoomMantaActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool SnatchStealSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SnatchStealActivate()
        {

            return true;
        }

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool PrematureBurialSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PrematureBurialActivate()
        {

            return true;
        }

        private bool PotofGreedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PotofGreedActivate()
        {

            return true;
        }

        private bool HeavyStormSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HeavyStormActivate()
        {

            return true;
        }

        private bool ALegendaryOceanSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ALegendaryOceanActivate()
        {

            return true;
        }

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool ReloadSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ReloadActivate()
        {

            return true;
        }

        private bool SalvageSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SalvageActivate()
        {

            return true;
        }

        private bool HammerShotSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HammerShotActivate()
        {

            return true;
        }

        private bool BigWaveSmallWaveSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BigWaveSmallWaveActivate()
        {

            return true;
        }

        private bool DustTornadoSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DustTornadoActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

        private bool GravityBindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GravityBindActivate()
        {

            return true;
        }

        private bool TornadoWallSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TornadoWallActivate()
        {

            return true;
        }

        private bool TorrentialTributeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TorrentialTributeActivate()
        {

            return true;
        }

        private bool SpellShieldType8SpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellShieldType8Activate()
        {

            return true;
        }

        private bool XingZhenHuSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool XingZhenHuActivate()
        {

            return true;
        }

    }
}