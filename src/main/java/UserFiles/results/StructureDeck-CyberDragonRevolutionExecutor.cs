using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Cyber Dragon Revolution ", "StructureDeckCyberDragonRevolution")]
    public class StructureDeckCyberDragonRevolutionExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int CyberDragonCore = 23893227;
            public const int CyberDragonDrei = 59281922;
            public const int CyberDragon = 70095154;
            public const int CyberDragonZwei = 5373478;
            public const int ProtoCyberDragon = 26439287;
            public const int CyberValley = 3657444;
            public const int CyberLarva = 35050257;
            public const int CyberPhoenix = 3370104;
            public const int CyberDinosaur = 39439590;
            public const int CyberEltanin = 33093439;
            public const int ArmoredCybern = 67159705;
            public const int SatelliteCannon = 50400231;
            public const int SolarWindJammer = 33911264;
            public const int JadeKnight = 44364207;
            public const int Falchion = 86170989;
            public const int ReflectBounder = 2851070;
            public const int TheLightHexSealedFusion = 15717011;
            public const int ShiningAngel = 95956346;
            public const int CyberDragonNova = 58069384;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            public const int CyberTwinDragon = 74157028;
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int CyberRepairPlant = 86686671;
            public const int EvolutionBurst = 52875873;
            public const int SuperPolymerization = 48130397;
            public const int PowerBond = 37630732;
            public const int LimiterRemoval = 23171610;
            public const int Megamorph = 22046459;
            public const int DDRDifferentDimensionReincarnation = 9622164;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int LightofRedemption = 2362787;
            public const int MachinaArmoredUnit = 31828916;
            public const int CyberNetwork = 12670770;
            public const int CyberneticHiddenTechnology = 92773018;
            public const int ThreeofaKind = 47594192;
            public const int TrapStun = 59616123;
            public const int DimensionalPrison = 70342110;
            public const int MalevolentCatastrophe = 1224927;
            public const int Waboku = 12607053;
            public const int CalloftheHaunted = 97077563;
            // Initialize all useless cards

         }
        public Structure Deck - Cyber Dragon Revolution Executor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.CyberDragonCore, CyberDragonCoreNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragonCore, CyberDragonCoreMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragonCore, CyberDragonCoreRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragonCore, CyberDragonCoreActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragonDrei, CyberDragonDreiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragonDrei, CyberDragonDreiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragonDrei, CyberDragonDreiRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragonDrei, CyberDragonDreiActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragon, CyberDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragon, CyberDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragon, CyberDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragon, CyberDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragonZwei, CyberDragonZweiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragonZwei, CyberDragonZweiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragonZwei, CyberDragonZweiRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragonZwei, CyberDragonZweiActivate);
            AddExecutor(ExecutorType.Summon, CardId.ProtoCyberDragon, ProtoCyberDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ProtoCyberDragon, ProtoCyberDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ProtoCyberDragon, ProtoCyberDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.ProtoCyberDragon, ProtoCyberDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberValley, CyberValleyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberValley, CyberValleyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberValley, CyberValleyRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberValley, CyberValleyActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberLarva, CyberLarvaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberLarva, CyberLarvaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberLarva, CyberLarvaRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberLarva, CyberLarvaActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberPhoenix, CyberPhoenixNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberPhoenix, CyberPhoenixMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberPhoenix, CyberPhoenixRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberPhoenix, CyberPhoenixActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDinosaur, CyberDinosaurNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDinosaur, CyberDinosaurMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDinosaur, CyberDinosaurRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDinosaur, CyberDinosaurActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberEltanin, CyberEltaninNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberEltanin, CyberEltaninMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberEltanin, CyberEltaninRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberEltanin, CyberEltaninActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArmoredCybern, ArmoredCybernNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArmoredCybern, ArmoredCybernMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArmoredCybern, ArmoredCybernRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArmoredCybern, ArmoredCybernActivate);
            AddExecutor(ExecutorType.Summon, CardId.SatelliteCannon, SatelliteCannonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SatelliteCannon, SatelliteCannonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SatelliteCannon, SatelliteCannonRepos);
            AddExecutor(ExecutorType.Activate, CardId.SatelliteCannon, SatelliteCannonActivate);
            AddExecutor(ExecutorType.Summon, CardId.SolarWindJammer, SolarWindJammerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SolarWindJammer, SolarWindJammerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SolarWindJammer, SolarWindJammerRepos);
            AddExecutor(ExecutorType.Activate, CardId.SolarWindJammer, SolarWindJammerActivate);
            AddExecutor(ExecutorType.Summon, CardId.JadeKnight, JadeKnightNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.JadeKnight, JadeKnightMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.JadeKnight, JadeKnightRepos);
            AddExecutor(ExecutorType.Activate, CardId.JadeKnight, JadeKnightActivate);
            AddExecutor(ExecutorType.Summon, CardId.Falchion, FalchionNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Falchion, FalchionMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Falchion, FalchionRepos);
            AddExecutor(ExecutorType.Activate, CardId.Falchion, FalchionActivate);
            AddExecutor(ExecutorType.Summon, CardId.ReflectBounder, ReflectBounderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ReflectBounder, ReflectBounderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ReflectBounder, ReflectBounderRepos);
            AddExecutor(ExecutorType.Activate, CardId.ReflectBounder, ReflectBounderActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheLightHexSealedFusion, TheLightHexSealedFusionNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheLightHexSealedFusion, TheLightHexSealedFusionMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheLightHexSealedFusion, TheLightHexSealedFusionRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheLightHexSealedFusion, TheLightHexSealedFusionActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShiningAngel, ShiningAngelNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShiningAngel, ShiningAngelMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShiningAngel, ShiningAngelRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShiningAngel, ShiningAngelActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragonNova, CyberDragonNovaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragonNova, CyberDragonNovaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragonNova, CyberDragonNovaRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragonNova, CyberDragonNovaActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            AddExecutor(ExecutorType.Repos, CardId.CyberTwinDragon, CyberTwinDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberTwinDragon, CyberTwinDragonActivate);
            AddExecutor(ExecutorType.SpSummon, CardId.CyberTwinDragon, CyberTwinDragonSpSummon);
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.CyberRepairPlant, CyberRepairPlantSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CyberRepairPlant, CyberRepairPlantActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.EvolutionBurst, EvolutionBurstSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.EvolutionBurst, EvolutionBurstActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SuperPolymerization, SuperPolymerizationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SuperPolymerization, SuperPolymerizationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PowerBond, PowerBondSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PowerBond, PowerBondActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LimiterRemoval, LimiterRemovalSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LimiterRemoval, LimiterRemovalActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Megamorph, MegamorphSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Megamorph, MegamorphActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DDRDifferentDimensionReincarnation, DDRDifferentDimensionReincarnationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DDRDifferentDimensionReincarnation, DDRDifferentDimensionReincarnationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightofRedemption, LightofRedemptionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightofRedemption, LightofRedemptionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MachinaArmoredUnit, MachinaArmoredUnitSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MachinaArmoredUnit, MachinaArmoredUnitActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CyberNetwork, CyberNetworkSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CyberNetwork, CyberNetworkActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CyberneticHiddenTechnology, CyberneticHiddenTechnologySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CyberneticHiddenTechnology, CyberneticHiddenTechnologyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ThreeofaKind, ThreeofaKindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ThreeofaKind, ThreeofaKindActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TrapStun, TrapStunSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TrapStun, TrapStunActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalPrison, DimensionalPrisonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DimensionalPrison, DimensionalPrisonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MalevolentCatastrophe, MalevolentCatastropheSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MalevolentCatastrophe, MalevolentCatastropheActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Waboku, WabokuSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Waboku, WabokuActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool CyberDragonCoreNormalSummon()
        {

            return true;
        }

        private bool CyberDragonCoreMonsterSet()
        {

            return true;
        }

        private bool CyberDragonCoreRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonCoreActivate()
        {

            return true;
        }

        private bool CyberDragonDreiNormalSummon()
        {

            return true;
        }

        private bool CyberDragonDreiMonsterSet()
        {

            return true;
        }

        private bool CyberDragonDreiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonDreiActivate()
        {

            return true;
        }

        private bool CyberDragonNormalSummon()
        {

            return true;
        }

        private bool CyberDragonMonsterSet()
        {

            return true;
        }

        private bool CyberDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonActivate()
        {

            return true;
        }

        private bool CyberDragonZweiNormalSummon()
        {

            return true;
        }

        private bool CyberDragonZweiMonsterSet()
        {

            return true;
        }

        private bool CyberDragonZweiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonZweiActivate()
        {

            return true;
        }

        private bool ProtoCyberDragonNormalSummon()
        {

            return true;
        }

        private bool ProtoCyberDragonMonsterSet()
        {

            return true;
        }

        private bool ProtoCyberDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ProtoCyberDragonActivate()
        {

            return true;
        }

        private bool CyberValleyNormalSummon()
        {

            return true;
        }

        private bool CyberValleyMonsterSet()
        {

            return true;
        }

        private bool CyberValleyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberValleyActivate()
        {

            return true;
        }

        private bool CyberLarvaNormalSummon()
        {

            return true;
        }

        private bool CyberLarvaMonsterSet()
        {

            return true;
        }

        private bool CyberLarvaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberLarvaActivate()
        {

            return true;
        }

        private bool CyberPhoenixNormalSummon()
        {

            return true;
        }

        private bool CyberPhoenixMonsterSet()
        {

            return true;
        }

        private bool CyberPhoenixRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberPhoenixActivate()
        {

            return true;
        }

        private bool CyberDinosaurNormalSummon()
        {

            return true;
        }

        private bool CyberDinosaurMonsterSet()
        {

            return true;
        }

        private bool CyberDinosaurRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDinosaurActivate()
        {

            return true;
        }

        private bool CyberEltaninNormalSummon()
        {

            return true;
        }

        private bool CyberEltaninMonsterSet()
        {

            return true;
        }

        private bool CyberEltaninRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberEltaninActivate()
        {

            return true;
        }

        private bool ArmoredCybernNormalSummon()
        {

            return true;
        }

        private bool ArmoredCybernMonsterSet()
        {

            return true;
        }

        private bool ArmoredCybernRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArmoredCybernActivate()
        {

            return true;
        }

        private bool SatelliteCannonNormalSummon()
        {

            return true;
        }

        private bool SatelliteCannonMonsterSet()
        {

            return true;
        }

        private bool SatelliteCannonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SatelliteCannonActivate()
        {

            return true;
        }

        private bool SolarWindJammerNormalSummon()
        {

            return true;
        }

        private bool SolarWindJammerMonsterSet()
        {

            return true;
        }

        private bool SolarWindJammerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SolarWindJammerActivate()
        {

            return true;
        }

        private bool JadeKnightNormalSummon()
        {

            return true;
        }

        private bool JadeKnightMonsterSet()
        {

            return true;
        }

        private bool JadeKnightRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool JadeKnightActivate()
        {

            return true;
        }

        private bool FalchionNormalSummon()
        {

            return true;
        }

        private bool FalchionMonsterSet()
        {

            return true;
        }

        private bool FalchionRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FalchionActivate()
        {

            return true;
        }

        private bool ReflectBounderNormalSummon()
        {

            return true;
        }

        private bool ReflectBounderMonsterSet()
        {

            return true;
        }

        private bool ReflectBounderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ReflectBounderActivate()
        {

            return true;
        }

        private bool TheLightHexSealedFusionNormalSummon()
        {

            return true;
        }

        private bool TheLightHexSealedFusionMonsterSet()
        {

            return true;
        }

        private bool TheLightHexSealedFusionRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheLightHexSealedFusionActivate()
        {

            return true;
        }

        private bool ShiningAngelNormalSummon()
        {

            return true;
        }

        private bool ShiningAngelMonsterSet()
        {

            return true;
        }

        private bool ShiningAngelRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShiningAngelActivate()
        {

            return true;
        }

        private bool CyberDragonNovaNormalSummon()
        {

            return true;
        }

        private bool CyberDragonNovaMonsterSet()
        {

            return true;
        }

        private bool CyberDragonNovaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonNovaActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

        private bool CyberTwinDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberTwinDragonActivate()
        {

            return true;
        }

        private bool CyberTwinDragonSpSummon()
        {

            return true;
        }

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool CyberRepairPlantSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CyberRepairPlantActivate()
        {

            return true;
        }

        private bool EvolutionBurstSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool EvolutionBurstActivate()
        {

            return true;
        }

        private bool SuperPolymerizationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SuperPolymerizationActivate()
        {

            return true;
        }

        private bool PowerBondSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PowerBondActivate()
        {

            return true;
        }

        private bool LimiterRemovalSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LimiterRemovalActivate()
        {

            return true;
        }

        private bool MegamorphSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MegamorphActivate()
        {

            return true;
        }

        private bool DDRDifferentDimensionReincarnationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DDRDifferentDimensionReincarnationActivate()
        {

            return true;
        }

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool LightofRedemptionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightofRedemptionActivate()
        {

            return true;
        }

        private bool MachinaArmoredUnitSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MachinaArmoredUnitActivate()
        {

            return true;
        }

        private bool CyberNetworkSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CyberNetworkActivate()
        {

            return true;
        }

        private bool CyberneticHiddenTechnologySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CyberneticHiddenTechnologyActivate()
        {

            return true;
        }

        private bool ThreeofaKindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ThreeofaKindActivate()
        {

            return true;
        }

        private bool TrapStunSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TrapStunActivate()
        {

            return true;
        }

        private bool DimensionalPrisonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DimensionalPrisonActivate()
        {

            return true;
        }

        private bool MalevolentCatastropheSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MalevolentCatastropheActivate()
        {

            return true;
        }

        private bool WabokuSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WabokuActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

    }
}