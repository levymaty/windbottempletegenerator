using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Realm of the Sea Emperor", "StructureDeckRealmoftheSeaEmperor")]
    public class StructureDeckRealmoftheSeaEmperorExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int AtlanteanPikeman = 26976414;
            // Initialize all effect monsters
            public const int PoseidratheAtlanteanDragon = 47826112;
            public const int WarriorofAtlantis = 43797906;
            public const int AbyssSoldier = 18318842;
            public const int ArmedSeaHunter = 20470500;
            public const int AtlanteanDragoons = 74311226;
            public const int Skreech = 27655513;
            public const int Codarus = 65496056;
            public const int MotherGrizzly = 57839750;
            public const int NightmarePenguin = 81306586;
            public const int AtlanteanMarksman = 706925;
            public const int AtlanteanAttackSquad = 8078366;
            public const int LostBlueBreaker = 95231062;
            public const int SpinedGillman = 42463414;
            public const int MermaidArcher = 4252828;
            public const int DeepDiver = 17559367;
            public const int FrillerRabca = 93830681;
            public const int SnowmanEater = 91133740;
            public const int ReesetheIceMistress = 30276969;
            public const int PenguinSoldier = 93920745;
            public const int DeepSeaDiva = 78868119;
            public const int AtlanteanHeavyInfantry = 37104630;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int AquaJet = 7653207;
            public const int MorayofGreed = 22123627;
            public const int Surface = 33057951;
            public const int BigWaveSmallWave = 51562916;
            public const int DarkHole = 53129443;
            public const int Terraforming = 73628505;
            public const int Salvage = 96947648;
            public const int CalloftheAtlanteans = 73199638;
            public const int WaterHazard = 49669730;
            public const int ALegendaryOcean = 295517;
            public const int SpiritualWaterArtAoi = 6540606;
            public const int AegisoftheOceanDragonLord = 7935043;
            public const int PoseidonWave = 25642998;
            public const int TorrentialTribute = 53582587;
            public const int TornadoWall = 18605135;
            public const int ForgottenTempleoftheDeep = 43889633;
            public const int GravityBind = 85742772;
            // Initialize all useless cards

         }
        public Structure Deck - Realm of the Sea EmperorExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.AtlanteanPikeman, AtlanteanPikemanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AtlanteanPikeman, AtlanteanPikemanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AtlanteanPikeman, AtlanteanPikemanRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.PoseidratheAtlanteanDragon, PoseidratheAtlanteanDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PoseidratheAtlanteanDragon, PoseidratheAtlanteanDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PoseidratheAtlanteanDragon, PoseidratheAtlanteanDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.PoseidratheAtlanteanDragon, PoseidratheAtlanteanDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.WarriorofAtlantis, WarriorofAtlantisNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WarriorofAtlantis, WarriorofAtlantisMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WarriorofAtlantis, WarriorofAtlantisRepos);
            AddExecutor(ExecutorType.Activate, CardId.WarriorofAtlantis, WarriorofAtlantisActivate);
            AddExecutor(ExecutorType.Summon, CardId.AbyssSoldier, AbyssSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AbyssSoldier, AbyssSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AbyssSoldier, AbyssSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.AbyssSoldier, AbyssSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArmedSeaHunter, ArmedSeaHunterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArmedSeaHunter, ArmedSeaHunterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArmedSeaHunter, ArmedSeaHunterRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArmedSeaHunter, ArmedSeaHunterActivate);
            AddExecutor(ExecutorType.Summon, CardId.AtlanteanDragoons, AtlanteanDragoonsNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AtlanteanDragoons, AtlanteanDragoonsMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AtlanteanDragoons, AtlanteanDragoonsRepos);
            AddExecutor(ExecutorType.Activate, CardId.AtlanteanDragoons, AtlanteanDragoonsActivate);
            AddExecutor(ExecutorType.Summon, CardId.Skreech, SkreechNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Skreech, SkreechMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Skreech, SkreechRepos);
            AddExecutor(ExecutorType.Activate, CardId.Skreech, SkreechActivate);
            AddExecutor(ExecutorType.Summon, CardId.Codarus, CodarusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Codarus, CodarusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Codarus, CodarusRepos);
            AddExecutor(ExecutorType.Activate, CardId.Codarus, CodarusActivate);
            AddExecutor(ExecutorType.Summon, CardId.MotherGrizzly, MotherGrizzlyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MotherGrizzly, MotherGrizzlyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MotherGrizzly, MotherGrizzlyRepos);
            AddExecutor(ExecutorType.Activate, CardId.MotherGrizzly, MotherGrizzlyActivate);
            AddExecutor(ExecutorType.Summon, CardId.NightmarePenguin, NightmarePenguinNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NightmarePenguin, NightmarePenguinMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NightmarePenguin, NightmarePenguinRepos);
            AddExecutor(ExecutorType.Activate, CardId.NightmarePenguin, NightmarePenguinActivate);
            AddExecutor(ExecutorType.Summon, CardId.AtlanteanMarksman, AtlanteanMarksmanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AtlanteanMarksman, AtlanteanMarksmanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AtlanteanMarksman, AtlanteanMarksmanRepos);
            AddExecutor(ExecutorType.Activate, CardId.AtlanteanMarksman, AtlanteanMarksmanActivate);
            AddExecutor(ExecutorType.Summon, CardId.AtlanteanAttackSquad, AtlanteanAttackSquadNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AtlanteanAttackSquad, AtlanteanAttackSquadMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AtlanteanAttackSquad, AtlanteanAttackSquadRepos);
            AddExecutor(ExecutorType.Activate, CardId.AtlanteanAttackSquad, AtlanteanAttackSquadActivate);
            AddExecutor(ExecutorType.Summon, CardId.LostBlueBreaker, LostBlueBreakerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LostBlueBreaker, LostBlueBreakerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LostBlueBreaker, LostBlueBreakerRepos);
            AddExecutor(ExecutorType.Activate, CardId.LostBlueBreaker, LostBlueBreakerActivate);
            AddExecutor(ExecutorType.Summon, CardId.SpinedGillman, SpinedGillmanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SpinedGillman, SpinedGillmanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SpinedGillman, SpinedGillmanRepos);
            AddExecutor(ExecutorType.Activate, CardId.SpinedGillman, SpinedGillmanActivate);
            AddExecutor(ExecutorType.Summon, CardId.MermaidArcher, MermaidArcherNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MermaidArcher, MermaidArcherMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MermaidArcher, MermaidArcherRepos);
            AddExecutor(ExecutorType.Activate, CardId.MermaidArcher, MermaidArcherActivate);
            AddExecutor(ExecutorType.Summon, CardId.DeepDiver, DeepDiverNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DeepDiver, DeepDiverMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DeepDiver, DeepDiverRepos);
            AddExecutor(ExecutorType.Activate, CardId.DeepDiver, DeepDiverActivate);
            AddExecutor(ExecutorType.Summon, CardId.FrillerRabca, FrillerRabcaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FrillerRabca, FrillerRabcaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FrillerRabca, FrillerRabcaRepos);
            AddExecutor(ExecutorType.Activate, CardId.FrillerRabca, FrillerRabcaActivate);
            AddExecutor(ExecutorType.Summon, CardId.SnowmanEater, SnowmanEaterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SnowmanEater, SnowmanEaterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SnowmanEater, SnowmanEaterRepos);
            AddExecutor(ExecutorType.Activate, CardId.SnowmanEater, SnowmanEaterActivate);
            AddExecutor(ExecutorType.Summon, CardId.ReesetheIceMistress, ReesetheIceMistressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ReesetheIceMistress, ReesetheIceMistressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ReesetheIceMistress, ReesetheIceMistressRepos);
            AddExecutor(ExecutorType.Activate, CardId.ReesetheIceMistress, ReesetheIceMistressActivate);
            AddExecutor(ExecutorType.Summon, CardId.PenguinSoldier, PenguinSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PenguinSoldier, PenguinSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PenguinSoldier, PenguinSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.PenguinSoldier, PenguinSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.DeepSeaDiva, DeepSeaDivaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DeepSeaDiva, DeepSeaDivaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DeepSeaDiva, DeepSeaDivaRepos);
            AddExecutor(ExecutorType.Activate, CardId.DeepSeaDiva, DeepSeaDivaActivate);
            AddExecutor(ExecutorType.Summon, CardId.AtlanteanHeavyInfantry, AtlanteanHeavyInfantryNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.AtlanteanHeavyInfantry, AtlanteanHeavyInfantryMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.AtlanteanHeavyInfantry, AtlanteanHeavyInfantryRepos);
            AddExecutor(ExecutorType.Activate, CardId.AtlanteanHeavyInfantry, AtlanteanHeavyInfantryActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.AquaJet, AquaJetSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AquaJet, AquaJetActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MorayofGreed, MorayofGreedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MorayofGreed, MorayofGreedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Surface, SurfaceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Surface, SurfaceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BigWaveSmallWave, BigWaveSmallWaveSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BigWaveSmallWave, BigWaveSmallWaveActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, DarkHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DarkHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Terraforming, TerraformingSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Terraforming, TerraformingActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Salvage, SalvageSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Salvage, SalvageActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheAtlanteans, CalloftheAtlanteansSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheAtlanteans, CalloftheAtlanteansActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.WaterHazard, WaterHazardSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.WaterHazard, WaterHazardActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ALegendaryOcean, ALegendaryOceanSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ALegendaryOcean, ALegendaryOceanActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpiritualWaterArtAoi, SpiritualWaterArtAoiSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpiritualWaterArtAoi, SpiritualWaterArtAoiActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AegisoftheOceanDragonLord, AegisoftheOceanDragonLordSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AegisoftheOceanDragonLord, AegisoftheOceanDragonLordActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PoseidonWave, PoseidonWaveSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PoseidonWave, PoseidonWaveActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, TorrentialTributeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TorrentialTribute, TorrentialTributeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TornadoWall, TornadoWallSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TornadoWall, TornadoWallActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ForgottenTempleoftheDeep, ForgottenTempleoftheDeepSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ForgottenTempleoftheDeep, ForgottenTempleoftheDeepActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GravityBind, GravityBindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GravityBind, GravityBindActivate);

         }

            // All Normal Monster Methods

        private bool AtlanteanPikemanNormalSummon()
        {

            return true;
        }

        private bool AtlanteanPikemanMonsterSet()
        {

            return true;
        }

        private bool AtlanteanPikemanRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool PoseidratheAtlanteanDragonNormalSummon()
        {

            return true;
        }

        private bool PoseidratheAtlanteanDragonMonsterSet()
        {

            return true;
        }

        private bool PoseidratheAtlanteanDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PoseidratheAtlanteanDragonActivate()
        {

            return true;
        }

        private bool WarriorofAtlantisNormalSummon()
        {

            return true;
        }

        private bool WarriorofAtlantisMonsterSet()
        {

            return true;
        }

        private bool WarriorofAtlantisRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WarriorofAtlantisActivate()
        {

            return true;
        }

        private bool AbyssSoldierNormalSummon()
        {

            return true;
        }

        private bool AbyssSoldierMonsterSet()
        {

            return true;
        }

        private bool AbyssSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool AbyssSoldierActivate()
        {

            return true;
        }

        private bool ArmedSeaHunterNormalSummon()
        {

            return true;
        }

        private bool ArmedSeaHunterMonsterSet()
        {

            return true;
        }

        private bool ArmedSeaHunterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArmedSeaHunterActivate()
        {

            return true;
        }

        private bool AtlanteanDragoonsNormalSummon()
        {

            return true;
        }

        private bool AtlanteanDragoonsMonsterSet()
        {

            return true;
        }

        private bool AtlanteanDragoonsRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool AtlanteanDragoonsActivate()
        {

            return true;
        }

        private bool SkreechNormalSummon()
        {

            return true;
        }

        private bool SkreechMonsterSet()
        {

            return true;
        }

        private bool SkreechRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SkreechActivate()
        {

            return true;
        }

        private bool CodarusNormalSummon()
        {

            return true;
        }

        private bool CodarusMonsterSet()
        {

            return true;
        }

        private bool CodarusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CodarusActivate()
        {

            return true;
        }

        private bool MotherGrizzlyNormalSummon()
        {

            return true;
        }

        private bool MotherGrizzlyMonsterSet()
        {

            return true;
        }

        private bool MotherGrizzlyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MotherGrizzlyActivate()
        {

            return true;
        }

        private bool NightmarePenguinNormalSummon()
        {

            return true;
        }

        private bool NightmarePenguinMonsterSet()
        {

            return true;
        }

        private bool NightmarePenguinRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NightmarePenguinActivate()
        {

            return true;
        }

        private bool AtlanteanMarksmanNormalSummon()
        {

            return true;
        }

        private bool AtlanteanMarksmanMonsterSet()
        {

            return true;
        }

        private bool AtlanteanMarksmanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool AtlanteanMarksmanActivate()
        {

            return true;
        }

        private bool AtlanteanAttackSquadNormalSummon()
        {

            return true;
        }

        private bool AtlanteanAttackSquadMonsterSet()
        {

            return true;
        }

        private bool AtlanteanAttackSquadRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool AtlanteanAttackSquadActivate()
        {

            return true;
        }

        private bool LostBlueBreakerNormalSummon()
        {

            return true;
        }

        private bool LostBlueBreakerMonsterSet()
        {

            return true;
        }

        private bool LostBlueBreakerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LostBlueBreakerActivate()
        {

            return true;
        }

        private bool SpinedGillmanNormalSummon()
        {

            return true;
        }

        private bool SpinedGillmanMonsterSet()
        {

            return true;
        }

        private bool SpinedGillmanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SpinedGillmanActivate()
        {

            return true;
        }

        private bool MermaidArcherNormalSummon()
        {

            return true;
        }

        private bool MermaidArcherMonsterSet()
        {

            return true;
        }

        private bool MermaidArcherRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MermaidArcherActivate()
        {

            return true;
        }

        private bool DeepDiverNormalSummon()
        {

            return true;
        }

        private bool DeepDiverMonsterSet()
        {

            return true;
        }

        private bool DeepDiverRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DeepDiverActivate()
        {

            return true;
        }

        private bool FrillerRabcaNormalSummon()
        {

            return true;
        }

        private bool FrillerRabcaMonsterSet()
        {

            return true;
        }

        private bool FrillerRabcaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FrillerRabcaActivate()
        {

            return true;
        }

        private bool SnowmanEaterNormalSummon()
        {

            return true;
        }

        private bool SnowmanEaterMonsterSet()
        {

            return true;
        }

        private bool SnowmanEaterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SnowmanEaterActivate()
        {

            return true;
        }

        private bool ReesetheIceMistressNormalSummon()
        {

            return true;
        }

        private bool ReesetheIceMistressMonsterSet()
        {

            return true;
        }

        private bool ReesetheIceMistressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ReesetheIceMistressActivate()
        {

            return true;
        }

        private bool PenguinSoldierNormalSummon()
        {

            return true;
        }

        private bool PenguinSoldierMonsterSet()
        {

            return true;
        }

        private bool PenguinSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PenguinSoldierActivate()
        {

            return true;
        }

        private bool DeepSeaDivaNormalSummon()
        {

            return true;
        }

        private bool DeepSeaDivaMonsterSet()
        {

            return true;
        }

        private bool DeepSeaDivaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DeepSeaDivaActivate()
        {

            return true;
        }

        private bool AtlanteanHeavyInfantryNormalSummon()
        {

            return true;
        }

        private bool AtlanteanHeavyInfantryMonsterSet()
        {

            return true;
        }

        private bool AtlanteanHeavyInfantryRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool AtlanteanHeavyInfantryActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool AquaJetSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AquaJetActivate()
        {

            return true;
        }

        private bool MorayofGreedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MorayofGreedActivate()
        {

            return true;
        }

        private bool SurfaceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SurfaceActivate()
        {

            return true;
        }

        private bool BigWaveSmallWaveSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BigWaveSmallWaveActivate()
        {

            return true;
        }

        private bool DarkHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkHoleActivate()
        {

            return true;
        }

        private bool TerraformingSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TerraformingActivate()
        {

            return true;
        }

        private bool SalvageSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SalvageActivate()
        {

            return true;
        }

        private bool CalloftheAtlanteansSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheAtlanteansActivate()
        {

            return true;
        }

        private bool WaterHazardSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WaterHazardActivate()
        {

            return true;
        }

        private bool ALegendaryOceanSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ALegendaryOceanActivate()
        {

            return true;
        }

        private bool SpiritualWaterArtAoiSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpiritualWaterArtAoiActivate()
        {

            return true;
        }

        private bool AegisoftheOceanDragonLordSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AegisoftheOceanDragonLordActivate()
        {

            return true;
        }

        private bool PoseidonWaveSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PoseidonWaveActivate()
        {

            return true;
        }

        private bool TorrentialTributeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TorrentialTributeActivate()
        {

            return true;
        }

        private bool TornadoWallSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TornadoWallActivate()
        {

            return true;
        }

        private bool ForgottenTempleoftheDeepSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ForgottenTempleoftheDeepActivate()
        {

            return true;
        }

        private bool GravityBindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GravityBindActivate()
        {

            return true;
        }

    }
}