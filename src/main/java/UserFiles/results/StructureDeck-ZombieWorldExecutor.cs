using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Zombie World", "StructureDeckZombieWorld")]
    public class StructureDeckZombieWorldExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int MasterKyonshee = 24530661;
            // Initialize all effect monsters
            public const int RedEyesZombieDragon = 5186893;
            public const int RyuKokki = 57281778;
            public const int MalevolentMechGokuEn = 31571902;
            public const int PatricianofDarkness = 19153634;
            public const int PaladinoftheCursedDragon = 68670547;
            public const int RegeneratingMummy = 70821187;
            public const int ZombieMaster = 17259470;
            public const int GetsuFuhma = 21887179;
            public const int RoyalKeeper = 16509093;
            public const int Gernia = 77936940;
            public const int PyramidTurtle = 77044671;
            public const int PlagueWolf = 55696885;
            public const int MarionetteMite = 87514539;
            public const int DesLacooda = 2326738;
            public const int SpiritReaper = 23205979;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int BookofLife = 2204140;
            public const int CreatureSwap = 31036355;
            public const int ColdWave = 60682203;
            public const int PotofAvarice = 67169062;
            public const int CardDestruction = 72892473;
            public const int Terraforming = 73628505;
            public const int SoulTaker = 81510157;
            public const int MagicalStoneExcavation = 98494543;
            public const int Shrink = 55713623;
            public const int SpellShatteringArrow = 93260132;
            public const int CalloftheMummy = 4861205;
            public const int FieldBarrier = 7153114;
            public const int CardofSafeReturn = 57953380;
            public const int RibbonofRebirth = 37534148;
            public const int ZombieWorld = 4064256;
            public const int Waboku = 12607053;
            public const int BottomlessTrapHole = 29401950;
            public const int DustTornado = 60082869;
            public const int MagicalArmShield = 96008713;
            public const int ImperialIronWall = 30459350;
            public const int TutanMask = 3149764;
            // Initialize all useless cards

         }
        public Structure Deck - Zombie WorldExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.MasterKyonshee, MasterKyonsheeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MasterKyonshee, MasterKyonsheeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MasterKyonshee, MasterKyonsheeRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.RedEyesZombieDragon, RedEyesZombieDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RedEyesZombieDragon, RedEyesZombieDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RedEyesZombieDragon, RedEyesZombieDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.RedEyesZombieDragon, RedEyesZombieDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.RyuKokki, RyuKokkiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RyuKokki, RyuKokkiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RyuKokki, RyuKokkiRepos);
            AddExecutor(ExecutorType.Activate, CardId.RyuKokki, RyuKokkiActivate);
            AddExecutor(ExecutorType.Summon, CardId.MalevolentMechGokuEn, MalevolentMechGokuEnNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MalevolentMechGokuEn, MalevolentMechGokuEnMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MalevolentMechGokuEn, MalevolentMechGokuEnRepos);
            AddExecutor(ExecutorType.Activate, CardId.MalevolentMechGokuEn, MalevolentMechGokuEnActivate);
            AddExecutor(ExecutorType.Summon, CardId.PatricianofDarkness, PatricianofDarknessNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PatricianofDarkness, PatricianofDarknessMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PatricianofDarkness, PatricianofDarknessRepos);
            AddExecutor(ExecutorType.Activate, CardId.PatricianofDarkness, PatricianofDarknessActivate);
            AddExecutor(ExecutorType.Summon, CardId.PaladinoftheCursedDragon, PaladinoftheCursedDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PaladinoftheCursedDragon, PaladinoftheCursedDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PaladinoftheCursedDragon, PaladinoftheCursedDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.PaladinoftheCursedDragon, PaladinoftheCursedDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.RegeneratingMummy, RegeneratingMummyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RegeneratingMummy, RegeneratingMummyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RegeneratingMummy, RegeneratingMummyRepos);
            AddExecutor(ExecutorType.Activate, CardId.RegeneratingMummy, RegeneratingMummyActivate);
            AddExecutor(ExecutorType.Summon, CardId.ZombieMaster, ZombieMasterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ZombieMaster, ZombieMasterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ZombieMaster, ZombieMasterRepos);
            AddExecutor(ExecutorType.Activate, CardId.ZombieMaster, ZombieMasterActivate);
            AddExecutor(ExecutorType.Summon, CardId.GetsuFuhma, GetsuFuhmaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GetsuFuhma, GetsuFuhmaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GetsuFuhma, GetsuFuhmaRepos);
            AddExecutor(ExecutorType.Activate, CardId.GetsuFuhma, GetsuFuhmaActivate);
            AddExecutor(ExecutorType.Summon, CardId.RoyalKeeper, RoyalKeeperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RoyalKeeper, RoyalKeeperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RoyalKeeper, RoyalKeeperRepos);
            AddExecutor(ExecutorType.Activate, CardId.RoyalKeeper, RoyalKeeperActivate);
            AddExecutor(ExecutorType.Summon, CardId.Gernia, GerniaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Gernia, GerniaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Gernia, GerniaRepos);
            AddExecutor(ExecutorType.Activate, CardId.Gernia, GerniaActivate);
            AddExecutor(ExecutorType.Summon, CardId.PyramidTurtle, PyramidTurtleNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PyramidTurtle, PyramidTurtleMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PyramidTurtle, PyramidTurtleRepos);
            AddExecutor(ExecutorType.Activate, CardId.PyramidTurtle, PyramidTurtleActivate);
            AddExecutor(ExecutorType.Summon, CardId.PlagueWolf, PlagueWolfNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.PlagueWolf, PlagueWolfMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.PlagueWolf, PlagueWolfRepos);
            AddExecutor(ExecutorType.Activate, CardId.PlagueWolf, PlagueWolfActivate);
            AddExecutor(ExecutorType.Summon, CardId.MarionetteMite, MarionetteMiteNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MarionetteMite, MarionetteMiteMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MarionetteMite, MarionetteMiteRepos);
            AddExecutor(ExecutorType.Activate, CardId.MarionetteMite, MarionetteMiteActivate);
            AddExecutor(ExecutorType.Summon, CardId.DesLacooda, DesLacoodaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DesLacooda, DesLacoodaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DesLacooda, DesLacoodaRepos);
            AddExecutor(ExecutorType.Activate, CardId.DesLacooda, DesLacoodaActivate);
            AddExecutor(ExecutorType.Summon, CardId.SpiritReaper, SpiritReaperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SpiritReaper, SpiritReaperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SpiritReaper, SpiritReaperRepos);
            AddExecutor(ExecutorType.Activate, CardId.SpiritReaper, SpiritReaperActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.BookofLife, BookofLifeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BookofLife, BookofLifeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ColdWave, ColdWaveSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ColdWave, ColdWaveActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.PotofAvarice, PotofAvariceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.PotofAvarice, PotofAvariceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardDestruction, CardDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardDestruction, CardDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Terraforming, TerraformingSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Terraforming, TerraformingActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SoulTaker, SoulTakerSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SoulTaker, SoulTakerActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MagicalStoneExcavation, MagicalStoneExcavationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MagicalStoneExcavation, MagicalStoneExcavationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Shrink, ShrinkSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Shrink, ShrinkActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellShatteringArrow, SpellShatteringArrowSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellShatteringArrow, SpellShatteringArrowActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheMummy, CalloftheMummySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheMummy, CalloftheMummyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FieldBarrier, FieldBarrierSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FieldBarrier, FieldBarrierActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardofSafeReturn, CardofSafeReturnSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardofSafeReturn, CardofSafeReturnActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RibbonofRebirth, RibbonofRebirthSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RibbonofRebirth, RibbonofRebirthActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ZombieWorld, ZombieWorldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ZombieWorld, ZombieWorldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Waboku, WabokuSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Waboku, WabokuActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BottomlessTrapHole, BottomlessTrapHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BottomlessTrapHole, BottomlessTrapHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DustTornado, DustTornadoSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DustTornado, DustTornadoActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MagicalArmShield, MagicalArmShieldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MagicalArmShield, MagicalArmShieldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ImperialIronWall, ImperialIronWallSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ImperialIronWall, ImperialIronWallActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TutanMask, TutanMaskSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TutanMask, TutanMaskActivate);

         }

            // All Normal Monster Methods

        private bool MasterKyonsheeNormalSummon()
        {

            return true;
        }

        private bool MasterKyonsheeMonsterSet()
        {

            return true;
        }

        private bool MasterKyonsheeRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool RedEyesZombieDragonNormalSummon()
        {

            return true;
        }

        private bool RedEyesZombieDragonMonsterSet()
        {

            return true;
        }

        private bool RedEyesZombieDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RedEyesZombieDragonActivate()
        {

            return true;
        }

        private bool RyuKokkiNormalSummon()
        {

            return true;
        }

        private bool RyuKokkiMonsterSet()
        {

            return true;
        }

        private bool RyuKokkiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RyuKokkiActivate()
        {

            return true;
        }

        private bool MalevolentMechGokuEnNormalSummon()
        {

            return true;
        }

        private bool MalevolentMechGokuEnMonsterSet()
        {

            return true;
        }

        private bool MalevolentMechGokuEnRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MalevolentMechGokuEnActivate()
        {

            return true;
        }

        private bool PatricianofDarknessNormalSummon()
        {

            return true;
        }

        private bool PatricianofDarknessMonsterSet()
        {

            return true;
        }

        private bool PatricianofDarknessRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PatricianofDarknessActivate()
        {

            return true;
        }

        private bool PaladinoftheCursedDragonNormalSummon()
        {

            return true;
        }

        private bool PaladinoftheCursedDragonMonsterSet()
        {

            return true;
        }

        private bool PaladinoftheCursedDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PaladinoftheCursedDragonActivate()
        {

            return true;
        }

        private bool RegeneratingMummyNormalSummon()
        {

            return true;
        }

        private bool RegeneratingMummyMonsterSet()
        {

            return true;
        }

        private bool RegeneratingMummyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RegeneratingMummyActivate()
        {

            return true;
        }

        private bool ZombieMasterNormalSummon()
        {

            return true;
        }

        private bool ZombieMasterMonsterSet()
        {

            return true;
        }

        private bool ZombieMasterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ZombieMasterActivate()
        {

            return true;
        }

        private bool GetsuFuhmaNormalSummon()
        {

            return true;
        }

        private bool GetsuFuhmaMonsterSet()
        {

            return true;
        }

        private bool GetsuFuhmaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GetsuFuhmaActivate()
        {

            return true;
        }

        private bool RoyalKeeperNormalSummon()
        {

            return true;
        }

        private bool RoyalKeeperMonsterSet()
        {

            return true;
        }

        private bool RoyalKeeperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RoyalKeeperActivate()
        {

            return true;
        }

        private bool GerniaNormalSummon()
        {

            return true;
        }

        private bool GerniaMonsterSet()
        {

            return true;
        }

        private bool GerniaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GerniaActivate()
        {

            return true;
        }

        private bool PyramidTurtleNormalSummon()
        {

            return true;
        }

        private bool PyramidTurtleMonsterSet()
        {

            return true;
        }

        private bool PyramidTurtleRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PyramidTurtleActivate()
        {

            return true;
        }

        private bool PlagueWolfNormalSummon()
        {

            return true;
        }

        private bool PlagueWolfMonsterSet()
        {

            return true;
        }

        private bool PlagueWolfRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PlagueWolfActivate()
        {

            return true;
        }

        private bool MarionetteMiteNormalSummon()
        {

            return true;
        }

        private bool MarionetteMiteMonsterSet()
        {

            return true;
        }

        private bool MarionetteMiteRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MarionetteMiteActivate()
        {

            return true;
        }

        private bool DesLacoodaNormalSummon()
        {

            return true;
        }

        private bool DesLacoodaMonsterSet()
        {

            return true;
        }

        private bool DesLacoodaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DesLacoodaActivate()
        {

            return true;
        }

        private bool SpiritReaperNormalSummon()
        {

            return true;
        }

        private bool SpiritReaperMonsterSet()
        {

            return true;
        }

        private bool SpiritReaperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SpiritReaperActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool BookofLifeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BookofLifeActivate()
        {

            return true;
        }

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool ColdWaveSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ColdWaveActivate()
        {

            return true;
        }

        private bool PotofAvariceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool PotofAvariceActivate()
        {

            return true;
        }

        private bool CardDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardDestructionActivate()
        {

            return true;
        }

        private bool TerraformingSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TerraformingActivate()
        {

            return true;
        }

        private bool SoulTakerSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SoulTakerActivate()
        {

            return true;
        }

        private bool MagicalStoneExcavationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MagicalStoneExcavationActivate()
        {

            return true;
        }

        private bool ShrinkSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShrinkActivate()
        {

            return true;
        }

        private bool SpellShatteringArrowSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellShatteringArrowActivate()
        {

            return true;
        }

        private bool CalloftheMummySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheMummyActivate()
        {

            return true;
        }

        private bool FieldBarrierSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FieldBarrierActivate()
        {

            return true;
        }

        private bool CardofSafeReturnSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardofSafeReturnActivate()
        {

            return true;
        }

        private bool RibbonofRebirthSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RibbonofRebirthActivate()
        {

            return true;
        }

        private bool ZombieWorldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ZombieWorldActivate()
        {

            return true;
        }

        private bool WabokuSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WabokuActivate()
        {

            return true;
        }

        private bool BottomlessTrapHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BottomlessTrapHoleActivate()
        {

            return true;
        }

        private bool DustTornadoSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DustTornadoActivate()
        {

            return true;
        }

        private bool MagicalArmShieldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MagicalArmShieldActivate()
        {

            return true;
        }

        private bool ImperialIronWallSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ImperialIronWallActivate()
        {

            return true;
        }

        private bool TutanMaskSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TutanMaskActivate()
        {

            return true;
        }

    }
}