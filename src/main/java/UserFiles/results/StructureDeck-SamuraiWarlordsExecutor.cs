using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Samurai Warlords", "StructureDeckSamuraiWarlords")]
    public class StructureDeckSamuraiWarlordsExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int ChamberlainoftheSixSamurai = 44430454;
            // Initialize all effect monsters
            public const int GreatShogunShien = 63176202;
            public const int EnishiShiensChancellor = 38280762;
            public const int GrandmasteroftheSixSamurai = 83039729;
            public const int TheSixSamuraiZanji = 95519486;
            public const int LegendarySixSamuraiKizan = 49721904;
            public const int TheSixSamuraiIrou = 27782503;
            public const int LegendarySixSamuraiEnishi = 75116619;
            public const int FutureSamurai = 90642597;
            public const int ShiensDaredevil = 98162021;
            public const int TheSixSamuraiNisashi = 31904181;
            public const int ShiensAdvisor = 98126725;
            public const int HandoftheSixSamurai = 78792195;
            public const int TheSixSamuraiKamon = 90397998;
            public const int TheSixSamuraiYaichi = 64398890;
            public const int TheImmortalBushi = 52035300;
            public const int TheSixSamuraiYariza = 69025477;
            public const int SpiritoftheSixSamurai = 65685470;
            public const int ElderoftheSixSamurai = 61737116;
            public const int LegendarySixSamuraiKageki = 2511717;
            public const int ShiensFootsoldier = 99675356;
            public const int ShiensSquire = 33883834;
            public const int ShadowoftheSixSamuraiShien = 1828513;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int ReinforcementoftheArmy = 32807846;
            public const int DarkHole = 53129443;
            public const int ShiensSmokeSignal = 54031490;
            public const int TheWarriorReturningAlive = 95281259;
            public const int CunningoftheSixSamurai = 27178262;
            public const int TheAForces = 403847;
            public const int GatewayoftheSix = 27970830;
            public const int ShiensDojo = 47436247;
            public const int SixSamuraiUnited = 72345736;
            public const int TempleoftheSix = 53819808;
            public const int DoubleEdgedSwordTechnique = 21007444;
            public const int SixStrikeThunderBlast = 23212990;
            public const int ReturnoftheSixSamurai = 46874015;
            public const int SixStyleDualWield = 75525309;
            public const int ShiensScheme = 77847678;
            public const int FiendishChain = 50078509;
            public const int RivalryofWarlords = 90846359;
            public const int MusakaniMagatama = 41458579;
            // Initialize all useless cards

         }
        public Structure Deck - Samurai WarlordsExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.ChamberlainoftheSixSamurai, ChamberlainoftheSixSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ChamberlainoftheSixSamurai, ChamberlainoftheSixSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ChamberlainoftheSixSamurai, ChamberlainoftheSixSamuraiRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.GreatShogunShien, GreatShogunShienNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GreatShogunShien, GreatShogunShienMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GreatShogunShien, GreatShogunShienRepos);
            AddExecutor(ExecutorType.Activate, CardId.GreatShogunShien, GreatShogunShienActivate);
            AddExecutor(ExecutorType.Summon, CardId.EnishiShiensChancellor, EnishiShiensChancellorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EnishiShiensChancellor, EnishiShiensChancellorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EnishiShiensChancellor, EnishiShiensChancellorRepos);
            AddExecutor(ExecutorType.Activate, CardId.EnishiShiensChancellor, EnishiShiensChancellorActivate);
            AddExecutor(ExecutorType.Summon, CardId.GrandmasteroftheSixSamurai, GrandmasteroftheSixSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GrandmasteroftheSixSamurai, GrandmasteroftheSixSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GrandmasteroftheSixSamurai, GrandmasteroftheSixSamuraiRepos);
            AddExecutor(ExecutorType.Activate, CardId.GrandmasteroftheSixSamurai, GrandmasteroftheSixSamuraiActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiZanji, TheSixSamuraiZanjiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiZanji, TheSixSamuraiZanjiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiZanji, TheSixSamuraiZanjiRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiZanji, TheSixSamuraiZanjiActivate);
            AddExecutor(ExecutorType.Summon, CardId.LegendarySixSamuraiKizan, LegendarySixSamuraiKizanNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LegendarySixSamuraiKizan, LegendarySixSamuraiKizanMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LegendarySixSamuraiKizan, LegendarySixSamuraiKizanRepos);
            AddExecutor(ExecutorType.Activate, CardId.LegendarySixSamuraiKizan, LegendarySixSamuraiKizanActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiIrou, TheSixSamuraiIrouNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiIrou, TheSixSamuraiIrouMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiIrou, TheSixSamuraiIrouRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiIrou, TheSixSamuraiIrouActivate);
            AddExecutor(ExecutorType.Summon, CardId.LegendarySixSamuraiEnishi, LegendarySixSamuraiEnishiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LegendarySixSamuraiEnishi, LegendarySixSamuraiEnishiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LegendarySixSamuraiEnishi, LegendarySixSamuraiEnishiRepos);
            AddExecutor(ExecutorType.Activate, CardId.LegendarySixSamuraiEnishi, LegendarySixSamuraiEnishiActivate);
            AddExecutor(ExecutorType.Summon, CardId.FutureSamurai, FutureSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FutureSamurai, FutureSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FutureSamurai, FutureSamuraiRepos);
            AddExecutor(ExecutorType.Activate, CardId.FutureSamurai, FutureSamuraiActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShiensDaredevil, ShiensDaredevilNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShiensDaredevil, ShiensDaredevilMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShiensDaredevil, ShiensDaredevilRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShiensDaredevil, ShiensDaredevilActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiNisashi, TheSixSamuraiNisashiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiNisashi, TheSixSamuraiNisashiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiNisashi, TheSixSamuraiNisashiRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiNisashi, TheSixSamuraiNisashiActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShiensAdvisor, ShiensAdvisorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShiensAdvisor, ShiensAdvisorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShiensAdvisor, ShiensAdvisorRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShiensAdvisor, ShiensAdvisorActivate);
            AddExecutor(ExecutorType.Summon, CardId.HandoftheSixSamurai, HandoftheSixSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HandoftheSixSamurai, HandoftheSixSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HandoftheSixSamurai, HandoftheSixSamuraiRepos);
            AddExecutor(ExecutorType.Activate, CardId.HandoftheSixSamurai, HandoftheSixSamuraiActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiKamon, TheSixSamuraiKamonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiKamon, TheSixSamuraiKamonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiKamon, TheSixSamuraiKamonRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiKamon, TheSixSamuraiKamonActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiYaichi, TheSixSamuraiYaichiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiYaichi, TheSixSamuraiYaichiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiYaichi, TheSixSamuraiYaichiRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiYaichi, TheSixSamuraiYaichiActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheImmortalBushi, TheImmortalBushiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheImmortalBushi, TheImmortalBushiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheImmortalBushi, TheImmortalBushiRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheImmortalBushi, TheImmortalBushiActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheSixSamuraiYariza, TheSixSamuraiYarizaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheSixSamuraiYariza, TheSixSamuraiYarizaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheSixSamuraiYariza, TheSixSamuraiYarizaRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheSixSamuraiYariza, TheSixSamuraiYarizaActivate);
            AddExecutor(ExecutorType.Summon, CardId.SpiritoftheSixSamurai, SpiritoftheSixSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SpiritoftheSixSamurai, SpiritoftheSixSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SpiritoftheSixSamurai, SpiritoftheSixSamuraiRepos);
            AddExecutor(ExecutorType.Activate, CardId.SpiritoftheSixSamurai, SpiritoftheSixSamuraiActivate);
            AddExecutor(ExecutorType.Summon, CardId.ElderoftheSixSamurai, ElderoftheSixSamuraiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ElderoftheSixSamurai, ElderoftheSixSamuraiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ElderoftheSixSamurai, ElderoftheSixSamuraiRepos);
            AddExecutor(ExecutorType.Activate, CardId.ElderoftheSixSamurai, ElderoftheSixSamuraiActivate);
            AddExecutor(ExecutorType.Summon, CardId.LegendarySixSamuraiKageki, LegendarySixSamuraiKagekiNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LegendarySixSamuraiKageki, LegendarySixSamuraiKagekiMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LegendarySixSamuraiKageki, LegendarySixSamuraiKagekiRepos);
            AddExecutor(ExecutorType.Activate, CardId.LegendarySixSamuraiKageki, LegendarySixSamuraiKagekiActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShiensFootsoldier, ShiensFootsoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShiensFootsoldier, ShiensFootsoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShiensFootsoldier, ShiensFootsoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShiensFootsoldier, ShiensFootsoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShiensSquire, ShiensSquireNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShiensSquire, ShiensSquireMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShiensSquire, ShiensSquireRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShiensSquire, ShiensSquireActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShadowoftheSixSamuraiShien, ShadowoftheSixSamuraiShienNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShadowoftheSixSamuraiShien, ShadowoftheSixSamuraiShienMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShadowoftheSixSamuraiShien, ShadowoftheSixSamuraiShienRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShadowoftheSixSamuraiShien, ShadowoftheSixSamuraiShienActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.ReinforcementoftheArmy, ReinforcementoftheArmySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ReinforcementoftheArmy, ReinforcementoftheArmyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, DarkHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DarkHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ShiensSmokeSignal, ShiensSmokeSignalSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ShiensSmokeSignal, ShiensSmokeSignalActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TheWarriorReturningAlive, TheWarriorReturningAliveSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheWarriorReturningAlive, TheWarriorReturningAliveActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CunningoftheSixSamurai, CunningoftheSixSamuraiSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CunningoftheSixSamurai, CunningoftheSixSamuraiActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TheAForces, TheAForcesSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheAForces, TheAForcesActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GatewayoftheSix, GatewayoftheSixSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GatewayoftheSix, GatewayoftheSixActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ShiensDojo, ShiensDojoSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ShiensDojo, ShiensDojoActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SixSamuraiUnited, SixSamuraiUnitedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SixSamuraiUnited, SixSamuraiUnitedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TempleoftheSix, TempleoftheSixSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TempleoftheSix, TempleoftheSixActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DoubleEdgedSwordTechnique, DoubleEdgedSwordTechniqueSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DoubleEdgedSwordTechnique, DoubleEdgedSwordTechniqueActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SixStrikeThunderBlast, SixStrikeThunderBlastSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SixStrikeThunderBlast, SixStrikeThunderBlastActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ReturnoftheSixSamurai, ReturnoftheSixSamuraiSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ReturnoftheSixSamurai, ReturnoftheSixSamuraiActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SixStyleDualWield, SixStyleDualWieldSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SixStyleDualWield, SixStyleDualWieldActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ShiensScheme, ShiensSchemeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ShiensScheme, ShiensSchemeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FiendishChain, FiendishChainSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FiendishChain, FiendishChainActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RivalryofWarlords, RivalryofWarlordsSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RivalryofWarlords, RivalryofWarlordsActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MusakaniMagatama, MusakaniMagatamaSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MusakaniMagatama, MusakaniMagatamaActivate);

         }

            // All Normal Monster Methods

        private bool ChamberlainoftheSixSamuraiNormalSummon()
        {

            return true;
        }

        private bool ChamberlainoftheSixSamuraiMonsterSet()
        {

            return true;
        }

        private bool ChamberlainoftheSixSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool GreatShogunShienNormalSummon()
        {

            return true;
        }

        private bool GreatShogunShienMonsterSet()
        {

            return true;
        }

        private bool GreatShogunShienRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GreatShogunShienActivate()
        {

            return true;
        }

        private bool EnishiShiensChancellorNormalSummon()
        {

            return true;
        }

        private bool EnishiShiensChancellorMonsterSet()
        {

            return true;
        }

        private bool EnishiShiensChancellorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EnishiShiensChancellorActivate()
        {

            return true;
        }

        private bool GrandmasteroftheSixSamuraiNormalSummon()
        {

            return true;
        }

        private bool GrandmasteroftheSixSamuraiMonsterSet()
        {

            return true;
        }

        private bool GrandmasteroftheSixSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GrandmasteroftheSixSamuraiActivate()
        {

            return true;
        }

        private bool TheSixSamuraiZanjiNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiZanjiMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiZanjiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiZanjiActivate()
        {

            return true;
        }

        private bool LegendarySixSamuraiKizanNormalSummon()
        {

            return true;
        }

        private bool LegendarySixSamuraiKizanMonsterSet()
        {

            return true;
        }

        private bool LegendarySixSamuraiKizanRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LegendarySixSamuraiKizanActivate()
        {

            return true;
        }

        private bool TheSixSamuraiIrouNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiIrouMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiIrouRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiIrouActivate()
        {

            return true;
        }

        private bool LegendarySixSamuraiEnishiNormalSummon()
        {

            return true;
        }

        private bool LegendarySixSamuraiEnishiMonsterSet()
        {

            return true;
        }

        private bool LegendarySixSamuraiEnishiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LegendarySixSamuraiEnishiActivate()
        {

            return true;
        }

        private bool FutureSamuraiNormalSummon()
        {

            return true;
        }

        private bool FutureSamuraiMonsterSet()
        {

            return true;
        }

        private bool FutureSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FutureSamuraiActivate()
        {

            return true;
        }

        private bool ShiensDaredevilNormalSummon()
        {

            return true;
        }

        private bool ShiensDaredevilMonsterSet()
        {

            return true;
        }

        private bool ShiensDaredevilRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShiensDaredevilActivate()
        {

            return true;
        }

        private bool TheSixSamuraiNisashiNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiNisashiMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiNisashiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiNisashiActivate()
        {

            return true;
        }

        private bool ShiensAdvisorNormalSummon()
        {

            return true;
        }

        private bool ShiensAdvisorMonsterSet()
        {

            return true;
        }

        private bool ShiensAdvisorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShiensAdvisorActivate()
        {

            return true;
        }

        private bool HandoftheSixSamuraiNormalSummon()
        {

            return true;
        }

        private bool HandoftheSixSamuraiMonsterSet()
        {

            return true;
        }

        private bool HandoftheSixSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HandoftheSixSamuraiActivate()
        {

            return true;
        }

        private bool TheSixSamuraiKamonNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiKamonMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiKamonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiKamonActivate()
        {

            return true;
        }

        private bool TheSixSamuraiYaichiNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiYaichiMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiYaichiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiYaichiActivate()
        {

            return true;
        }

        private bool TheImmortalBushiNormalSummon()
        {

            return true;
        }

        private bool TheImmortalBushiMonsterSet()
        {

            return true;
        }

        private bool TheImmortalBushiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheImmortalBushiActivate()
        {

            return true;
        }

        private bool TheSixSamuraiYarizaNormalSummon()
        {

            return true;
        }

        private bool TheSixSamuraiYarizaMonsterSet()
        {

            return true;
        }

        private bool TheSixSamuraiYarizaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheSixSamuraiYarizaActivate()
        {

            return true;
        }

        private bool SpiritoftheSixSamuraiNormalSummon()
        {

            return true;
        }

        private bool SpiritoftheSixSamuraiMonsterSet()
        {

            return true;
        }

        private bool SpiritoftheSixSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SpiritoftheSixSamuraiActivate()
        {

            return true;
        }

        private bool ElderoftheSixSamuraiNormalSummon()
        {

            return true;
        }

        private bool ElderoftheSixSamuraiMonsterSet()
        {

            return true;
        }

        private bool ElderoftheSixSamuraiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ElderoftheSixSamuraiActivate()
        {

            return true;
        }

        private bool LegendarySixSamuraiKagekiNormalSummon()
        {

            return true;
        }

        private bool LegendarySixSamuraiKagekiMonsterSet()
        {

            return true;
        }

        private bool LegendarySixSamuraiKagekiRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LegendarySixSamuraiKagekiActivate()
        {

            return true;
        }

        private bool ShiensFootsoldierNormalSummon()
        {

            return true;
        }

        private bool ShiensFootsoldierMonsterSet()
        {

            return true;
        }

        private bool ShiensFootsoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShiensFootsoldierActivate()
        {

            return true;
        }

        private bool ShiensSquireNormalSummon()
        {

            return true;
        }

        private bool ShiensSquireMonsterSet()
        {

            return true;
        }

        private bool ShiensSquireRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShiensSquireActivate()
        {

            return true;
        }

        private bool ShadowoftheSixSamuraiShienNormalSummon()
        {

            return true;
        }

        private bool ShadowoftheSixSamuraiShienMonsterSet()
        {

            return true;
        }

        private bool ShadowoftheSixSamuraiShienRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShadowoftheSixSamuraiShienActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool ReinforcementoftheArmySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ReinforcementoftheArmyActivate()
        {

            return true;
        }

        private bool DarkHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkHoleActivate()
        {

            return true;
        }

        private bool ShiensSmokeSignalSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShiensSmokeSignalActivate()
        {

            return true;
        }

        private bool TheWarriorReturningAliveSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheWarriorReturningAliveActivate()
        {

            return true;
        }

        private bool CunningoftheSixSamuraiSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CunningoftheSixSamuraiActivate()
        {

            return true;
        }

        private bool TheAForcesSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheAForcesActivate()
        {

            return true;
        }

        private bool GatewayoftheSixSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GatewayoftheSixActivate()
        {

            return true;
        }

        private bool ShiensDojoSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShiensDojoActivate()
        {

            return true;
        }

        private bool SixSamuraiUnitedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SixSamuraiUnitedActivate()
        {

            return true;
        }

        private bool TempleoftheSixSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TempleoftheSixActivate()
        {

            return true;
        }

        private bool DoubleEdgedSwordTechniqueSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DoubleEdgedSwordTechniqueActivate()
        {

            return true;
        }

        private bool SixStrikeThunderBlastSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SixStrikeThunderBlastActivate()
        {

            return true;
        }

        private bool ReturnoftheSixSamuraiSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ReturnoftheSixSamuraiActivate()
        {

            return true;
        }

        private bool SixStyleDualWieldSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SixStyleDualWieldActivate()
        {

            return true;
        }

        private bool ShiensSchemeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShiensSchemeActivate()
        {

            return true;
        }

        private bool FiendishChainSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FiendishChainActivate()
        {

            return true;
        }

        private bool RivalryofWarlordsSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RivalryofWarlordsActivate()
        {

            return true;
        }

        private bool MusakaniMagatamaSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MusakaniMagatamaActivate()
        {

            return true;
        }

    }
}