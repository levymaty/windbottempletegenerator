using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Dragunity Legion", "StructureDeckDragunityLegion")]
    public class StructureDeckDragunityLegionExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int DragunityArmaLeyvaten = 63487632;
            public const int DragunityArmaMystletainn = 876330;
            public const int DragunityAklys = 36870345;
            public const int DragunityDux = 28183605;
            public const int DragunityLegionnaire = 54578613;
            public const int DragunityTribus = 81962318;
            public const int DragunityDarkspear = 13361027;
            public const int DragunityMilitum = 81661951;
            public const int DragunityPrimusPilus = 18060565;
            public const int DragunityBrandistock = 54455664;
            public const int DragunityJavelin = 80549379;
            public const int MistValleyFalcon = 82199284;
            public const int HunterOwl = 51962254;
            public const int GarudatheWindSpirit = 12800777;
            public const int FlyingKamakiri1 = 84834865;
            public const int SpearDragon = 31553716;
            public const int TwinHeadedBehemoth = 43586926;
            public const int ArmedDragonLV3 = 980973;
            public const int ArmedDragonLV5 = 46384672;
            public const int MaskedDragon = 39191307;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int DragonRavine = 62265044;
            public const int DragonMastery = 99659159;
            public const int UnitedWeStand = 56747793;
            public const int MagePower = 83746708;
            public const int DragonsGunfire = 55991637;
            public const int StampingDestruction = 81385346;
            public const int CreatureSwap = 31036355;
            public const int MonsterReincarnation = 74848038;
            public const int FoolishBurial = 81439174;
            public const int CardDestruction = 72892473;
            public const int WindstormofEtaqua = 59744639;
            public const int RelieveMonster = 37507488;
            public const int LegacyofYataGarasu = 30461781;
            public const int FinalAttackOrders = 52503575;
            public const int MirrorForce = 44095762;
            public const int DragonsRage = 54178050;
            public const int BottomlessTrapHole = 29401950;
            public const int SpiritualWindArtMiyabi = 79333300;
            public const int IcarusAttack = 53567095;
            // Initialize all useless cards

         }
        public Structure Deck - Dragunity LegionExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.DragunityArmaLeyvaten, DragunityArmaLeyvatenNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityArmaLeyvaten, DragunityArmaLeyvatenMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityArmaLeyvaten, DragunityArmaLeyvatenRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityArmaLeyvaten, DragunityArmaLeyvatenActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityArmaMystletainn, DragunityArmaMystletainnNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityArmaMystletainn, DragunityArmaMystletainnMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityArmaMystletainn, DragunityArmaMystletainnRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityArmaMystletainn, DragunityArmaMystletainnActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityAklys, DragunityAklysNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityAklys, DragunityAklysMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityAklys, DragunityAklysRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityAklys, DragunityAklysActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityDux, DragunityDuxNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityDux, DragunityDuxMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityDux, DragunityDuxRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityDux, DragunityDuxActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityLegionnaire, DragunityLegionnaireNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityLegionnaire, DragunityLegionnaireMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityLegionnaire, DragunityLegionnaireRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityLegionnaire, DragunityLegionnaireActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityTribus, DragunityTribusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityTribus, DragunityTribusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityTribus, DragunityTribusRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityTribus, DragunityTribusActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityDarkspear, DragunityDarkspearNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityDarkspear, DragunityDarkspearMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityDarkspear, DragunityDarkspearRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityDarkspear, DragunityDarkspearActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityMilitum, DragunityMilitumNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityMilitum, DragunityMilitumMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityMilitum, DragunityMilitumRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityMilitum, DragunityMilitumActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityPrimusPilus, DragunityPrimusPilusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityPrimusPilus, DragunityPrimusPilusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityPrimusPilus, DragunityPrimusPilusRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityPrimusPilus, DragunityPrimusPilusActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityBrandistock, DragunityBrandistockNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityBrandistock, DragunityBrandistockMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityBrandistock, DragunityBrandistockRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityBrandistock, DragunityBrandistockActivate);
            AddExecutor(ExecutorType.Summon, CardId.DragunityJavelin, DragunityJavelinNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DragunityJavelin, DragunityJavelinMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DragunityJavelin, DragunityJavelinRepos);
            AddExecutor(ExecutorType.Activate, CardId.DragunityJavelin, DragunityJavelinActivate);
            AddExecutor(ExecutorType.Summon, CardId.MistValleyFalcon, MistValleyFalconNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MistValleyFalcon, MistValleyFalconMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MistValleyFalcon, MistValleyFalconRepos);
            AddExecutor(ExecutorType.Activate, CardId.MistValleyFalcon, MistValleyFalconActivate);
            AddExecutor(ExecutorType.Summon, CardId.HunterOwl, HunterOwlNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HunterOwl, HunterOwlMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HunterOwl, HunterOwlRepos);
            AddExecutor(ExecutorType.Activate, CardId.HunterOwl, HunterOwlActivate);
            AddExecutor(ExecutorType.Summon, CardId.GarudatheWindSpirit, GarudatheWindSpiritNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GarudatheWindSpirit, GarudatheWindSpiritMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GarudatheWindSpirit, GarudatheWindSpiritRepos);
            AddExecutor(ExecutorType.Activate, CardId.GarudatheWindSpirit, GarudatheWindSpiritActivate);
            AddExecutor(ExecutorType.Summon, CardId.FlyingKamakiri1, FlyingKamakiri1NormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FlyingKamakiri1, FlyingKamakiri1MonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FlyingKamakiri1, FlyingKamakiri1Repos);
            AddExecutor(ExecutorType.Activate, CardId.FlyingKamakiri1, FlyingKamakiri1Activate);
            AddExecutor(ExecutorType.Summon, CardId.SpearDragon, SpearDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SpearDragon, SpearDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SpearDragon, SpearDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.SpearDragon, SpearDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.TwinHeadedBehemoth, TwinHeadedBehemothNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TwinHeadedBehemoth, TwinHeadedBehemothMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TwinHeadedBehemoth, TwinHeadedBehemothRepos);
            AddExecutor(ExecutorType.Activate, CardId.TwinHeadedBehemoth, TwinHeadedBehemothActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArmedDragonLV3, ArmedDragonLV3NormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArmedDragonLV3, ArmedDragonLV3MonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArmedDragonLV3, ArmedDragonLV3Repos);
            AddExecutor(ExecutorType.Activate, CardId.ArmedDragonLV3, ArmedDragonLV3Activate);
            AddExecutor(ExecutorType.Summon, CardId.ArmedDragonLV5, ArmedDragonLV5NormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArmedDragonLV5, ArmedDragonLV5MonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArmedDragonLV5, ArmedDragonLV5Repos);
            AddExecutor(ExecutorType.Activate, CardId.ArmedDragonLV5, ArmedDragonLV5Activate);
            AddExecutor(ExecutorType.Summon, CardId.MaskedDragon, MaskedDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MaskedDragon, MaskedDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MaskedDragon, MaskedDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.MaskedDragon, MaskedDragonActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.DragonRavine, DragonRavineSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DragonRavine, DragonRavineActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DragonMastery, DragonMasterySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DragonMastery, DragonMasteryActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.UnitedWeStand, UnitedWeStandSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.UnitedWeStand, UnitedWeStandActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MagePower, MagePowerSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MagePower, MagePowerActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DragonsGunfire, DragonsGunfireSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DragonsGunfire, DragonsGunfireActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.StampingDestruction, StampingDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.StampingDestruction, StampingDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MonsterReincarnation, MonsterReincarnationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReincarnation, MonsterReincarnationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FoolishBurial, FoolishBurialSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FoolishBurial, FoolishBurialActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardDestruction, CardDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardDestruction, CardDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.WindstormofEtaqua, WindstormofEtaquaSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.WindstormofEtaqua, WindstormofEtaquaActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RelieveMonster, RelieveMonsterSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RelieveMonster, RelieveMonsterActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LegacyofYataGarasu, LegacyofYataGarasuSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LegacyofYataGarasu, LegacyofYataGarasuActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FinalAttackOrders, FinalAttackOrdersSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FinalAttackOrders, FinalAttackOrdersActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, MirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, MirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DragonsRage, DragonsRageSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DragonsRage, DragonsRageActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BottomlessTrapHole, BottomlessTrapHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BottomlessTrapHole, BottomlessTrapHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpiritualWindArtMiyabi, SpiritualWindArtMiyabiSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpiritualWindArtMiyabi, SpiritualWindArtMiyabiActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.IcarusAttack, IcarusAttackSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.IcarusAttack, IcarusAttackActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool DragunityArmaLeyvatenNormalSummon()
        {

            return true;
        }

        private bool DragunityArmaLeyvatenMonsterSet()
        {

            return true;
        }

        private bool DragunityArmaLeyvatenRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityArmaLeyvatenActivate()
        {

            return true;
        }

        private bool DragunityArmaMystletainnNormalSummon()
        {

            return true;
        }

        private bool DragunityArmaMystletainnMonsterSet()
        {

            return true;
        }

        private bool DragunityArmaMystletainnRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityArmaMystletainnActivate()
        {

            return true;
        }

        private bool DragunityAklysNormalSummon()
        {

            return true;
        }

        private bool DragunityAklysMonsterSet()
        {

            return true;
        }

        private bool DragunityAklysRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityAklysActivate()
        {

            return true;
        }

        private bool DragunityDuxNormalSummon()
        {

            return true;
        }

        private bool DragunityDuxMonsterSet()
        {

            return true;
        }

        private bool DragunityDuxRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityDuxActivate()
        {

            return true;
        }

        private bool DragunityLegionnaireNormalSummon()
        {

            return true;
        }

        private bool DragunityLegionnaireMonsterSet()
        {

            return true;
        }

        private bool DragunityLegionnaireRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityLegionnaireActivate()
        {

            return true;
        }

        private bool DragunityTribusNormalSummon()
        {

            return true;
        }

        private bool DragunityTribusMonsterSet()
        {

            return true;
        }

        private bool DragunityTribusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityTribusActivate()
        {

            return true;
        }

        private bool DragunityDarkspearNormalSummon()
        {

            return true;
        }

        private bool DragunityDarkspearMonsterSet()
        {

            return true;
        }

        private bool DragunityDarkspearRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityDarkspearActivate()
        {

            return true;
        }

        private bool DragunityMilitumNormalSummon()
        {

            return true;
        }

        private bool DragunityMilitumMonsterSet()
        {

            return true;
        }

        private bool DragunityMilitumRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityMilitumActivate()
        {

            return true;
        }

        private bool DragunityPrimusPilusNormalSummon()
        {

            return true;
        }

        private bool DragunityPrimusPilusMonsterSet()
        {

            return true;
        }

        private bool DragunityPrimusPilusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityPrimusPilusActivate()
        {

            return true;
        }

        private bool DragunityBrandistockNormalSummon()
        {

            return true;
        }

        private bool DragunityBrandistockMonsterSet()
        {

            return true;
        }

        private bool DragunityBrandistockRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityBrandistockActivate()
        {

            return true;
        }

        private bool DragunityJavelinNormalSummon()
        {

            return true;
        }

        private bool DragunityJavelinMonsterSet()
        {

            return true;
        }

        private bool DragunityJavelinRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DragunityJavelinActivate()
        {

            return true;
        }

        private bool MistValleyFalconNormalSummon()
        {

            return true;
        }

        private bool MistValleyFalconMonsterSet()
        {

            return true;
        }

        private bool MistValleyFalconRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MistValleyFalconActivate()
        {

            return true;
        }

        private bool HunterOwlNormalSummon()
        {

            return true;
        }

        private bool HunterOwlMonsterSet()
        {

            return true;
        }

        private bool HunterOwlRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HunterOwlActivate()
        {

            return true;
        }

        private bool GarudatheWindSpiritNormalSummon()
        {

            return true;
        }

        private bool GarudatheWindSpiritMonsterSet()
        {

            return true;
        }

        private bool GarudatheWindSpiritRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GarudatheWindSpiritActivate()
        {

            return true;
        }

        private bool FlyingKamakiri1NormalSummon()
        {

            return true;
        }

        private bool FlyingKamakiri1MonsterSet()
        {

            return true;
        }

        private bool FlyingKamakiri1Repos()
        {

            return DefaultMonsterRepos;
        }

        private bool FlyingKamakiri1Activate()
        {

            return true;
        }

        private bool SpearDragonNormalSummon()
        {

            return true;
        }

        private bool SpearDragonMonsterSet()
        {

            return true;
        }

        private bool SpearDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SpearDragonActivate()
        {

            return true;
        }

        private bool TwinHeadedBehemothNormalSummon()
        {

            return true;
        }

        private bool TwinHeadedBehemothMonsterSet()
        {

            return true;
        }

        private bool TwinHeadedBehemothRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TwinHeadedBehemothActivate()
        {

            return true;
        }

        private bool ArmedDragonLV3NormalSummon()
        {

            return true;
        }

        private bool ArmedDragonLV3MonsterSet()
        {

            return true;
        }

        private bool ArmedDragonLV3Repos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArmedDragonLV3Activate()
        {

            return true;
        }

        private bool ArmedDragonLV5NormalSummon()
        {

            return true;
        }

        private bool ArmedDragonLV5MonsterSet()
        {

            return true;
        }

        private bool ArmedDragonLV5Repos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArmedDragonLV5Activate()
        {

            return true;
        }

        private bool MaskedDragonNormalSummon()
        {

            return true;
        }

        private bool MaskedDragonMonsterSet()
        {

            return true;
        }

        private bool MaskedDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MaskedDragonActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool DragonRavineSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DragonRavineActivate()
        {

            return true;
        }

        private bool DragonMasterySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DragonMasteryActivate()
        {

            return true;
        }

        private bool UnitedWeStandSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool UnitedWeStandActivate()
        {

            return true;
        }

        private bool MagePowerSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MagePowerActivate()
        {

            return true;
        }

        private bool DragonsGunfireSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DragonsGunfireActivate()
        {

            return true;
        }

        private bool StampingDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool StampingDestructionActivate()
        {

            return true;
        }

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool MonsterReincarnationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MonsterReincarnationActivate()
        {

            return true;
        }

        private bool FoolishBurialSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FoolishBurialActivate()
        {

            return true;
        }

        private bool CardDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardDestructionActivate()
        {

            return true;
        }

        private bool WindstormofEtaquaSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool WindstormofEtaquaActivate()
        {

            return true;
        }

        private bool RelieveMonsterSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RelieveMonsterActivate()
        {

            return true;
        }

        private bool LegacyofYataGarasuSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LegacyofYataGarasuActivate()
        {

            return true;
        }

        private bool FinalAttackOrdersSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FinalAttackOrdersActivate()
        {

            return true;
        }

        private bool MirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MirrorForceActivate()
        {

            return true;
        }

        private bool DragonsRageSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DragonsRageActivate()
        {

            return true;
        }

        private bool BottomlessTrapHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BottomlessTrapHoleActivate()
        {

            return true;
        }

        private bool SpiritualWindArtMiyabiSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpiritualWindArtMiyabiActivate()
        {

            return true;
        }

        private bool IcarusAttackSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool IcarusAttackActivate()
        {

            return true;
        }

    }
}