using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Macro Burner", "MacroBurner")]
    public class MacroBurnerExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int LavaGolem = 102380;
            public const int HeliosDuoMegistus = 80887952;
            public const int VolticKong = 93151201;
            public const int DDSurvivor = 48092532;
            public const int DDAssailant = 70074904;
            public const int ShieldWorm = 15939448;
            public const int HeliosThePrimordialSun = 54493213;
            public const int BanisheroftheRadiance = 94853057;
            public const int MorphingJar2 = 79106360;
            public const int FirebrandHymnist = 373085;
            public const int WarmWorm = 16751086;
            public const int BanisheroftheLight = 61528025;
            public const int GrenMajuDaEiza = 36584821;
            public const int InfernityBeetle = 49080532;
            public const int NeedleWorm = 81843628;
            public const int MorphingJar = 33508719;
            public const int Scapeghost = 67284107;
            public const int EaterofMillions = 63845230;
            public const int IronChainDragon = 19974580;
            public const int DarkDiviner = 31919988;
            public const int DigvorzhakKingofHeavyIndustry = 29515122;
            public const int Number104Masquerade = 2061963;
            public const int TimeThiefRedoer = 55285840;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int DarkHole = 53129443;
            public const int SwordsofRevealingLight = 72302403;
            public const int CardDestruction = 72892473;
            public const int HandDestruction = 74519184;
            public const int GravekeepersServant = 16762927;
            public const int MessengerofPeace = 44656491;
            public const int DimensionalFissure = 81674782;
            public const int DDDynamite = 8628798;
            public const int MirrorForce = 44095762;
            public const int AssaultonGHQ = 62633180;
            public const int MacroCosmos = 30241314;
            public const int GravityBind = 85742772;
            // Initialize all useless cards

         }
        public Macro BurnerExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.LavaGolem, LavaGolemNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LavaGolem, LavaGolemMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LavaGolem, LavaGolemRepos);
            AddExecutor(ExecutorType.Activate, CardId.LavaGolem, LavaGolemActivate);
            AddExecutor(ExecutorType.Summon, CardId.HeliosDuoMegistus, HeliosDuoMegistusNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HeliosDuoMegistus, HeliosDuoMegistusMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HeliosDuoMegistus, HeliosDuoMegistusRepos);
            AddExecutor(ExecutorType.Activate, CardId.HeliosDuoMegistus, HeliosDuoMegistusActivate);
            AddExecutor(ExecutorType.Summon, CardId.VolticKong, VolticKongNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VolticKong, VolticKongMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VolticKong, VolticKongRepos);
            AddExecutor(ExecutorType.Activate, CardId.VolticKong, VolticKongActivate);
            AddExecutor(ExecutorType.Summon, CardId.DDSurvivor, DDSurvivorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DDSurvivor, DDSurvivorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DDSurvivor, DDSurvivorRepos);
            AddExecutor(ExecutorType.Activate, CardId.DDSurvivor, DDSurvivorActivate);
            AddExecutor(ExecutorType.Summon, CardId.DDAssailant, DDAssailantNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DDAssailant, DDAssailantMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DDAssailant, DDAssailantRepos);
            AddExecutor(ExecutorType.Activate, CardId.DDAssailant, DDAssailantActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShieldWorm, ShieldWormNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShieldWorm, ShieldWormMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShieldWorm, ShieldWormRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShieldWorm, ShieldWormActivate);
            AddExecutor(ExecutorType.Summon, CardId.HeliosThePrimordialSun, HeliosThePrimordialSunNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HeliosThePrimordialSun, HeliosThePrimordialSunMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HeliosThePrimordialSun, HeliosThePrimordialSunRepos);
            AddExecutor(ExecutorType.Activate, CardId.HeliosThePrimordialSun, HeliosThePrimordialSunActivate);
            AddExecutor(ExecutorType.Summon, CardId.BanisheroftheRadiance, BanisheroftheRadianceNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BanisheroftheRadiance, BanisheroftheRadianceMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BanisheroftheRadiance, BanisheroftheRadianceRepos);
            AddExecutor(ExecutorType.Activate, CardId.BanisheroftheRadiance, BanisheroftheRadianceActivate);
            AddExecutor(ExecutorType.Summon, CardId.MorphingJar2, MorphingJar2NormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MorphingJar2, MorphingJar2MonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MorphingJar2, MorphingJar2Repos);
            AddExecutor(ExecutorType.Activate, CardId.MorphingJar2, MorphingJar2Activate);
            AddExecutor(ExecutorType.Summon, CardId.FirebrandHymnist, FirebrandHymnistNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FirebrandHymnist, FirebrandHymnistMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FirebrandHymnist, FirebrandHymnistRepos);
            AddExecutor(ExecutorType.Activate, CardId.FirebrandHymnist, FirebrandHymnistActivate);
            AddExecutor(ExecutorType.Summon, CardId.WarmWorm, WarmWormNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WarmWorm, WarmWormMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WarmWorm, WarmWormRepos);
            AddExecutor(ExecutorType.Activate, CardId.WarmWorm, WarmWormActivate);
            AddExecutor(ExecutorType.Summon, CardId.BanisheroftheLight, BanisheroftheLightNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BanisheroftheLight, BanisheroftheLightMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BanisheroftheLight, BanisheroftheLightRepos);
            AddExecutor(ExecutorType.Activate, CardId.BanisheroftheLight, BanisheroftheLightActivate);
            AddExecutor(ExecutorType.Summon, CardId.GrenMajuDaEiza, GrenMajuDaEizaNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GrenMajuDaEiza, GrenMajuDaEizaMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GrenMajuDaEiza, GrenMajuDaEizaRepos);
            AddExecutor(ExecutorType.Activate, CardId.GrenMajuDaEiza, GrenMajuDaEizaActivate);
            AddExecutor(ExecutorType.Summon, CardId.InfernityBeetle, InfernityBeetleNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.InfernityBeetle, InfernityBeetleMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.InfernityBeetle, InfernityBeetleRepos);
            AddExecutor(ExecutorType.Activate, CardId.InfernityBeetle, InfernityBeetleActivate);
            AddExecutor(ExecutorType.Summon, CardId.NeedleWorm, NeedleWormNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.NeedleWorm, NeedleWormMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.NeedleWorm, NeedleWormRepos);
            AddExecutor(ExecutorType.Activate, CardId.NeedleWorm, NeedleWormActivate);
            AddExecutor(ExecutorType.Summon, CardId.MorphingJar, MorphingJarNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MorphingJar, MorphingJarMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MorphingJar, MorphingJarRepos);
            AddExecutor(ExecutorType.Activate, CardId.MorphingJar, MorphingJarActivate);
            AddExecutor(ExecutorType.Summon, CardId.Scapeghost, ScapeghostNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Scapeghost, ScapeghostMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Scapeghost, ScapeghostRepos);
            AddExecutor(ExecutorType.Activate, CardId.Scapeghost, ScapeghostActivate);
            AddExecutor(ExecutorType.Summon, CardId.EaterofMillions, EaterofMillionsNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EaterofMillions, EaterofMillionsMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EaterofMillions, EaterofMillionsRepos);
            AddExecutor(ExecutorType.Activate, CardId.EaterofMillions, EaterofMillionsActivate);
            AddExecutor(ExecutorType.Summon, CardId.IronChainDragon, IronChainDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.IronChainDragon, IronChainDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.IronChainDragon, IronChainDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.IronChainDragon, IronChainDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.DarkDiviner, DarkDivinerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DarkDiviner, DarkDivinerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DarkDiviner, DarkDivinerRepos);
            AddExecutor(ExecutorType.Activate, CardId.DarkDiviner, DarkDivinerActivate);
            AddExecutor(ExecutorType.Summon, CardId.DigvorzhakKingofHeavyIndustry, DigvorzhakKingofHeavyIndustryNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DigvorzhakKingofHeavyIndustry, DigvorzhakKingofHeavyIndustryMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DigvorzhakKingofHeavyIndustry, DigvorzhakKingofHeavyIndustryRepos);
            AddExecutor(ExecutorType.Activate, CardId.DigvorzhakKingofHeavyIndustry, DigvorzhakKingofHeavyIndustryActivate);
            AddExecutor(ExecutorType.Summon, CardId.Number104Masquerade, Number104MasqueradeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Number104Masquerade, Number104MasqueradeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Number104Masquerade, Number104MasqueradeRepos);
            AddExecutor(ExecutorType.Activate, CardId.Number104Masquerade, Number104MasqueradeActivate);
            AddExecutor(ExecutorType.Summon, CardId.TimeThiefRedoer, TimeThiefRedoerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TimeThiefRedoer, TimeThiefRedoerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TimeThiefRedoer, TimeThiefRedoerRepos);
            AddExecutor(ExecutorType.Activate, CardId.TimeThiefRedoer, TimeThiefRedoerActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, DarkHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DarkHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SwordsofRevealingLight, SwordsofRevealingLightSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SwordsofRevealingLight, SwordsofRevealingLightActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardDestruction, CardDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardDestruction, CardDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HandDestruction, HandDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HandDestruction, HandDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GravekeepersServant, GravekeepersServantSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GravekeepersServant, GravekeepersServantActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MessengerofPeace, MessengerofPeaceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MessengerofPeace, MessengerofPeaceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalFissure, DimensionalFissureSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DimensionalFissure, DimensionalFissureActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DDDynamite, DDDynamiteSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DDDynamite, DDDynamiteActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, MirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, MirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.AssaultonGHQ, AssaultonGHQSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.AssaultonGHQ, AssaultonGHQActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MacroCosmos, MacroCosmosSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MacroCosmos, MacroCosmosActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.GravityBind, GravityBindSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.GravityBind, GravityBindActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool LavaGolemNormalSummon()
        {

            return true;
        }

        private bool LavaGolemMonsterSet()
        {

            return true;
        }

        private bool LavaGolemRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LavaGolemActivate()
        {

            return true;
        }

        private bool HeliosDuoMegistusNormalSummon()
        {

            return true;
        }

        private bool HeliosDuoMegistusMonsterSet()
        {

            return true;
        }

        private bool HeliosDuoMegistusRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HeliosDuoMegistusActivate()
        {

            return true;
        }

        private bool VolticKongNormalSummon()
        {

            return true;
        }

        private bool VolticKongMonsterSet()
        {

            return true;
        }

        private bool VolticKongRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VolticKongActivate()
        {

            return true;
        }

        private bool DDSurvivorNormalSummon()
        {

            return true;
        }

        private bool DDSurvivorMonsterSet()
        {

            return true;
        }

        private bool DDSurvivorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DDSurvivorActivate()
        {

            return true;
        }

        private bool DDAssailantNormalSummon()
        {

            return true;
        }

        private bool DDAssailantMonsterSet()
        {

            return true;
        }

        private bool DDAssailantRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DDAssailantActivate()
        {

            return true;
        }

        private bool ShieldWormNormalSummon()
        {

            return true;
        }

        private bool ShieldWormMonsterSet()
        {

            return true;
        }

        private bool ShieldWormRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShieldWormActivate()
        {

            return true;
        }

        private bool HeliosThePrimordialSunNormalSummon()
        {

            return true;
        }

        private bool HeliosThePrimordialSunMonsterSet()
        {

            return true;
        }

        private bool HeliosThePrimordialSunRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HeliosThePrimordialSunActivate()
        {

            return true;
        }

        private bool BanisheroftheRadianceNormalSummon()
        {

            return true;
        }

        private bool BanisheroftheRadianceMonsterSet()
        {

            return true;
        }

        private bool BanisheroftheRadianceRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BanisheroftheRadianceActivate()
        {

            return true;
        }

        private bool MorphingJar2NormalSummon()
        {

            return true;
        }

        private bool MorphingJar2MonsterSet()
        {

            return true;
        }

        private bool MorphingJar2Repos()
        {

            return DefaultMonsterRepos;
        }

        private bool MorphingJar2Activate()
        {

            return true;
        }

        private bool FirebrandHymnistNormalSummon()
        {

            return true;
        }

        private bool FirebrandHymnistMonsterSet()
        {

            return true;
        }

        private bool FirebrandHymnistRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FirebrandHymnistActivate()
        {

            return true;
        }

        private bool WarmWormNormalSummon()
        {

            return true;
        }

        private bool WarmWormMonsterSet()
        {

            return true;
        }

        private bool WarmWormRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WarmWormActivate()
        {

            return true;
        }

        private bool BanisheroftheLightNormalSummon()
        {

            return true;
        }

        private bool BanisheroftheLightMonsterSet()
        {

            return true;
        }

        private bool BanisheroftheLightRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BanisheroftheLightActivate()
        {

            return true;
        }

        private bool GrenMajuDaEizaNormalSummon()
        {

            return true;
        }

        private bool GrenMajuDaEizaMonsterSet()
        {

            return true;
        }

        private bool GrenMajuDaEizaRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GrenMajuDaEizaActivate()
        {

            return true;
        }

        private bool InfernityBeetleNormalSummon()
        {

            return true;
        }

        private bool InfernityBeetleMonsterSet()
        {

            return true;
        }

        private bool InfernityBeetleRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool InfernityBeetleActivate()
        {

            return true;
        }

        private bool NeedleWormNormalSummon()
        {

            return true;
        }

        private bool NeedleWormMonsterSet()
        {

            return true;
        }

        private bool NeedleWormRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool NeedleWormActivate()
        {

            return true;
        }

        private bool MorphingJarNormalSummon()
        {

            return true;
        }

        private bool MorphingJarMonsterSet()
        {

            return true;
        }

        private bool MorphingJarRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MorphingJarActivate()
        {

            return true;
        }

        private bool ScapeghostNormalSummon()
        {

            return true;
        }

        private bool ScapeghostMonsterSet()
        {

            return true;
        }

        private bool ScapeghostRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ScapeghostActivate()
        {

            return true;
        }

        private bool EaterofMillionsNormalSummon()
        {

            return true;
        }

        private bool EaterofMillionsMonsterSet()
        {

            return true;
        }

        private bool EaterofMillionsRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EaterofMillionsActivate()
        {

            return true;
        }

        private bool IronChainDragonNormalSummon()
        {

            return true;
        }

        private bool IronChainDragonMonsterSet()
        {

            return true;
        }

        private bool IronChainDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool IronChainDragonActivate()
        {

            return true;
        }

        private bool DarkDivinerNormalSummon()
        {

            return true;
        }

        private bool DarkDivinerMonsterSet()
        {

            return true;
        }

        private bool DarkDivinerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DarkDivinerActivate()
        {

            return true;
        }

        private bool DigvorzhakKingofHeavyIndustryNormalSummon()
        {

            return true;
        }

        private bool DigvorzhakKingofHeavyIndustryMonsterSet()
        {

            return true;
        }

        private bool DigvorzhakKingofHeavyIndustryRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DigvorzhakKingofHeavyIndustryActivate()
        {

            return true;
        }

        private bool Number104MasqueradeNormalSummon()
        {

            return true;
        }

        private bool Number104MasqueradeMonsterSet()
        {

            return true;
        }

        private bool Number104MasqueradeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool Number104MasqueradeActivate()
        {

            return true;
        }

        private bool TimeThiefRedoerNormalSummon()
        {

            return true;
        }

        private bool TimeThiefRedoerMonsterSet()
        {

            return true;
        }

        private bool TimeThiefRedoerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TimeThiefRedoerActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool DarkHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkHoleActivate()
        {

            return true;
        }

        private bool SwordsofRevealingLightSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SwordsofRevealingLightActivate()
        {

            return true;
        }

        private bool CardDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardDestructionActivate()
        {

            return true;
        }

        private bool HandDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HandDestructionActivate()
        {

            return true;
        }

        private bool GravekeepersServantSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GravekeepersServantActivate()
        {

            return true;
        }

        private bool MessengerofPeaceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MessengerofPeaceActivate()
        {

            return true;
        }

        private bool DimensionalFissureSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DimensionalFissureActivate()
        {

            return true;
        }

        private bool DDDynamiteSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DDDynamiteActivate()
        {

            return true;
        }

        private bool MirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MirrorForceActivate()
        {

            return true;
        }

        private bool AssaultonGHQSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool AssaultonGHQActivate()
        {

            return true;
        }

        private bool MacroCosmosSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MacroCosmosActivate()
        {

            return true;
        }

        private bool GravityBindSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool GravityBindActivate()
        {

            return true;
        }

    }
}