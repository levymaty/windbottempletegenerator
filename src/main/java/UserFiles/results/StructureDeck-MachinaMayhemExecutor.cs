using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Structure Deck - Machina Mayhem", "StructureDeckMachinaMayhem")]
    public class StructureDeckMachinaMayhemExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int MachinaFortress = 5556499;
            public const int MachinaGearframe = 42940404;
            public const int MachinaPeacekeeper = 78349103;
            public const int ScrapRecycler = 4334811;
            public const int CommanderCovington = 22666164;
            public const int MachinaSoldier = 60999392;
            public const int MachinaSniper = 23782705;
            public const int MachinaDefender = 96384007;
            public const int MachinaForce = 58054262;
            public const int CipherSoldier = 79853073;
            public const int BlastSphere = 26302522;
            public const int HeavyMechSupportArmor = 39890958;
            public const int CyberDragon = 70095154;
            public const int ProtoCyberDragon = 26439287;
            public const int GreenGadget = 41172955;
            public const int RedGadget = 86445415;
            public const int YellowGadget = 13839120;
            public const int ArmoredCybern = 67159705;
            public const int CyberValley = 3657444;
            public const int TheBigSaturn = 34004470;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int MachinaArmoredUnit = 31828916;
            public const int Prohibition = 43711255;
            public const int SwordsofRevealingLight = 72302403;
            public const int Shrink = 55713623;
            public const int FrontlineBase = 46181000;
            public const int MachineDuplication = 63995093;
            public const int InfernoRecklessSummon = 12247206;
            public const int HandDestruction = 74519184;
            public const int CardTrader = 48712195;
            public const int Solidarity = 86780027;
            public const int TimeMachine = 80987696;
            public const int DimensionalPrison = 70342110;
            public const int Metalmorph = 68540058;
            public const int RareMetalmorph = 12503902;
            public const int Ceasefire = 36468556;
            public const int CompulsoryEvacuationDevice = 94192409;
            public const int RollOut = 91597389;
            // Initialize all useless cards

         }
        public Structure Deck - Machina MayhemExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.MachinaFortress, MachinaFortressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaFortress, MachinaFortressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaFortress, MachinaFortressRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaFortress, MachinaFortressActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaGearframe, MachinaGearframeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaGearframe, MachinaGearframeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaGearframe, MachinaGearframeRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaGearframe, MachinaGearframeActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaPeacekeeper, MachinaPeacekeeperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaPeacekeeper, MachinaPeacekeeperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaPeacekeeper, MachinaPeacekeeperRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaPeacekeeper, MachinaPeacekeeperActivate);
            AddExecutor(ExecutorType.Summon, CardId.ScrapRecycler, ScrapRecyclerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ScrapRecycler, ScrapRecyclerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ScrapRecycler, ScrapRecyclerRepos);
            AddExecutor(ExecutorType.Activate, CardId.ScrapRecycler, ScrapRecyclerActivate);
            AddExecutor(ExecutorType.Summon, CardId.CommanderCovington, CommanderCovingtonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CommanderCovington, CommanderCovingtonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CommanderCovington, CommanderCovingtonRepos);
            AddExecutor(ExecutorType.Activate, CardId.CommanderCovington, CommanderCovingtonActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaSoldier, MachinaSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaSoldier, MachinaSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaSoldier, MachinaSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaSoldier, MachinaSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaSniper, MachinaSniperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaSniper, MachinaSniperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaSniper, MachinaSniperRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaSniper, MachinaSniperActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaDefender, MachinaDefenderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaDefender, MachinaDefenderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaDefender, MachinaDefenderRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaDefender, MachinaDefenderActivate);
            AddExecutor(ExecutorType.Summon, CardId.MachinaForce, MachinaForceNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.MachinaForce, MachinaForceMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.MachinaForce, MachinaForceRepos);
            AddExecutor(ExecutorType.Activate, CardId.MachinaForce, MachinaForceActivate);
            AddExecutor(ExecutorType.Summon, CardId.CipherSoldier, CipherSoldierNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CipherSoldier, CipherSoldierMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CipherSoldier, CipherSoldierRepos);
            AddExecutor(ExecutorType.Activate, CardId.CipherSoldier, CipherSoldierActivate);
            AddExecutor(ExecutorType.Summon, CardId.BlastSphere, BlastSphereNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BlastSphere, BlastSphereMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BlastSphere, BlastSphereRepos);
            AddExecutor(ExecutorType.Activate, CardId.BlastSphere, BlastSphereActivate);
            AddExecutor(ExecutorType.Summon, CardId.HeavyMechSupportArmor, HeavyMechSupportArmorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HeavyMechSupportArmor, HeavyMechSupportArmorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HeavyMechSupportArmor, HeavyMechSupportArmorRepos);
            AddExecutor(ExecutorType.Activate, CardId.HeavyMechSupportArmor, HeavyMechSupportArmorActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragon, CyberDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragon, CyberDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragon, CyberDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragon, CyberDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.ProtoCyberDragon, ProtoCyberDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ProtoCyberDragon, ProtoCyberDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ProtoCyberDragon, ProtoCyberDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.ProtoCyberDragon, ProtoCyberDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.GreenGadget, GreenGadgetNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.GreenGadget, GreenGadgetMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.GreenGadget, GreenGadgetRepos);
            AddExecutor(ExecutorType.Activate, CardId.GreenGadget, GreenGadgetActivate);
            AddExecutor(ExecutorType.Summon, CardId.RedGadget, RedGadgetNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RedGadget, RedGadgetMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RedGadget, RedGadgetRepos);
            AddExecutor(ExecutorType.Activate, CardId.RedGadget, RedGadgetActivate);
            AddExecutor(ExecutorType.Summon, CardId.YellowGadget, YellowGadgetNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.YellowGadget, YellowGadgetMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.YellowGadget, YellowGadgetRepos);
            AddExecutor(ExecutorType.Activate, CardId.YellowGadget, YellowGadgetActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArmoredCybern, ArmoredCybernNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArmoredCybern, ArmoredCybernMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArmoredCybern, ArmoredCybernRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArmoredCybern, ArmoredCybernActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberValley, CyberValleyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberValley, CyberValleyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberValley, CyberValleyRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberValley, CyberValleyActivate);
            AddExecutor(ExecutorType.Summon, CardId.TheBigSaturn, TheBigSaturnNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TheBigSaturn, TheBigSaturnMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TheBigSaturn, TheBigSaturnRepos);
            AddExecutor(ExecutorType.Activate, CardId.TheBigSaturn, TheBigSaturnActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.MachinaArmoredUnit, MachinaArmoredUnitSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MachinaArmoredUnit, MachinaArmoredUnitActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Prohibition, ProhibitionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Prohibition, ProhibitionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SwordsofRevealingLight, SwordsofRevealingLightSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SwordsofRevealingLight, SwordsofRevealingLightActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Shrink, ShrinkSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Shrink, ShrinkActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FrontlineBase, FrontlineBaseSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FrontlineBase, FrontlineBaseActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MachineDuplication, MachineDuplicationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MachineDuplication, MachineDuplicationActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.InfernoRecklessSummon, InfernoRecklessSummonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.InfernoRecklessSummon, InfernoRecklessSummonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.HandDestruction, HandDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.HandDestruction, HandDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardTrader, CardTraderSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardTrader, CardTraderActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Solidarity, SolidaritySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Solidarity, SolidarityActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TimeMachine, TimeMachineSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TimeMachine, TimeMachineActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalPrison, DimensionalPrisonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DimensionalPrison, DimensionalPrisonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Metalmorph, MetalmorphSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Metalmorph, MetalmorphActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RareMetalmorph, RareMetalmorphSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RareMetalmorph, RareMetalmorphActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Ceasefire, CeasefireSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Ceasefire, CeasefireActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CompulsoryEvacuationDevice, CompulsoryEvacuationDeviceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CompulsoryEvacuationDevice, CompulsoryEvacuationDeviceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.RollOut, RollOutSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.RollOut, RollOutActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool MachinaFortressNormalSummon()
        {

            return true;
        }

        private bool MachinaFortressMonsterSet()
        {

            return true;
        }

        private bool MachinaFortressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaFortressActivate()
        {

            return true;
        }

        private bool MachinaGearframeNormalSummon()
        {

            return true;
        }

        private bool MachinaGearframeMonsterSet()
        {

            return true;
        }

        private bool MachinaGearframeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaGearframeActivate()
        {

            return true;
        }

        private bool MachinaPeacekeeperNormalSummon()
        {

            return true;
        }

        private bool MachinaPeacekeeperMonsterSet()
        {

            return true;
        }

        private bool MachinaPeacekeeperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaPeacekeeperActivate()
        {

            return true;
        }

        private bool ScrapRecyclerNormalSummon()
        {

            return true;
        }

        private bool ScrapRecyclerMonsterSet()
        {

            return true;
        }

        private bool ScrapRecyclerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ScrapRecyclerActivate()
        {

            return true;
        }

        private bool CommanderCovingtonNormalSummon()
        {

            return true;
        }

        private bool CommanderCovingtonMonsterSet()
        {

            return true;
        }

        private bool CommanderCovingtonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CommanderCovingtonActivate()
        {

            return true;
        }

        private bool MachinaSoldierNormalSummon()
        {

            return true;
        }

        private bool MachinaSoldierMonsterSet()
        {

            return true;
        }

        private bool MachinaSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaSoldierActivate()
        {

            return true;
        }

        private bool MachinaSniperNormalSummon()
        {

            return true;
        }

        private bool MachinaSniperMonsterSet()
        {

            return true;
        }

        private bool MachinaSniperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaSniperActivate()
        {

            return true;
        }

        private bool MachinaDefenderNormalSummon()
        {

            return true;
        }

        private bool MachinaDefenderMonsterSet()
        {

            return true;
        }

        private bool MachinaDefenderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaDefenderActivate()
        {

            return true;
        }

        private bool MachinaForceNormalSummon()
        {

            return true;
        }

        private bool MachinaForceMonsterSet()
        {

            return true;
        }

        private bool MachinaForceRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MachinaForceActivate()
        {

            return true;
        }

        private bool CipherSoldierNormalSummon()
        {

            return true;
        }

        private bool CipherSoldierMonsterSet()
        {

            return true;
        }

        private bool CipherSoldierRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CipherSoldierActivate()
        {

            return true;
        }

        private bool BlastSphereNormalSummon()
        {

            return true;
        }

        private bool BlastSphereMonsterSet()
        {

            return true;
        }

        private bool BlastSphereRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BlastSphereActivate()
        {

            return true;
        }

        private bool HeavyMechSupportArmorNormalSummon()
        {

            return true;
        }

        private bool HeavyMechSupportArmorMonsterSet()
        {

            return true;
        }

        private bool HeavyMechSupportArmorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HeavyMechSupportArmorActivate()
        {

            return true;
        }

        private bool CyberDragonNormalSummon()
        {

            return true;
        }

        private bool CyberDragonMonsterSet()
        {

            return true;
        }

        private bool CyberDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonActivate()
        {

            return true;
        }

        private bool ProtoCyberDragonNormalSummon()
        {

            return true;
        }

        private bool ProtoCyberDragonMonsterSet()
        {

            return true;
        }

        private bool ProtoCyberDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ProtoCyberDragonActivate()
        {

            return true;
        }

        private bool GreenGadgetNormalSummon()
        {

            return true;
        }

        private bool GreenGadgetMonsterSet()
        {

            return true;
        }

        private bool GreenGadgetRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool GreenGadgetActivate()
        {

            return true;
        }

        private bool RedGadgetNormalSummon()
        {

            return true;
        }

        private bool RedGadgetMonsterSet()
        {

            return true;
        }

        private bool RedGadgetRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RedGadgetActivate()
        {

            return true;
        }

        private bool YellowGadgetNormalSummon()
        {

            return true;
        }

        private bool YellowGadgetMonsterSet()
        {

            return true;
        }

        private bool YellowGadgetRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool YellowGadgetActivate()
        {

            return true;
        }

        private bool ArmoredCybernNormalSummon()
        {

            return true;
        }

        private bool ArmoredCybernMonsterSet()
        {

            return true;
        }

        private bool ArmoredCybernRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArmoredCybernActivate()
        {

            return true;
        }

        private bool CyberValleyNormalSummon()
        {

            return true;
        }

        private bool CyberValleyMonsterSet()
        {

            return true;
        }

        private bool CyberValleyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberValleyActivate()
        {

            return true;
        }

        private bool TheBigSaturnNormalSummon()
        {

            return true;
        }

        private bool TheBigSaturnMonsterSet()
        {

            return true;
        }

        private bool TheBigSaturnRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TheBigSaturnActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool MachinaArmoredUnitSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MachinaArmoredUnitActivate()
        {

            return true;
        }

        private bool ProhibitionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ProhibitionActivate()
        {

            return true;
        }

        private bool SwordsofRevealingLightSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SwordsofRevealingLightActivate()
        {

            return true;
        }

        private bool ShrinkSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShrinkActivate()
        {

            return true;
        }

        private bool FrontlineBaseSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FrontlineBaseActivate()
        {

            return true;
        }

        private bool MachineDuplicationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MachineDuplicationActivate()
        {

            return true;
        }

        private bool InfernoRecklessSummonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool InfernoRecklessSummonActivate()
        {

            return true;
        }

        private bool HandDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool HandDestructionActivate()
        {

            return true;
        }

        private bool CardTraderSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardTraderActivate()
        {

            return true;
        }

        private bool SolidaritySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SolidarityActivate()
        {

            return true;
        }

        private bool TimeMachineSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TimeMachineActivate()
        {

            return true;
        }

        private bool DimensionalPrisonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DimensionalPrisonActivate()
        {

            return true;
        }

        private bool MetalmorphSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MetalmorphActivate()
        {

            return true;
        }

        private bool RareMetalmorphSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RareMetalmorphActivate()
        {

            return true;
        }

        private bool CeasefireSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CeasefireActivate()
        {

            return true;
        }

        private bool CompulsoryEvacuationDeviceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CompulsoryEvacuationDeviceActivate()
        {

            return true;
        }

        private bool RollOutSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool RollOutActivate()
        {

            return true;
        }

    }
}