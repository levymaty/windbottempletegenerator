using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("JoJo", "JoJo")]
    public class JoJoExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int ArcanaForceVITheLovers = 97574404;
            public const int ArcanaForceXXITheWorld = 23846921;
            public const int ArcanaForce0TheFool = 62892347;
            public const int ArcanaForceIVTheEmperor = 61175706;
            public const int ArcanaForceIIITheEmpress = 35781051;
            public const int WeatherReport = 72053645;
            public const int FoolofProphecy = 63175639;
            public const int EmperorofProphecy = 53136004;
            public const int ArcanaForceVIITheChariot = 34568403;
            public const int CharioteerofProphecy = 49191560;
            public const int HermitofProphecy = 90743290;
            public const int JusticeofProphecy = 26732909;
            public const int ReaperofProphecy = 9560338;
            public const int StoicofProphecy = 54359696;
            public const int WorldofProphecy = 29146185;
            public const int LylaLightswornSorceress = 22624373;
            public const int JenisLightswornMender = 83725008;
            public const int RykoLightswornHunter = 21502796;
            public const int EmpressofProphecy = 770365;
            public const int HierophantofProphecy = 92918648;
            public const int CurioustheLightswornDominion = 98095162;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int TheGrandSpellbookTower = 33981008;
            public const int LightBarrier = 73206827;
            public const int SpellbookStarHall = 56321639;
            public const int SpellbookLibraryoftheCrescent = 40230018;
            public const int SpellbookLibraryoftheHeliosphere = 20822520;
            public const int SpellbookofEternity = 61592395;
            public const int SpellbookofKnowledge = 23314220;
            public const int SpellbookofLife = 52628687;
            public const int SpellbookofMiracles = 43841694;
            public const int SpellbookofPower = 25123082;
            public const int SpellbookofSecrets = 89739383;
            public const int SpellbookofWisdom = 88616795;
            public const int SpellbookoftheMaster = 56981417;
            public const int DrowningMirrorForce = 47475363;
            // Initialize all useless cards

         }
        public JoJoExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForceVITheLovers, ArcanaForceVITheLoversNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForceVITheLovers, ArcanaForceVITheLoversMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForceVITheLovers, ArcanaForceVITheLoversRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForceVITheLovers, ArcanaForceVITheLoversActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForceXXITheWorld, ArcanaForceXXITheWorldNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForceXXITheWorld, ArcanaForceXXITheWorldMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForceXXITheWorld, ArcanaForceXXITheWorldRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForceXXITheWorld, ArcanaForceXXITheWorldActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForce0TheFool, ArcanaForce0TheFoolNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForce0TheFool, ArcanaForce0TheFoolMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForce0TheFool, ArcanaForce0TheFoolRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForce0TheFool, ArcanaForce0TheFoolActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForceIVTheEmperor, ArcanaForceIVTheEmperorNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForceIVTheEmperor, ArcanaForceIVTheEmperorMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForceIVTheEmperor, ArcanaForceIVTheEmperorRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForceIVTheEmperor, ArcanaForceIVTheEmperorActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForceIIITheEmpress, ArcanaForceIIITheEmpressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForceIIITheEmpress, ArcanaForceIIITheEmpressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForceIIITheEmpress, ArcanaForceIIITheEmpressRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForceIIITheEmpress, ArcanaForceIIITheEmpressActivate);
            AddExecutor(ExecutorType.Summon, CardId.WeatherReport, WeatherReportNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WeatherReport, WeatherReportMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WeatherReport, WeatherReportRepos);
            AddExecutor(ExecutorType.Activate, CardId.WeatherReport, WeatherReportActivate);
            AddExecutor(ExecutorType.Summon, CardId.FoolofProphecy, FoolofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FoolofProphecy, FoolofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FoolofProphecy, FoolofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.FoolofProphecy, FoolofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.EmperorofProphecy, EmperorofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EmperorofProphecy, EmperorofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EmperorofProphecy, EmperorofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.EmperorofProphecy, EmperorofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.ArcanaForceVIITheChariot, ArcanaForceVIITheChariotNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ArcanaForceVIITheChariot, ArcanaForceVIITheChariotMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ArcanaForceVIITheChariot, ArcanaForceVIITheChariotRepos);
            AddExecutor(ExecutorType.Activate, CardId.ArcanaForceVIITheChariot, ArcanaForceVIITheChariotActivate);
            AddExecutor(ExecutorType.Summon, CardId.CharioteerofProphecy, CharioteerofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CharioteerofProphecy, CharioteerofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CharioteerofProphecy, CharioteerofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.CharioteerofProphecy, CharioteerofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.HermitofProphecy, HermitofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HermitofProphecy, HermitofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HermitofProphecy, HermitofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.HermitofProphecy, HermitofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.JusticeofProphecy, JusticeofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.JusticeofProphecy, JusticeofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.JusticeofProphecy, JusticeofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.JusticeofProphecy, JusticeofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.ReaperofProphecy, ReaperofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ReaperofProphecy, ReaperofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ReaperofProphecy, ReaperofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.ReaperofProphecy, ReaperofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.StoicofProphecy, StoicofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.StoicofProphecy, StoicofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.StoicofProphecy, StoicofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.StoicofProphecy, StoicofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.WorldofProphecy, WorldofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WorldofProphecy, WorldofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WorldofProphecy, WorldofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.WorldofProphecy, WorldofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.LylaLightswornSorceress, LylaLightswornSorceressNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LylaLightswornSorceress, LylaLightswornSorceressMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LylaLightswornSorceress, LylaLightswornSorceressRepos);
            AddExecutor(ExecutorType.Activate, CardId.LylaLightswornSorceress, LylaLightswornSorceressActivate);
            AddExecutor(ExecutorType.Summon, CardId.JenisLightswornMender, JenisLightswornMenderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.JenisLightswornMender, JenisLightswornMenderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.JenisLightswornMender, JenisLightswornMenderRepos);
            AddExecutor(ExecutorType.Activate, CardId.JenisLightswornMender, JenisLightswornMenderActivate);
            AddExecutor(ExecutorType.Summon, CardId.RykoLightswornHunter, RykoLightswornHunterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RykoLightswornHunter, RykoLightswornHunterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RykoLightswornHunter, RykoLightswornHunterRepos);
            AddExecutor(ExecutorType.Activate, CardId.RykoLightswornHunter, RykoLightswornHunterActivate);
            AddExecutor(ExecutorType.Summon, CardId.EmpressofProphecy, EmpressofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EmpressofProphecy, EmpressofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EmpressofProphecy, EmpressofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.EmpressofProphecy, EmpressofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.HierophantofProphecy, HierophantofProphecyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.HierophantofProphecy, HierophantofProphecyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.HierophantofProphecy, HierophantofProphecyRepos);
            AddExecutor(ExecutorType.Activate, CardId.HierophantofProphecy, HierophantofProphecyActivate);
            AddExecutor(ExecutorType.Summon, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionRepos);
            AddExecutor(ExecutorType.Activate, CardId.CurioustheLightswornDominion, CurioustheLightswornDominionActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.TheGrandSpellbookTower, TheGrandSpellbookTowerSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TheGrandSpellbookTower, TheGrandSpellbookTowerActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.LightBarrier, LightBarrierSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.LightBarrier, LightBarrierActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookStarHall, SpellbookStarHallSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookStarHall, SpellbookStarHallActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookLibraryoftheCrescent, SpellbookLibraryoftheCrescentSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookLibraryoftheCrescent, SpellbookLibraryoftheCrescentActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookLibraryoftheHeliosphere, SpellbookLibraryoftheHeliosphereSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookLibraryoftheHeliosphere, SpellbookLibraryoftheHeliosphereActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofEternity, SpellbookofEternitySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofEternity, SpellbookofEternityActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofKnowledge, SpellbookofKnowledgeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofKnowledge, SpellbookofKnowledgeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofLife, SpellbookofLifeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofLife, SpellbookofLifeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofMiracles, SpellbookofMiraclesSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofMiracles, SpellbookofMiraclesActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofPower, SpellbookofPowerSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofPower, SpellbookofPowerActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofSecrets, SpellbookofSecretsSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofSecrets, SpellbookofSecretsActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookofWisdom, SpellbookofWisdomSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookofWisdom, SpellbookofWisdomActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SpellbookoftheMaster, SpellbookoftheMasterSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SpellbookoftheMaster, SpellbookoftheMasterActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool ArcanaForceVITheLoversNormalSummon()
        {

            return true;
        }

        private bool ArcanaForceVITheLoversMonsterSet()
        {

            return true;
        }

        private bool ArcanaForceVITheLoversRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForceVITheLoversActivate()
        {

            return true;
        }

        private bool ArcanaForceXXITheWorldNormalSummon()
        {

            return true;
        }

        private bool ArcanaForceXXITheWorldMonsterSet()
        {

            return true;
        }

        private bool ArcanaForceXXITheWorldRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForceXXITheWorldActivate()
        {

            return true;
        }

        private bool ArcanaForce0TheFoolNormalSummon()
        {

            return true;
        }

        private bool ArcanaForce0TheFoolMonsterSet()
        {

            return true;
        }

        private bool ArcanaForce0TheFoolRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForce0TheFoolActivate()
        {

            return true;
        }

        private bool ArcanaForceIVTheEmperorNormalSummon()
        {

            return true;
        }

        private bool ArcanaForceIVTheEmperorMonsterSet()
        {

            return true;
        }

        private bool ArcanaForceIVTheEmperorRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForceIVTheEmperorActivate()
        {

            return true;
        }

        private bool ArcanaForceIIITheEmpressNormalSummon()
        {

            return true;
        }

        private bool ArcanaForceIIITheEmpressMonsterSet()
        {

            return true;
        }

        private bool ArcanaForceIIITheEmpressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForceIIITheEmpressActivate()
        {

            return true;
        }

        private bool WeatherReportNormalSummon()
        {

            return true;
        }

        private bool WeatherReportMonsterSet()
        {

            return true;
        }

        private bool WeatherReportRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WeatherReportActivate()
        {

            return true;
        }

        private bool FoolofProphecyNormalSummon()
        {

            return true;
        }

        private bool FoolofProphecyMonsterSet()
        {

            return true;
        }

        private bool FoolofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FoolofProphecyActivate()
        {

            return true;
        }

        private bool EmperorofProphecyNormalSummon()
        {

            return true;
        }

        private bool EmperorofProphecyMonsterSet()
        {

            return true;
        }

        private bool EmperorofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EmperorofProphecyActivate()
        {

            return true;
        }

        private bool ArcanaForceVIITheChariotNormalSummon()
        {

            return true;
        }

        private bool ArcanaForceVIITheChariotMonsterSet()
        {

            return true;
        }

        private bool ArcanaForceVIITheChariotRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ArcanaForceVIITheChariotActivate()
        {

            return true;
        }

        private bool CharioteerofProphecyNormalSummon()
        {

            return true;
        }

        private bool CharioteerofProphecyMonsterSet()
        {

            return true;
        }

        private bool CharioteerofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CharioteerofProphecyActivate()
        {

            return true;
        }

        private bool HermitofProphecyNormalSummon()
        {

            return true;
        }

        private bool HermitofProphecyMonsterSet()
        {

            return true;
        }

        private bool HermitofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HermitofProphecyActivate()
        {

            return true;
        }

        private bool JusticeofProphecyNormalSummon()
        {

            return true;
        }

        private bool JusticeofProphecyMonsterSet()
        {

            return true;
        }

        private bool JusticeofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool JusticeofProphecyActivate()
        {

            return true;
        }

        private bool ReaperofProphecyNormalSummon()
        {

            return true;
        }

        private bool ReaperofProphecyMonsterSet()
        {

            return true;
        }

        private bool ReaperofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ReaperofProphecyActivate()
        {

            return true;
        }

        private bool StoicofProphecyNormalSummon()
        {

            return true;
        }

        private bool StoicofProphecyMonsterSet()
        {

            return true;
        }

        private bool StoicofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool StoicofProphecyActivate()
        {

            return true;
        }

        private bool WorldofProphecyNormalSummon()
        {

            return true;
        }

        private bool WorldofProphecyMonsterSet()
        {

            return true;
        }

        private bool WorldofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WorldofProphecyActivate()
        {

            return true;
        }

        private bool LylaLightswornSorceressNormalSummon()
        {

            return true;
        }

        private bool LylaLightswornSorceressMonsterSet()
        {

            return true;
        }

        private bool LylaLightswornSorceressRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LylaLightswornSorceressActivate()
        {

            return true;
        }

        private bool JenisLightswornMenderNormalSummon()
        {

            return true;
        }

        private bool JenisLightswornMenderMonsterSet()
        {

            return true;
        }

        private bool JenisLightswornMenderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool JenisLightswornMenderActivate()
        {

            return true;
        }

        private bool RykoLightswornHunterNormalSummon()
        {

            return true;
        }

        private bool RykoLightswornHunterMonsterSet()
        {

            return true;
        }

        private bool RykoLightswornHunterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RykoLightswornHunterActivate()
        {

            return true;
        }

        private bool EmpressofProphecyNormalSummon()
        {

            return true;
        }

        private bool EmpressofProphecyMonsterSet()
        {

            return true;
        }

        private bool EmpressofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EmpressofProphecyActivate()
        {

            return true;
        }

        private bool HierophantofProphecyNormalSummon()
        {

            return true;
        }

        private bool HierophantofProphecyMonsterSet()
        {

            return true;
        }

        private bool HierophantofProphecyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool HierophantofProphecyActivate()
        {

            return true;
        }

        private bool CurioustheLightswornDominionNormalSummon()
        {

            return true;
        }

        private bool CurioustheLightswornDominionMonsterSet()
        {

            return true;
        }

        private bool CurioustheLightswornDominionRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CurioustheLightswornDominionActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool TheGrandSpellbookTowerSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TheGrandSpellbookTowerActivate()
        {

            return true;
        }

        private bool LightBarrierSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool LightBarrierActivate()
        {

            return true;
        }

        private bool SpellbookStarHallSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookStarHallActivate()
        {

            return true;
        }

        private bool SpellbookLibraryoftheCrescentSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookLibraryoftheCrescentActivate()
        {

            return true;
        }

        private bool SpellbookLibraryoftheHeliosphereSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookLibraryoftheHeliosphereActivate()
        {

            return true;
        }

        private bool SpellbookofEternitySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofEternityActivate()
        {

            return true;
        }

        private bool SpellbookofKnowledgeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofKnowledgeActivate()
        {

            return true;
        }

        private bool SpellbookofLifeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofLifeActivate()
        {

            return true;
        }

        private bool SpellbookofMiraclesSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofMiraclesActivate()
        {

            return true;
        }

        private bool SpellbookofPowerSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofPowerActivate()
        {

            return true;
        }

        private bool SpellbookofSecretsSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofSecretsActivate()
        {

            return true;
        }

        private bool SpellbookofWisdomSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookofWisdomActivate()
        {

            return true;
        }

        private bool SpellbookoftheMasterSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SpellbookoftheMasterActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

    }
}