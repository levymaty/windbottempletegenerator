using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Starter Deck - Codebreaker", "StarterDeckCodebreaker")]
    public class StarterDeckCodebreakerExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            public const int Leotron = 47226949;
            public const int Bitron = 36211150;
            public const int FlamvellGuard = 21615956;
            // Initialize all effect monsters
            public const int Texchanger = 74210057;
            public const int WidgetKid = 10705656;
            public const int CyberseWhiteHat = 46104361;
            public const int RAMClouder = 9190563;
            public const int Linkslayer = 35595518;
            public const int BackupSecretary = 63528891;
            public const int LauncherCommander = 35911108;
            public const int Cliant = 45778242;
            public const int Bitrooper = 36694815;
            public const int BeastKingBarbaros = 78651105;
            public const int CyberDragon = 70095154;
            public const int ExarionUniverse = 63749102;
            public const int EvilswarmMandragora = 8814959;
            public const int Marshmallon = 31305911;
            public const int RykoLightswornHunter = 21502796;
            public const int BattleFader = 19665973;
            public const int SwiftScarecrow = 18964575;
            public const int TranscodeTalker = 46947713;
            public const int Pentestag = 72336818;
            public const int DecodeTalker = 1861629;
            public const int LinkSpider = 98978921;
            public const int Linkuriboh = 41999284;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int CynetRecovery = 73558460;
            public const int CynetUniverse = 61583217;
            public const int Scapegoat = 73915051;
            public const int MonsterReborn = 83764719;
            public const int DarkHole = 53129443;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int BookofMoon = 14087893;
            public const int UnitedWeStand = 56747793;
            public const int CardTrader = 48712195;
            public const int BurdenoftheMighty = 44947065;
            public const int EgoBoost = 73178098;
            public const int SupplySquad = 17626381;
            public const int CynetRegression = 19943114;
            public const int ShadowSpell = 29267084;
            public const int CalloftheHaunted = 97077563;
            public const int MirrorForce = 44095762;
            public const int TorrentialTribute = 53582587;
            public const int BottomlessTrapHole = 29401950;
            public const int ZeroGravity = 83133491;
            public const int CompulsoryEvacuationDevice = 94192409;
            // Initialize all useless cards

         }
        public Starter Deck - CodebreakerExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            AddExecutor(ExecutorType.Summon, CardId.Leotron, LeotronNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Leotron, LeotronMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Leotron, LeotronRepos);
            AddExecutor(ExecutorType.Summon, CardId.Bitron, BitronNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Bitron, BitronMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Bitron, BitronRepos);
            AddExecutor(ExecutorType.Summon, CardId.FlamvellGuard, FlamvellGuardNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.FlamvellGuard, FlamvellGuardMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.FlamvellGuard, FlamvellGuardRepos);
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.Texchanger, TexchangerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Texchanger, TexchangerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Texchanger, TexchangerRepos);
            AddExecutor(ExecutorType.Activate, CardId.Texchanger, TexchangerActivate);
            AddExecutor(ExecutorType.Summon, CardId.WidgetKid, WidgetKidNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.WidgetKid, WidgetKidMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.WidgetKid, WidgetKidRepos);
            AddExecutor(ExecutorType.Activate, CardId.WidgetKid, WidgetKidActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberseWhiteHat, CyberseWhiteHatNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberseWhiteHat, CyberseWhiteHatMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberseWhiteHat, CyberseWhiteHatRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberseWhiteHat, CyberseWhiteHatActivate);
            AddExecutor(ExecutorType.Summon, CardId.RAMClouder, RAMClouderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RAMClouder, RAMClouderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RAMClouder, RAMClouderRepos);
            AddExecutor(ExecutorType.Activate, CardId.RAMClouder, RAMClouderActivate);
            AddExecutor(ExecutorType.Summon, CardId.Linkslayer, LinkslayerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Linkslayer, LinkslayerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Linkslayer, LinkslayerRepos);
            AddExecutor(ExecutorType.Activate, CardId.Linkslayer, LinkslayerActivate);
            AddExecutor(ExecutorType.Summon, CardId.BackupSecretary, BackupSecretaryNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BackupSecretary, BackupSecretaryMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BackupSecretary, BackupSecretaryRepos);
            AddExecutor(ExecutorType.Activate, CardId.BackupSecretary, BackupSecretaryActivate);
            AddExecutor(ExecutorType.Summon, CardId.LauncherCommander, LauncherCommanderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LauncherCommander, LauncherCommanderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LauncherCommander, LauncherCommanderRepos);
            AddExecutor(ExecutorType.Activate, CardId.LauncherCommander, LauncherCommanderActivate);
            AddExecutor(ExecutorType.Summon, CardId.Cliant, CliantNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Cliant, CliantMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Cliant, CliantRepos);
            AddExecutor(ExecutorType.Activate, CardId.Cliant, CliantActivate);
            AddExecutor(ExecutorType.Summon, CardId.Bitrooper, BitrooperNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Bitrooper, BitrooperMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Bitrooper, BitrooperRepos);
            AddExecutor(ExecutorType.Activate, CardId.Bitrooper, BitrooperActivate);
            AddExecutor(ExecutorType.Summon, CardId.BeastKingBarbaros, BeastKingBarbarosNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BeastKingBarbaros, BeastKingBarbarosMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BeastKingBarbaros, BeastKingBarbarosRepos);
            AddExecutor(ExecutorType.Activate, CardId.BeastKingBarbaros, BeastKingBarbarosActivate);
            AddExecutor(ExecutorType.Summon, CardId.CyberDragon, CyberDragonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.CyberDragon, CyberDragonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.CyberDragon, CyberDragonRepos);
            AddExecutor(ExecutorType.Activate, CardId.CyberDragon, CyberDragonActivate);
            AddExecutor(ExecutorType.Summon, CardId.ExarionUniverse, ExarionUniverseNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ExarionUniverse, ExarionUniverseMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ExarionUniverse, ExarionUniverseRepos);
            AddExecutor(ExecutorType.Activate, CardId.ExarionUniverse, ExarionUniverseActivate);
            AddExecutor(ExecutorType.Summon, CardId.EvilswarmMandragora, EvilswarmMandragoraNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.EvilswarmMandragora, EvilswarmMandragoraMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.EvilswarmMandragora, EvilswarmMandragoraRepos);
            AddExecutor(ExecutorType.Activate, CardId.EvilswarmMandragora, EvilswarmMandragoraActivate);
            AddExecutor(ExecutorType.Summon, CardId.Marshmallon, MarshmallonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Marshmallon, MarshmallonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Marshmallon, MarshmallonRepos);
            AddExecutor(ExecutorType.Activate, CardId.Marshmallon, MarshmallonActivate);
            AddExecutor(ExecutorType.Summon, CardId.RykoLightswornHunter, RykoLightswornHunterNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.RykoLightswornHunter, RykoLightswornHunterMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.RykoLightswornHunter, RykoLightswornHunterRepos);
            AddExecutor(ExecutorType.Activate, CardId.RykoLightswornHunter, RykoLightswornHunterActivate);
            AddExecutor(ExecutorType.Summon, CardId.BattleFader, BattleFaderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.BattleFader, BattleFaderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.BattleFader, BattleFaderRepos);
            AddExecutor(ExecutorType.Activate, CardId.BattleFader, BattleFaderActivate);
            AddExecutor(ExecutorType.Summon, CardId.SwiftScarecrow, SwiftScarecrowNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.SwiftScarecrow, SwiftScarecrowMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.SwiftScarecrow, SwiftScarecrowRepos);
            AddExecutor(ExecutorType.Activate, CardId.SwiftScarecrow, SwiftScarecrowActivate);
            AddExecutor(ExecutorType.Summon, CardId.TranscodeTalker, TranscodeTalkerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.TranscodeTalker, TranscodeTalkerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.TranscodeTalker, TranscodeTalkerRepos);
            AddExecutor(ExecutorType.Activate, CardId.TranscodeTalker, TranscodeTalkerActivate);
            AddExecutor(ExecutorType.Summon, CardId.Pentestag, PentestagNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Pentestag, PentestagMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Pentestag, PentestagRepos);
            AddExecutor(ExecutorType.Activate, CardId.Pentestag, PentestagActivate);
            AddExecutor(ExecutorType.Summon, CardId.DecodeTalker, DecodeTalkerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.DecodeTalker, DecodeTalkerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.DecodeTalker, DecodeTalkerRepos);
            AddExecutor(ExecutorType.Activate, CardId.DecodeTalker, DecodeTalkerActivate);
            AddExecutor(ExecutorType.Summon, CardId.LinkSpider, LinkSpiderNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.LinkSpider, LinkSpiderMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.LinkSpider, LinkSpiderRepos);
            AddExecutor(ExecutorType.Activate, CardId.LinkSpider, LinkSpiderActivate);
            AddExecutor(ExecutorType.Summon, CardId.Linkuriboh, LinkuribohNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.Linkuriboh, LinkuribohMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.Linkuriboh, LinkuribohRepos);
            AddExecutor(ExecutorType.Activate, CardId.Linkuriboh, LinkuribohActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.CynetRecovery, CynetRecoverySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CynetRecovery, CynetRecoveryActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CynetUniverse, CynetUniverseSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CynetUniverse, CynetUniverseActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.Scapegoat, ScapegoatSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.Scapegoat, ScapegoatActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MonsterReborn, MonsterRebornSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReborn, MonsterRebornActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, DarkHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DarkHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BookofMoon, BookofMoonSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BookofMoon, BookofMoonActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.UnitedWeStand, UnitedWeStandSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.UnitedWeStand, UnitedWeStandActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardTrader, CardTraderSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardTrader, CardTraderActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BurdenoftheMighty, BurdenoftheMightySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BurdenoftheMighty, BurdenoftheMightyActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.EgoBoost, EgoBoostSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.EgoBoost, EgoBoostActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.SupplySquad, SupplySquadSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.SupplySquad, SupplySquadActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CynetRegression, CynetRegressionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CynetRegression, CynetRegressionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ShadowSpell, ShadowSpellSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ShadowSpell, ShadowSpellActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, MirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, MirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, TorrentialTributeSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.TorrentialTribute, TorrentialTributeActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.BottomlessTrapHole, BottomlessTrapHoleSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.BottomlessTrapHole, BottomlessTrapHoleActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.ZeroGravity, ZeroGravitySpellSet);
            AddExecutor(ExecutorType.Activate, CardId.ZeroGravity, ZeroGravityActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CompulsoryEvacuationDevice, CompulsoryEvacuationDeviceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CompulsoryEvacuationDevice, CompulsoryEvacuationDeviceActivate);

         }

            // All Normal Monster Methods

        private bool LeotronNormalSummon()
        {

            return true;
        }

        private bool LeotronMonsterSet()
        {

            return true;
        }

        private bool LeotronRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BitronNormalSummon()
        {

            return true;
        }

        private bool BitronMonsterSet()
        {

            return true;
        }

        private bool BitronRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool FlamvellGuardNormalSummon()
        {

            return true;
        }

        private bool FlamvellGuardMonsterSet()
        {

            return true;
        }

        private bool FlamvellGuardRepos()
        {

            return DefaultMonsterRepos;
        }

            // All Effect Monster Methods

        private bool TexchangerNormalSummon()
        {

            return true;
        }

        private bool TexchangerMonsterSet()
        {

            return true;
        }

        private bool TexchangerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TexchangerActivate()
        {

            return true;
        }

        private bool WidgetKidNormalSummon()
        {

            return true;
        }

        private bool WidgetKidMonsterSet()
        {

            return true;
        }

        private bool WidgetKidRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool WidgetKidActivate()
        {

            return true;
        }

        private bool CyberseWhiteHatNormalSummon()
        {

            return true;
        }

        private bool CyberseWhiteHatMonsterSet()
        {

            return true;
        }

        private bool CyberseWhiteHatRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberseWhiteHatActivate()
        {

            return true;
        }

        private bool RAMClouderNormalSummon()
        {

            return true;
        }

        private bool RAMClouderMonsterSet()
        {

            return true;
        }

        private bool RAMClouderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RAMClouderActivate()
        {

            return true;
        }

        private bool LinkslayerNormalSummon()
        {

            return true;
        }

        private bool LinkslayerMonsterSet()
        {

            return true;
        }

        private bool LinkslayerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LinkslayerActivate()
        {

            return true;
        }

        private bool BackupSecretaryNormalSummon()
        {

            return true;
        }

        private bool BackupSecretaryMonsterSet()
        {

            return true;
        }

        private bool BackupSecretaryRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BackupSecretaryActivate()
        {

            return true;
        }

        private bool LauncherCommanderNormalSummon()
        {

            return true;
        }

        private bool LauncherCommanderMonsterSet()
        {

            return true;
        }

        private bool LauncherCommanderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LauncherCommanderActivate()
        {

            return true;
        }

        private bool CliantNormalSummon()
        {

            return true;
        }

        private bool CliantMonsterSet()
        {

            return true;
        }

        private bool CliantRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CliantActivate()
        {

            return true;
        }

        private bool BitrooperNormalSummon()
        {

            return true;
        }

        private bool BitrooperMonsterSet()
        {

            return true;
        }

        private bool BitrooperRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BitrooperActivate()
        {

            return true;
        }

        private bool BeastKingBarbarosNormalSummon()
        {

            return true;
        }

        private bool BeastKingBarbarosMonsterSet()
        {

            return true;
        }

        private bool BeastKingBarbarosRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BeastKingBarbarosActivate()
        {

            return true;
        }

        private bool CyberDragonNormalSummon()
        {

            return true;
        }

        private bool CyberDragonMonsterSet()
        {

            return true;
        }

        private bool CyberDragonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool CyberDragonActivate()
        {

            return true;
        }

        private bool ExarionUniverseNormalSummon()
        {

            return true;
        }

        private bool ExarionUniverseMonsterSet()
        {

            return true;
        }

        private bool ExarionUniverseRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ExarionUniverseActivate()
        {

            return true;
        }

        private bool EvilswarmMandragoraNormalSummon()
        {

            return true;
        }

        private bool EvilswarmMandragoraMonsterSet()
        {

            return true;
        }

        private bool EvilswarmMandragoraRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool EvilswarmMandragoraActivate()
        {

            return true;
        }

        private bool MarshmallonNormalSummon()
        {

            return true;
        }

        private bool MarshmallonMonsterSet()
        {

            return true;
        }

        private bool MarshmallonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool MarshmallonActivate()
        {

            return true;
        }

        private bool RykoLightswornHunterNormalSummon()
        {

            return true;
        }

        private bool RykoLightswornHunterMonsterSet()
        {

            return true;
        }

        private bool RykoLightswornHunterRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool RykoLightswornHunterActivate()
        {

            return true;
        }

        private bool BattleFaderNormalSummon()
        {

            return true;
        }

        private bool BattleFaderMonsterSet()
        {

            return true;
        }

        private bool BattleFaderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool BattleFaderActivate()
        {

            return true;
        }

        private bool SwiftScarecrowNormalSummon()
        {

            return true;
        }

        private bool SwiftScarecrowMonsterSet()
        {

            return true;
        }

        private bool SwiftScarecrowRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool SwiftScarecrowActivate()
        {

            return true;
        }

        private bool TranscodeTalkerNormalSummon()
        {

            return true;
        }

        private bool TranscodeTalkerMonsterSet()
        {

            return true;
        }

        private bool TranscodeTalkerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool TranscodeTalkerActivate()
        {

            return true;
        }

        private bool PentestagNormalSummon()
        {

            return true;
        }

        private bool PentestagMonsterSet()
        {

            return true;
        }

        private bool PentestagRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool PentestagActivate()
        {

            return true;
        }

        private bool DecodeTalkerNormalSummon()
        {

            return true;
        }

        private bool DecodeTalkerMonsterSet()
        {

            return true;
        }

        private bool DecodeTalkerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool DecodeTalkerActivate()
        {

            return true;
        }

        private bool LinkSpiderNormalSummon()
        {

            return true;
        }

        private bool LinkSpiderMonsterSet()
        {

            return true;
        }

        private bool LinkSpiderRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LinkSpiderActivate()
        {

            return true;
        }

        private bool LinkuribohNormalSummon()
        {

            return true;
        }

        private bool LinkuribohMonsterSet()
        {

            return true;
        }

        private bool LinkuribohRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool LinkuribohActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool CynetRecoverySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CynetRecoveryActivate()
        {

            return true;
        }

        private bool CynetUniverseSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CynetUniverseActivate()
        {

            return true;
        }

        private bool ScapegoatSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ScapegoatActivate()
        {

            return true;
        }

        private bool MonsterRebornSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MonsterRebornActivate()
        {

            return true;
        }

        private bool DarkHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DarkHoleActivate()
        {

            return true;
        }

        private bool MysticalSpaceTyphoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MysticalSpaceTyphoonActivate()
        {

            return true;
        }

        private bool BookofMoonSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BookofMoonActivate()
        {

            return true;
        }

        private bool UnitedWeStandSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool UnitedWeStandActivate()
        {

            return true;
        }

        private bool CardTraderSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardTraderActivate()
        {

            return true;
        }

        private bool BurdenoftheMightySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BurdenoftheMightyActivate()
        {

            return true;
        }

        private bool EgoBoostSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool EgoBoostActivate()
        {

            return true;
        }

        private bool SupplySquadSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool SupplySquadActivate()
        {

            return true;
        }

        private bool CynetRegressionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CynetRegressionActivate()
        {

            return true;
        }

        private bool ShadowSpellSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ShadowSpellActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

        private bool MirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool MirrorForceActivate()
        {

            return true;
        }

        private bool TorrentialTributeSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool TorrentialTributeActivate()
        {

            return true;
        }

        private bool BottomlessTrapHoleSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool BottomlessTrapHoleActivate()
        {

            return true;
        }

        private bool ZeroGravitySpellSet()
        {

            return DefaultSpellSet;
        }

        private bool ZeroGravityActivate()
        {

            return true;
        }

        private bool CompulsoryEvacuationDeviceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CompulsoryEvacuationDeviceActivate()
        {

            return true;
        }

    }
}