using System.Collections.Generic;
using WindBot;
using WindBot.Game;
using WindBot.Game.AI;
using YGOSharp.OCGWrapper.Enums;
namespace WindBot.Game.AI.Decks
{
   
   [Deck("Vampire Kingdom", "VampireKingdom")]
    public class VampireKingdomExecutor: DefaultExecutor
    {
        public class CardId
        {
            // Initialize all normal monsters
            // Initialize all effect monsters
            public const int VampireGenesis = 22056710;
            public const int VampireVamp = 82962242;
            public const int VampireRedBaron = 6917479;
            public const int VampireScarletScourge = 79523365;
            public const int VampireGrace = 40607210;
            public const int VampireLord = 53839837;
            public const int VampireGrimson = 33438666;
            public const int ShadowVampire = 14212201;
            public const int VampireDuke = 58947797;
            public const int VampireFraulein = 6039967;
            public const int VampireLady = 26495087;
            public const int VampireSorcerer = 88728507;
            public const int VampireRetainer = 70645913;
            public const int VampireFamiliar = 34250214;
            // Initialize all special summonable effect monsters
            // Initialize all pure special summonable effect monsters
            // Initialize all link monsters
            // Initialize all spell and trap cards
            public const int CreatureSwap = 31036355;
            public const int VampiresDesire = 69700783;
            public const int CardDestruction = 72892473;
            public const int FoolishBurial = 81439173;
            public const int VampiresDomain = 5795882;
            public const int VampireKingdom = 62188962;
            public const int VampireTakeover = 22900598;
            public const int VampireAwakening = 31189536;
            public const int DrowningMirrorForce = 47475363;
            public const int CalloftheHaunted = 97077563;
            public const int VampireDomination = 68688135;
            // Initialize all useless cards

         }
        public Vampire KingdomExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            // Add Executors to all normal monsters
            // Add Executors to all effect monsters
            AddExecutor(ExecutorType.Summon, CardId.VampireGenesis, VampireGenesisNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireGenesis, VampireGenesisMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireGenesis, VampireGenesisRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireGenesis, VampireGenesisActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireVamp, VampireVampNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireVamp, VampireVampMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireVamp, VampireVampRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireVamp, VampireVampActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireRedBaron, VampireRedBaronNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireRedBaron, VampireRedBaronMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireRedBaron, VampireRedBaronRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireRedBaron, VampireRedBaronActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireScarletScourge, VampireScarletScourgeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireScarletScourge, VampireScarletScourgeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireScarletScourge, VampireScarletScourgeRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireScarletScourge, VampireScarletScourgeActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireGrace, VampireGraceNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireGrace, VampireGraceMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireGrace, VampireGraceRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireGrace, VampireGraceActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireLord, VampireLordNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireLord, VampireLordMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireLord, VampireLordRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireLord, VampireLordActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireGrimson, VampireGrimsonNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireGrimson, VampireGrimsonMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireGrimson, VampireGrimsonRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireGrimson, VampireGrimsonActivate);
            AddExecutor(ExecutorType.Summon, CardId.ShadowVampire, ShadowVampireNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.ShadowVampire, ShadowVampireMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.ShadowVampire, ShadowVampireRepos);
            AddExecutor(ExecutorType.Activate, CardId.ShadowVampire, ShadowVampireActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireDuke, VampireDukeNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireDuke, VampireDukeMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireDuke, VampireDukeRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireDuke, VampireDukeActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireFraulein, VampireFrauleinNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireFraulein, VampireFrauleinMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireFraulein, VampireFrauleinRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireFraulein, VampireFrauleinActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireLady, VampireLadyNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireLady, VampireLadyMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireLady, VampireLadyRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireLady, VampireLadyActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireSorcerer, VampireSorcererNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireSorcerer, VampireSorcererMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireSorcerer, VampireSorcererRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireSorcerer, VampireSorcererActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireRetainer, VampireRetainerNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireRetainer, VampireRetainerMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireRetainer, VampireRetainerRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireRetainer, VampireRetainerActivate);
            AddExecutor(ExecutorType.Summon, CardId.VampireFamiliar, VampireFamiliarNormalSummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.VampireFamiliar, VampireFamiliarMonsterSet);
            AddExecutor(ExecutorType.Repos, CardId.VampireFamiliar, VampireFamiliarRepos);
            AddExecutor(ExecutorType.Activate, CardId.VampireFamiliar, VampireFamiliarActivate);
            //  Add Executors to all special summonable effect monsters
            //  Add Executors to all pure special summonable effect monsters
            //  Add Executors to all Link monsters
            //  Add Executors to all spell and trap cards
            AddExecutor(ExecutorType.SpellSet, CardId.CreatureSwap, CreatureSwapSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CreatureSwap, CreatureSwapActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampiresDesire, VampiresDesireSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampiresDesire, VampiresDesireActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CardDestruction, CardDestructionSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CardDestruction, CardDestructionActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.FoolishBurial, FoolishBurialSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.FoolishBurial, FoolishBurialActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampiresDomain, VampiresDomainSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampiresDomain, VampiresDomainActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampireKingdom, VampireKingdomSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampireKingdom, VampireKingdomActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampireTakeover, VampireTakeoverSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampireTakeover, VampireTakeoverActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampireAwakening, VampireAwakeningSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampireAwakening, VampireAwakeningActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.DrowningMirrorForce, DrowningMirrorForceSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.CalloftheHaunted, CalloftheHauntedSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.CalloftheHaunted, CalloftheHauntedActivate);
            AddExecutor(ExecutorType.SpellSet, CardId.VampireDomination, VampireDominationSpellSet);
            AddExecutor(ExecutorType.Activate, CardId.VampireDomination, VampireDominationActivate);

         }

            // All Normal Monster Methods

            // All Effect Monster Methods

        private bool VampireGenesisNormalSummon()
        {

            return true;
        }

        private bool VampireGenesisMonsterSet()
        {

            return true;
        }

        private bool VampireGenesisRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireGenesisActivate()
        {

            return true;
        }

        private bool VampireVampNormalSummon()
        {

            return true;
        }

        private bool VampireVampMonsterSet()
        {

            return true;
        }

        private bool VampireVampRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireVampActivate()
        {

            return true;
        }

        private bool VampireRedBaronNormalSummon()
        {

            return true;
        }

        private bool VampireRedBaronMonsterSet()
        {

            return true;
        }

        private bool VampireRedBaronRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireRedBaronActivate()
        {

            return true;
        }

        private bool VampireScarletScourgeNormalSummon()
        {

            return true;
        }

        private bool VampireScarletScourgeMonsterSet()
        {

            return true;
        }

        private bool VampireScarletScourgeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireScarletScourgeActivate()
        {

            return true;
        }

        private bool VampireGraceNormalSummon()
        {

            return true;
        }

        private bool VampireGraceMonsterSet()
        {

            return true;
        }

        private bool VampireGraceRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireGraceActivate()
        {

            return true;
        }

        private bool VampireLordNormalSummon()
        {

            return true;
        }

        private bool VampireLordMonsterSet()
        {

            return true;
        }

        private bool VampireLordRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireLordActivate()
        {

            return true;
        }

        private bool VampireGrimsonNormalSummon()
        {

            return true;
        }

        private bool VampireGrimsonMonsterSet()
        {

            return true;
        }

        private bool VampireGrimsonRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireGrimsonActivate()
        {

            return true;
        }

        private bool ShadowVampireNormalSummon()
        {

            return true;
        }

        private bool ShadowVampireMonsterSet()
        {

            return true;
        }

        private bool ShadowVampireRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool ShadowVampireActivate()
        {

            return true;
        }

        private bool VampireDukeNormalSummon()
        {

            return true;
        }

        private bool VampireDukeMonsterSet()
        {

            return true;
        }

        private bool VampireDukeRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireDukeActivate()
        {

            return true;
        }

        private bool VampireFrauleinNormalSummon()
        {

            return true;
        }

        private bool VampireFrauleinMonsterSet()
        {

            return true;
        }

        private bool VampireFrauleinRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireFrauleinActivate()
        {

            return true;
        }

        private bool VampireLadyNormalSummon()
        {

            return true;
        }

        private bool VampireLadyMonsterSet()
        {

            return true;
        }

        private bool VampireLadyRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireLadyActivate()
        {

            return true;
        }

        private bool VampireSorcererNormalSummon()
        {

            return true;
        }

        private bool VampireSorcererMonsterSet()
        {

            return true;
        }

        private bool VampireSorcererRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireSorcererActivate()
        {

            return true;
        }

        private bool VampireRetainerNormalSummon()
        {

            return true;
        }

        private bool VampireRetainerMonsterSet()
        {

            return true;
        }

        private bool VampireRetainerRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireRetainerActivate()
        {

            return true;
        }

        private bool VampireFamiliarNormalSummon()
        {

            return true;
        }

        private bool VampireFamiliarMonsterSet()
        {

            return true;
        }

        private bool VampireFamiliarRepos()
        {

            return DefaultMonsterRepos;
        }

        private bool VampireFamiliarActivate()
        {

            return true;
        }

            // All Special Summonable Effect Monster Methods

            // All Pure Special Summonable Effect Monster Methods

            // All Link Monster Methods

            // All Spell and Trap Card Methods

        private bool CreatureSwapSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CreatureSwapActivate()
        {

            return true;
        }

        private bool VampiresDesireSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampiresDesireActivate()
        {

            return true;
        }

        private bool CardDestructionSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CardDestructionActivate()
        {

            return true;
        }

        private bool FoolishBurialSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool FoolishBurialActivate()
        {

            return true;
        }

        private bool VampiresDomainSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampiresDomainActivate()
        {

            return true;
        }

        private bool VampireKingdomSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampireKingdomActivate()
        {

            return true;
        }

        private bool VampireTakeoverSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampireTakeoverActivate()
        {

            return true;
        }

        private bool VampireAwakeningSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampireAwakeningActivate()
        {

            return true;
        }

        private bool DrowningMirrorForceSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool DrowningMirrorForceActivate()
        {

            return true;
        }

        private bool CalloftheHauntedSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool CalloftheHauntedActivate()
        {

            return true;
        }

        private bool VampireDominationSpellSet()
        {

            return DefaultSpellSet;
        }

        private bool VampireDominationActivate()
        {

            return true;
        }

    }
}